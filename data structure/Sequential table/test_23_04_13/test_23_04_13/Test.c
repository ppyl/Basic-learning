#define _CRT_SECURE_NO_WARNINGS 1
#include"SeqList.h"

void TestSeqList1()
{
	SL sl;
	SLInit(&sl);
	SLPushBack(&sl, 1);
	SLPushBack(&sl, 2);
	SLPushBack(&sl, 3);
	SLPushBack(&sl, 4);
	SLPushBack(&sl, 5);
	
	SLPrint(&sl);
	
	SLDestroy(&sl);
}

void TestSeqList2()
{
	SL sl;
	SLInit(&sl);
	SLPushBack(&sl, 1);
	SLPushBack(&sl, 2);
	SLPushBack(&sl, 3);
	SLPushBack(&sl, 4);
	SLPrint(&sl);

	SLPopBack(&sl);
	SLPrint(&sl);

	SLDestroy(&sl);
}

void TestSeqList3()
{
	SL sl;
	SLInit(&sl);
	SLPushFront(&sl, 1);
	SLPushFront(&sl, 2);
	SLPushFront(&sl, 3);
	SLPushFront(&sl, 4);

	SLPrint(&sl);

	SLPopBack(&sl);
	SLPopBack(&sl);
	SLPopBack(&sl);

	SLPrint(&sl);

	SLDestroy(&sl);
}

void TestSeqList4()
{
	SL sl;
	SLInit(&sl);
	SLPushFront(&sl, 1);
	SLPushFront(&sl, 2);
	SLPushFront(&sl, 3);
	SLPushFront(&sl, 4);
	SLPrint(&sl);

	SLInsert(&sl, 2, 20);
	SLPrint(&sl);

	SLDestroy(&sl);
}

void TestSeqList5()
{
	SL sl;
	SLInit(&sl);
	SLPushFront(&sl, 1);
	SLPushFront(&sl, 2);
	SLPushFront(&sl, 3);
	SLPushFront(&sl, 4);
	SLPrint(&sl);

	SLErase(&sl, 2);
	SLPrint(&sl);

	SLDestroy(&sl);
}

//void TestSeqList6()
//{
//	SL sl;
//	SLInit(&sl);
//	SLPushFront(&sl, 1);
//	SLPushFront(&sl, 2);
//	SLPushFront(&sl, 3);
//	SLPushFront(&sl, 4);
//	SLPushFront(&sl, 5);
//	SLPushFront(&sl, 6);
//	SLPushFront(&sl, 7);
//	SLPushFront(&sl, 8);
//	SLPrint(&sl);
//
//	int pos = SLFind(&sl, 5);
//	if (pos != -1)
//	{
//		SLErase(&sl, pos);
//	}
//	SLPrint(&sl);
//	SLDestroy(&sl);
//}

void TestSeqList7()
{
	SL sl;
	SLInit(&sl);
	SLPushFront(&sl, 1);
	SLPushFront(&sl, 5);
	SLPushFront(&sl, 2);
	SLPushFront(&sl, 3);
	SLPushFront(&sl, 4);
	SLPushFront(&sl, 5);
	SLPushFront(&sl, 6);
	SLPushFront(&sl, 7);
	SLPushFront(&sl, 5);
	SLPushFront(&sl, 8);
	SLPrint(&sl);

	int pos = SLFind(&sl, 5, 0);
	while (pos != -1)
	{
		SLErase(&sl, pos);

		pos = SLFind(&sl, 5, pos);
	}
	SLPrint(&sl);
	SLDestroy(&sl);
}

void TestSeqList8()
{
	SL sl;
	SLInit(&sl);
	//SLPushFront(&sl, 0);
	//SLPushFront(&sl, 1);
	//SLPushFront(&sl, 2);
	//SLPushFront(&sl, 3);
	//SLPushFront(&sl, 4);
	//SLPushFront(&sl, 5);
	//SLPushFront(&sl, 6);
	//SLPushFront(&sl, 7);
	//SLPushFront(&sl, 8);
	for (int i = 0; i < 18; i++)
	{
		SLPushFront(&sl, i);
	}
	SLPrint(&sl);
	//int pos = SLFind(&sl, 5, 0);
	//InsList(&sl, pos, 3);
	InsList(&sl, 3, 5);
	SLPrint(&sl);

	SLDestroy(&sl);
}

int main()
{
	//TestSeqList1();
	//TestSeqList2();
	//TestSeqList3();
	//TestSeqList4();
	//TestSeqList6();
	//TestSeqList7();
	TestSeqList8();
	/*int* p1 = malloc(4);
	int* p2 = realloc(p1, 20);
	int* p3 = realloc(p2, 2000);*/
	//越界不一定报错，但是它本身就是一个错误
	//越界读是一个更严重的错误，该错误基本不会被检查出来（该错误不会改变目标位置数据）
	//越界写同样，但是它可能会被检查出来
	
	return 0;
}