#define _CRT_SECURE_NO_WARNINGS 1
#include"SeqList.h"

void SLPrint(SL* ps)
{
	assert(ps);
	for (int i = 0; i < ps->size; ++i)
	{
		printf("%d ", ps->a[i]);
	}
	printf("\n");
}

void SLCheckCapacity(SL* ps)
{
	assert(ps);
	//扩容
	if (ps->size == ps->capacity)
	{
		int newCapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
		SLDataType* tmp = (SLDataType*)realloc(ps->a, newCapacity * sizeof(SLDataType));
		if (tmp == NULL)
		{
			perror("realloc fail");
			exit(-1); //异常结束程序
		}
		ps->a = tmp;
		ps->capacity = newCapacity;
	}
}

void SLInit(SL* ps)
{
	assert(ps);
	ps->a = NULL;
	ps->size = 0;
	ps->capacity = 0;
}

void SLDestroy(SL* ps)
{
	assert(ps);
	if (ps->a)
	{
		free(ps->a);
		ps->a = NULL;
		ps->size = ps->capacity = 0;
	}
}

//O(1)
void SLPushBack(SL* ps, SLDataType x)
{
	//assert(ps);
	//SLCheckCapacity(ps);
	//ps->a[ps->size] = x;
	//ps->size++;

	SLInsert(ps, ps->size, x);
}

void SLPopBack(SL* ps)
{
	//正常检查
	//if (ps->size == 0)
	//{
	//	return;
	// }
	
	////暴力检查
	//assert(ps->size > 0);
	//ps->size--;
	SLErase(ps, ps->size - 1);
}

//O(N)
void SLPushFront(SL* ps, SLDataType x)
{
	//assert(ps);
	//SLCheckCapacity(ps);
	////挪动数据
	//int end = ps->size - 1;
	//while (end >= 0)
	//{
	//	ps->a[end + 1] = ps->a[end];
	//	--end;
	//}
	//ps->a[0] = x;
	//ps->size++;
	SLInsert(ps, 0, x);
}

void SLPopFront(SL* ps)
{
	//assert(ps->size > 0);
	//int begin = 1;
	//while (begin < ps->size)
	//{
	//	ps->a[begin - 1] = ps->a[begin];
	//	begin++;
	//}

	//ps->size--;
	SLErase(ps, 0);
}

void SLInsert(SL* ps, int pos, SLDataType x)
{
	assert(ps);
	assert(pos >= 0);
	assert(pos <= ps->size);

	SLCheckCapacity(ps);
	int end = ps->size - 1;
	while (end >= pos)
	{
		ps->a[end + 1] = ps->a[end];
		end--;
	}
	ps->a[pos] = x;
	ps->size++;
}

void SLErase(SL* ps, int pos)
{
	assert(ps);
	assert(pos >= 0);
	assert(pos < ps->size);
	int begin = pos + 1;
	while (begin < ps->size)
	{
		ps->a[begin - 1] = ps->a[begin];
		begin++;
	}
	ps->size--;
}

//int SLFind(SL* ps, SLDataType x)
//{
//	assert(ps);
//
//	for (int i = 0; i < ps->size; ++i)
//	{
//		if (ps->a[i] == x)
//		{
//			return i;
//		}
//	}
//	return -1;
//}

int SLFind(SL* ps, SLDataType x,int begin)
{
	assert(ps);

	for (int i = begin; i < ps->size; ++i)
	{
		if (ps->a[i] == x)
		{
			return i;
		}
	}
	return -1;
}

//修改
void SeqListModity(SL* psl, int pos, SLDataType x)
{
	assert(psl);
	assert(pos < psl->size && pos > 0);//判断给出的下标是否在范围内
	psl->a[pos] = x;
}

//void InsList(SL* ps, int pos, int k)
//{
//	assert(ps);
//	assert(pos >= 0);
//	assert(pos + k <= ps->size);
//	if (pos + k == ps->size)
//	{
//		ps->size = pos;
//		return;
//	}
//	int begin = pos;
//	for (int i = begin + k; i >= begin; i--)
//	{
//		ps->a[begin] = ps->a[begin + 1];
//		begin++;
//	}
//	ps->size = ps->size - k;
//}

void InsList(SL* ps, int pos, int k)
{
	assert(ps);
	assert(pos >= 0);
	assert(pos + k <= ps->size);
	for (int i = pos + k;i<ps->size;i++)
	{
		ps->a[i - k] = ps->a[i];
	}
	//void * memmove ( void * destination, const void * source, size_t num );
	//memmove(ps->a+pos, ps->a + pos + k, 4*(ps->size-pos+k));
	ps->size = ps->size - k;
}
