#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>

////静态顺序表
//#define N 10
//typedef int SLDataType;
//typedef struct SeqList
//{
//	SLDataType a[N];
//	int size; //记录存储多少个有效数据
//}SL;
//
//void SLInit(SL s);
//void SLPushBack(SL s, SLDataType x); //尾插aq

//动态顺序表 -- 按需扩空间
typedef int SLDataType;
typedef struct SeqList
{
	SLDataType* a;//指向动态开辟的数组（空间的开始）
	int size;     //记录存储多少个有效数据
	int capacity; //空间容量大小
}SL;

void SLPrint(SL* ps);

void SLInit(SL* ps);
void SLDestroy(SL* ps);
void SLCheckCapacity(SL* ps);

void SLPushBack(SL* ps, SLDataType x); //尾插aq
void SLPopBack(SL* ps); //尾删

void SLPushFront(SL* ps, SLDataType x); //头插
void SLPopFront(SL* ps); //头删

//中间插入删除
void SLInsert(SL* ps, int pos, SLDataType x);
void SLErase(SL* ps, int pos);

//int SLFind(SL* ps, SLDataType x);
//begin 查找x的起始位置
int SLFind(SL* ps, SLDataType x,int begin);

//修改
void SeqListModity(SL* psl, int pos, SLDataType x);

void InsList(SL* ps, int pos, int k);