#define _CRT_SECURE_NO_WARNINGS 1
#include "SeqList.h"

//初始化
//初始化
void SeqListInit(SL* psl)
{
	assert(psl);
	psl->a = NULL;
	psl->capacity = 0;
	psl->sizel = 0;
}

//增容
void SeqListCheckCapacity(SL* psl)
{
	assert(psl);
	if (psl->capacity == psl->sizel)
	{
		int newcapacity = psl->capacity == 0 ? 4 : psl->capacity * 2;
		SLDataType* temp = (SLDataType*)realloc(psl->a, newcapacity * sizeof(SLDataType));
		assert(temp);

		psl->a = temp;
		psl->capacity = newcapacity;
	}
}

//尾插
void SeqListPushBack(SL* psl, SLDataType x)
{
	assert(psl);
	SeqListCheckCapacity(psl);//判断capacity是否足够，若不够进行增容
	psl->a[psl->sizel] = x;
	psl->sizel++;
}

//头插——方法1
//void SeqListPushFront(SL* psl, SQDataType x)
//{
//  assert(psl);
//	SeqListCheckCapacity(psl);//增容
//	for (int i = psl->sizel-1; i >= 0; i--)
//	{
//		psl->a[i + 1] = psl->a[i];
//	}
//	psl->a[0] = x;
//	psl->sizel++;
//}

//头插——方法2
void SeqListPushFront(SL* psl, SLDataType x)
{
	assert(psl);
	SeqListCheckCapacity(psl);//增容
	memmove(&(psl->a[1]), psl->a, psl->sizel * sizeof(SLDataType));
	psl->a[0] = x;
	psl->sizel++;
}

//头删
void SeqListPopFront(SL* psl)
{
	assert(psl);
	assert(psl->sizel > 0);//判断顺序表中是否有数据
	memmove(psl->a, &(psl->a[1]), (psl->sizel - 1) * sizeof(SLDataType));
	psl->a[psl->sizel - 1] = 0;
	psl->sizel--;
}

//尾删
void SeqListPopBack(SL* psl)
{
	assert(psl);
	assert(psl->sizel > 0);//判断顺序表中是否有数据
	psl->a[psl->sizel - 1] = NULL;
	psl->sizel--;
}

//选择插入
//void SeqListInsert(SL* psl, int index, SQDataType x)
//{
//  assert(psl);
//	assert(index <= psl->sizel);
//	SeqListCheckCapacity(psl);//增容
//	memmove(&(psl->a[index + 1]), &(psl->a[index]), (psl->sizel - index) * sizeof(SQDataType));
//	psl->a[index] = x;
//	psl->sizel++;
//}

//选择插入
void SeqListInsert(SL* psl, int index, SLDataType x)
{
	assert(psl);
	assert(index <= psl->sizel);
	SeqListCheckCapacity(psl);//增容
	int cou = psl->sizel;
	while (cou > index)
	{
		psl->a[cou] = psl->a[cou - 1];
		cou--;
	}
	psl->a[index] = x;
	psl->sizel++;
}

//选择删除
void SeqListErase(SL* psl, int index)
{
	assert(psl);
	assert(index < psl->sizel);
	memmove(&(psl->a[index]), &(psl->a[index + 1]), (psl->sizel - index - 1) * sizeof(SLDataType));
	psl->a[psl->sizel - 1] = NULL;
	psl->sizel--;
}

//查找
void SeqListFind(SL* psl, SLDataType x)
{
	assert(psl);
	int num = 0;
	while (num < psl->sizel)
	{
		if (psl->a[num] == x)
		{
			printf("%d\n", num);
			return;
		}
		num++;
	}
	printf("表中没有%d\b", x);
	return;
}

//修改
void SeqListModity(SL* psl, int pos, SLDataType x)
{
	assert(psl);
	assert(pos < psl->sizel);
	psl->a[pos] = x;
}

//打印链表
void SeqListPrint(SL* psl)
{
	assert(psl);
	for (int i = 0; i < psl->sizel; i++)
	{
		printf("%d ", psl->a[i]);
	}
	printf("\n");
}

//销毁空间
void SeqListDestroy(SL* psl)
{
	assert(psl);
	free(psl->a);
	psl->a = NULL;
	psl->capacity = 0;
	psl->sizel = 0;
}