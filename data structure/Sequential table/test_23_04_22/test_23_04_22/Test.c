#define _CRT_SECURE_NO_WARNINGS 1
#include "SeqList.h"

//菜单
void menu()
{
	printf("**********************\n");
	printf("1.尾插数据，2.头插数据\n");
	printf("3.尾删数据，4.头删数据\n");
	printf("5.选择插入，6.选择删除\n");
	printf("7.查找元素，8.修改链表\n");
	printf("9.打印数据，-1，退出\n");
	printf("**********************\n");
	printf("请输入你的选择:");
}

int main()
{
	SL slist;
	int choose = 0;
	SeqListInit(&slist);//初始化
	while (choose != -1)
	{
		int a = 0, b = 0;
		menu();
		scanf(" %d", &choose);
		switch (choose)
		{
		case 1://尾插
			printf("请输入你要插入的数据，以-1结束\n");
			while (a != -1)
			{
				scanf("%d", &a);
				if (a != -1)
					SeqListPushBack(&slist, a);
			}
			break;
		case 2://头插
			printf("请输入你要插入的数据，以-1结束\n");
			while (a != -1)
			{
				scanf("%d", &a);
				if (a != -1)
					SeqListPushFront(&slist, a);
			}
			break;
		case 3://尾删
			SeqListPopBack(&slist);
			printf("尾删，删除成功！\n");
			break;
		case 4://头删
			SeqListPopFront(&slist);
			printf("头删，删除成功!\n");
			break;
		case 5://选择插入
			printf("请输入需要插入的下标和元素:");
			scanf("%d%d", &a, &b);
			SeqListInsert(&slist, a, b);
			break;
		case 6://选择删除
			printf("请输入需要删除的元素的下标:");
			scanf("%d", &a);
			SeqListErase(&slist, a);
			break;
		case 7://查找元素
			printf("请输入需要查找的元素的下标:");
			scanf("%d", &a);
			SeqListFind(&slist, a);
			break;
		case 8://修改链表
			printf("请输入需要修改的下标和元素:");
			scanf("%d%d", &a, &b);
			SeqListModity(&slist, a, b);
			break;
		case 9://打印
			SeqListPrint(&slist);
			break;
		case -1://退出
			SeqListDestroy(&slist);//销毁空间
			break;
		default:
			printf("输入错误，请重新输入！\n");
			break;
		}
		system("pause");
		system("cls");
	}
	return 0;
}