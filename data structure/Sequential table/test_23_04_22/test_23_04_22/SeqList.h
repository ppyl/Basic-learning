#pragma once
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<assert.h>

#define N 10;
typedef int SLDataType;

typedef struct SeqList
{
	SLDataType* a;
	int sizel;     //有效数据个数
	int capacity;  //容量
}SL;

//初始化
void SeqListInit(SL* psl);

//尾插入
void SeqListPushBack(SL* psl, SLDataType x);

//头插入
void SeqListPushFront(SL* psl, SLDataType x);

//头删
void SeqListPopFront(SL* psl);

//尾删
void SeqListPopBack(SL* psl);

//选择插入
void SeqListInsert(SL* psl, int index, SLDataType x);

//选择删除
void SeqListErase(SL* psl, int index);

//查找
void SeqListFind(SL* psl, SLDataType x);

//修改
void SeqListModity(SL* psl, int pos, SLDataType x);

//打印
void SeqListPrint(SL* psl);

//销毁空间
void SeqListDestroy(SL* psl);