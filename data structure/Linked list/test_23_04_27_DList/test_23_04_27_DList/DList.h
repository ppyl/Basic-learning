#pragma once

#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<stdbool.h>

typedef int LTDataType;
typedef struct ListNode
{
	struct ListNode* next;
	struct ListNode* prev;
	LTDataType data;
}LTNode;

LTNode* BuyLTNode(LTDataType x);
LTNode* LTInit();//使用返回值，避免使用二级指针

void LTPrint(LTNode* phead);
void LTPushBack(LTNode* phead,LTDataType x);
void LTPopBack(LTNode* phead);//不需要二级指针，有哨兵位

void LTPushFront(LTNode* phead, LTDataType x);
void LTPopFront(LTNode* phead);

LTNode* LTFind(LTNode* phead, LTDataType x);

//在pos之前插入pos
void LTInsert(LTNode* pos, LTDataType x);
void LTErase(LTNode* pos);

bool LTEmpty(LTNode* phead);
size_t LTSize(LTNode* phead);

void LTDestroy(LTNode* phead);