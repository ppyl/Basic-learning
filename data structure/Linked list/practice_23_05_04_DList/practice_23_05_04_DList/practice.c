#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//环形链表
//bool hasCycle(struct ListNode* head) 
//{
//    struct ListNode* fast = head;
//    struct ListNode* slow = head;
//    while (fast && fast->next)
//    {
//        slow = slow->next;
//        fast = fast->next->next;
//        if (slow == fast)
//        {
//            return true;
//        }
//    }
//    return false;
//}
//请证明slow每次走1步，fast每次走2步一定能相遇
//因为每追击一次，距离缩小1，所以最终相遇

//环形链表2
//公式：L=(k-1)*C+C-N
// L：头结点到入口点的距离
// C：环的周长
// N：slow和fast在环内相遇的位置与入口点的距离
//struct ListNode* detectCycle(struct ListNode* head)
//{
//    struct ListNode* slow = head, * fast = head;
//    while (fast && fast->next)
//    {
//        slow = slow->next;
//        fast = fast->next->next;
//        if (slow == fast)
//        {
//            struct ListNode* meet = slow;
//            while (head != meet)
//            {
//                head = head->next;
//                meet = meet->next;
//            }
//            return meet;
//        }
//    }
//    return NULL;
//}

//制造两条链表，将问题转换为求两条链表的相交
//int Count(struct ListNode* cur)
//{
//    int x = 0;
//    while (cur)
//    {
//        cur = cur->next;
//        x++;
//    }
//    return x;
//}
//
//struct ListNode* ComNewNode(struct ListNode* head, int a)
//{
//    while (a--)
//    {
//        head = head->next;
//    }
//    return head;
//}
//
//struct ListNode* getIntersectionNode(struct ListNode* headA, struct ListNode* headB) {
//    int a1 = Count(headA);
//    int a2 = Count(headB);
//    if (a1 < a2)
//    {
//        headB = ComNewNode(headB, a2 - a1);
//    }
//    else if (a1 > a2)
//    {
//        headA = ComNewNode(headA, a1 - a2);
//    }
//    while (headA != headB)
//    {
//        headA = headA->next;
//        headB = headB->next;
//    }
//    return headA;
//}
//struct ListNode* detectCycle(struct ListNode* head) {
//    struct ListNode* fast = head;
//    struct ListNode* slow = head;
//    while (fast && fast->next)
//    {
//        slow = slow->next;
//        fast = fast->next->next;
//        if (slow == fast)
//        {
//            struct ListNode* meet = slow;
//            struct ListNode* otherHead = meet->next;
//            meet->next == NULL;
//            return getIntersectionNode(head, otherHead);
//        }
//    }
//    return NULL;
//}

//复制带随机指针的链表
//struct Node* copyRandomList(struct Node* head) {
//    struct Node* cur = head;
//    while (cur)
//    {
//        struct Node* next = cur->next;
//        struct Node* copy = (struct Node*)malloc(sizeof(struct Node));
//        copy->val = cur->val;
//
//        //插入链接
//        cur->next = copy;
//        copy->next = next;
//
//        cur = next;
//    }
//
//    cur = head;
//    while (cur)
//    {
//        struct Node* copy = cur->next;
//
//        if (cur->random == NULL)
//        {
//            copy->random = NULL;
//        }
//        else
//        {
//            //copy结点的random是cur结点的random指向的next, 
//            //示例1，以第二个拷贝节点为例，即head的next（这里指的是copy的head）
//            //copy结点13的random指向的是此时cur结点的random的next（此时next指的是copy7）
//            copy->random = cur->random->next;
//        }
//        cur = cur->next->next;//由于附庸一个链表，所以指两次next
//    }
//
//    cur = head;
//    struct Node* copyHead = NULL, * copyTail = NULL;
//    while (cur)
//    {
//        struct Node* copy = cur->next;
//        struct Node* next = copy->next;
//
//        cur->next = next;
//        if (copyTail == NULL)
//        {
//            copyHead = copyTail = copy;
//        }
//        else
//        {
//            copyTail->next = copy;
//            copyTail = copyTail->next;
//        }
//        cur = next;
//    }
//    return copyHead;
//}