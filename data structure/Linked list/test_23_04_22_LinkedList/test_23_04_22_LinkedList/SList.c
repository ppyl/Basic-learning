#define _CRT_SECURE_NO_WARNINGS 1
#include"SList.h"

SLTNode* BuySLTNode(SLTDataType x)
{
	SLTNode* newnode = (SLTNode*)malloc(sizeof(SLTNode));
	if (newnode == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	newnode->data = x;
	newnode->next = NULL;

	return newnode;
}

SLTNode* CreatSList(int n)
{
	SLTNode* phead = NULL;
	SLTNode* ptail = NULL;
	int x = 0;
	for (int i = 0; i < n; i++)
	{
		scanf("%d", &x);
		SLTNode* newnode = BuySLTNode(x);
		if (phead == NULL)
		{
			ptail = phead = newnode;
		}
		else
		{
			ptail->next = newnode;
			ptail = newnode;
		}
	}
	//ptail->next = NULL;
	return phead; //返回phead避免丢失
}

void SLTPrint(SLTNode* phead)
{
	SLTNode* cur = phead;
	while (cur != NULL)
	{
		//printf("%d->", cur->data);
		printf("[%d|%p]->", cur->data, cur->next);
		cur = cur->next;
	}
	printf("NULL\n");
}

//传址调用，需要注意是不同等级指针之间的转换
//改变int，传递int*给形参，*形参进行交换改变
//改变int*，传递int**给形参，*形参进行交换改变
void SLTPushBack(SLTNode** pphead, SLTDataType x)//找尾
{
	SLTNode* newnode = BuySLTNode(x);
	if (*pphead == NULL)
	{
		*pphead = newnode;
	}
	else
	{
		SLTNode* tail = *pphead;
		while (tail->next) //利用结点存储的地址寻找结束条件
		{
			tail = tail->next;
		}
		tail->next = newnode;
	}
}

//尾删1
//void SLTPopBack(SLTNode** pphead)
//{
//	assert(*pphead);
//	SLTNode* prev = *pphead;
//	SLTNode* tail = *pphead;
//	while (tail->next)
//	{
//		prev = tail;
//		tail = tail->next;
//	}
//	if (prev == tail)
//	{
//		*pphead = NULL;
//		free(prev);
//	}
//	else
//	{
//		prev->next = NULL;
//		tail = NULL;
//		free(tail);//释放空间时应注意是否会再次使用该指针，避免产生野指针出现错误
//	}
//}

//尾删2
void SLTPopBack(SLTNode** pphead)
{
	assert(*pphead);//列表为空不能删除
	if ((*pphead)->next == NULL)
	{
		free(*pphead);
		*pphead = NULL;
	}
	else
	{
		SLTNode* tail = *pphead;
		while (tail->next->next)
		{
			tail = tail->next;
		}
		free(tail->next);
		tail->next = NULL;
	}
}

void SLTPushFront(SLTNode** pphead, SLTDataType x)
{
	SLTNode* newnode = BuySLTNode(x);
	newnode->next = *pphead;
	*pphead = newnode;
}

void SLTPopFront(SLTNode** pphead)
{
	assert(*pphead);
	SLTNode* tail = *pphead;
	*pphead = tail->next;
	tail = NULL;
	free(tail);
}	

SLTNode* SLTFind(SLTNode* phead, SLTDataType x)
{
	SLTNode* cur = phead;
	while (cur)
	{
		if (cur->data == x)
		{
			return cur;
		}
		cur = cur->next;
	}
	return NULL;
}

//由于此处改变的是结构，所以结构体指针够用，不需要二级指针
void SLTInsertAfter(SLTNode* pos, SLTDataType x)
{
	assert(pos);
	SLTNode* newnode = BuySLTNode(x);
	newnode->next = pos->next;
	pos->next = newnode;
}

void SLTInsert(SLTNode** pphead, SLTNode* pos, SLTDataType x)
{
	assert(pos);
	if (*pphead == pos)
	{
		SLTPushFront(pphead, x);
	}
	else
	{
		SLTNode* prev = *pphead;
		while (prev->next != pos)
		{
			prev = prev->next;
		}
		SLTNode* newnode = BuySLTNode(x);
		prev->next = newnode;
		newnode->next = pos;
	}
}

void SLTEraseAfter(SLTNode* pos)
{
	assert(pos);
	if (pos->next == NULL)
	{
		return;
	}
	else
	{
		//pos->next = pos->next->next;//这种写法需要保存pos->next指向的结点，否则无法完成对其的释放
		SLTNode* cur = pos->next;
		pos->next = cur->next;
		free(cur);
	}
}

void SLTErase(SLTNode** pphead, SLTNode* pos)
{
	assert(pos);
	assert(*pphead);

	if (pos == *pphead)
	{
		SLTPopFront(pphead);
	}
	else
	{
		SLTNode* prev = *pphead;
		while (prev->next != pos)
		{
			prev = prev->next;
		}
		prev->next = pos->next;
		free(pos);
	}
}

void SLTDestroy(SLTNode** pphead)
{
	SLTNode* cur = *pphead;
	while (cur)
	{
		SLTNode* next = cur->next;
		free(cur);
		cur = next;
	}
	*pphead = NULL;
}