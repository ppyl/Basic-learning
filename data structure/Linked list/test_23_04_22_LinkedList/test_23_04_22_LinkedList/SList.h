#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

typedef int SLTDataType;

typedef struct SListNode
{
	SLTDataType data;
	struct SListNode* next;
}SLTNode;

SLTNode* BuySLTNode(SLTDataType x);
SLTNode* CreatSList(int n);
void SLTPrint(SLTNode* phead);

void SLTPushBack(SLTNode** pphead, SLTDataType x);//找尾
void SLTPopBack(SLTNode* phead);
void SLTPushFront(SLTNode** pphead, SLTDataType x);
void SLTPopFront(SLTNode** pphead);

SLTNode* SLTFind(SLTNode* phead, SLTDataType x);
//先find后insert/erase
//pos之后
void SLTInsertAfter(SLTNode* pos, SLTDataType x);
//pos之前
void SLTInsert(SLTNode** pphead, SLTNode* pos, SLTDataType x);

void SLTEraseAfter(SLTNode* pos);

void SLTErase(SLTNode** pphead, SLTNode* pos);

void SLTDestroy(SLTNode** pphead);