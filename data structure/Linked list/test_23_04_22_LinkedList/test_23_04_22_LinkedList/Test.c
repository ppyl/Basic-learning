#define _CRT_SECURE_NO_WARNINGS 1
#include"SList.h"

//void TestSList1()
//{
//	//SLTNode* n1 = BuySLTNode(1);
//	//SLTNode* n2 = BuySLTNode(2);
//	//SLTNode* n3 = BuySLTNode(3);
//	//SLTNode* n4 = BuySLTNode(4);
//	//n1->next = n2;
//	//n2->next = n3;
//	//n3->next = n4;
//	//n4->next = NULL;
//}

//链表的关键有一个头指针
//顺序表的关键是一个结构
//void TestSList2()
//{
//	SLTNode* plist = CreatSList(5); //phead拷贝到plist
//	SLTPrint(plist);//传参
//}

void TestSList3()
{
	SLTNode* plist = CreatSList(5);
	SLTPushBack(&plist, 100);
	SLTPushBack(&plist, 200);
	SLTPushBack(&plist, 300);
	SLTPrint(plist);
}

void TestSList4()
{
	SLTNode* plist = NULL;
	SLTPushBack(&plist, 100);
	SLTPushBack(&plist, 200);
	SLTPushBack(&plist, 300);
	SLTPrint(plist);

	SLTPopBack(&plist);
	SLTPrint(plist);

	SLTPopBack(&plist);
	SLTPrint(plist);

	SLTPopBack(&plist);
	SLTPrint(plist);
}

void TestSList5()
{
	SLTNode* plist = NULL;
	SLTPushFront(&plist, 100);
	SLTPushFront(&plist, 200);
	SLTPushFront(&plist, 300);
	SLTPrint(plist);

	SLTPopFront(&plist);
	SLTPopFront(&plist);
	SLTPopFront(&plist);
	SLTPrint(plist);
}

void TestSList6()
{
	SLTNode* plist = NULL;
	SLTPushFront(&plist, 1);
	SLTPushFront(&plist, 2);
	SLTPushFront(&plist, 3);
	SLTPushFront(&plist, 4);
	SLTPushFront(&plist, 5);
	SLTPrint(plist);
	
	//SLTNode* pos = SLTFind(plist, 3);
	//SLTInsertAfter(pos, 30);
	//SLTPrint(plist);

	//pos = SLTFind(plist, 2);
	//SLTInsert(&plist, pos, 20);
	//SLTPrint(plist);

	SLTNode* pos = SLTFind(plist, 2);
	SLTEraseAfter(pos);
	SLTPrint(plist);

	pos = SLTFind(plist, 3);
	SLTErase(&plist,pos);
	pos = NULL;
	SLTPrint(plist);
	//if (pos)
	//{
	//	printf("找到了\n");
	//}
	//else
	//{
	//	printf("找不到\n");
	//}
}


//void Swap1(int* p1, int* p2)
//{
//	int tmp = *p1;
//	*p1 = *p2;
//	*p2 = tmp;
//}
//
//void Swap2(int** pp1, int** pp2)
//{
//	int* tmp = *pp1;
//	*pp1 = *pp2;
//	*pp2 = *tmp;
//}

int main()
{
	//TestSList1();
	//TestSList2();
	//TestSList3();
	//TestSList4();
	//int a = 0, b = 1;
	//Swap1(&a, &b);
	//int* ptr1 = &a, * ptr2 = &b;
	//Swap2(&ptr1, &ptr2);
	//TestSList5();
	TestSList6();

	return 0;
}