#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//移除链表元素1
//struct ListNode* removeElements(struct ListNode* head, int val) {
//    if (head == NULL)
//    {
//        return NULL;
//    }
//    struct ListNode* cur = head;
//    struct ListNode* tail = NULL;
//    struct ListNode* newhead = NULL;
//    while (cur)
//    {
//        if (cur->val != val)
//        {
//            if (tail == NULL)
//            {
//                newhead = tail = cur;
//            }
//            else
//            {
//                tail->next = cur;
//                tail = tail->next;
//            }
//            cur = cur->next;
//        }
//        else
//        {
//            struct ListNode* next = cur->next;
//            free(cur);
//            cur = next;
//        }
//    }
//    if (tail)
//        tail->next = NULL;
//    return newhead;
//}

//移除链表元素2
//struct ListNode* removeElements(struct ListNode* head, int val) {
//    struct ListNode* tail = head;
//    while (head && head->val == val)
//    {
//        tail = head;
//        head = head->next;
//        free(tail);
//    }
//    struct ListNode* cur = head;
//    while (cur && (tail = cur->next))
//    {
//        if (tail->val == val)
//        {
//            cur->next = tail->next;
//            free(tail);
//        }
//        else
//        {
//            cur = cur->next;
//        }
//    }
//    return head;
//}

//移除链表元素（哨兵位）
//struct ListNode* removeElements(struct ListNode* head, int val) {
//    struct ListNode* cur = head;
//    struct ListNode* tail = NULL;
//    struct ListNode* guard = NULL;
//    guard = tail = (struct ListNode*)malloc(sizeof(struct ListNode));
//    while (cur)
//    {
//        if (cur->val != val)
//        {
//            tail->next = cur;
//            tail = tail->next;
//            cur = cur->next;
//        }
//        else
//        {
//            struct ListNode* next = cur->next;
//            free(cur);
//            cur = next;
//        }
//    }
//    tail->next = NULL;
//    struct ListNode* newhead = guard->next;
//    free(guard);
//    return newhead;
//}

//反转链表1（头插）
//struct ListNode* reverseList(struct ListNode* head) {
//    struct ListNode* cur = head;
//    struct ListNode* rhead = NULL;
//    while (cur)
//    {
//        struct ListNode* next = cur->next;
//        cur->next = rhead;//断开链表
//        rhead = cur;
//        cur = next;
//    }
//    return rhead;
//}

//反转链表（换方向）
//struct ListNode* reverseList(struct ListNode* head) {
//    if (head == NULL)
//    {
//        return NULL;
//    }
//    struct ListNode* n1, * n2, * n3;
//    n1 = NULL;
//    n2 = head;
//    n3 = n2->next;
//    while (n2)
//    {
//        n2->next = n1;//反转
//        n1 = n2;
//        n2 = n3;
//        if (n3)
//        {
//            n3 = n3->next;
//        }
//    }
//    return n1;
//}

//合并两个链表
//struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2) {
//    if (list1 == NULL)
//    {
//        return list2;
//    }
//    if (list2 == NULL)
//    {
//        return list1;
//    }
//    struct ListNode* head, * tail;
//    head = tail = NULL;
//    while (list1 && list2)
//    {
//        if (list1->val < list2->val)
//        {
//            if (tail == NULL)
//            {
//                head = tail = list1;
//            }
//            else
//            {
//                tail->next = list1;
//                tail = tail->next;
//            }
//            list1 = list1->next;
//        }
//        else
//        {
//            if (tail == NULL)
//            {
//                head = tail = list2;
//            }
//            else
//            {
//                tail->next = list2;
//                tail = tail->next;
//            }
//            list2 = list2->next;
//        }
//    }
//    if (list1)//如果list1不为空，将list1剩余值直接尾插
//    {
//        tail->next = list1;
//    }
//    if (list2)//同上
//    {
//        tail->next = list2;
//    }
//    return head;
//}

//合并两个链表（哨兵位）
//struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2) {
//    struct ListNode* tail;
//    struct ListNode* guard;
//    guard = tail = (struct ListNode*)malloc(sizeof(struct ListNode));
//    guard->next = NULL;
//    while (list1 && list2)
//    {
//        if (list1->val < list2->val)
//        {
//            tail->next = list1;
//            tail = tail->next;
//            list1 = list1->next;
//        }
//        else
//        {
//            tail->next = list2;
//            tail = tail->next;
//            list2 = list2->next;
//        }
//    }
//    if (list1)
//        tail->next = list1;
//    if (list2)
//        tail->next = list2;
//    struct ListNode* head = guard->next;
//    free(guard);
//
//    return head;
//}

//链表的中间节点
//struct ListNode* middleNode(struct ListNode* head) {
//    int count = 0;
//    int k = 0;
//    struct ListNode* cur = head;
//
//    while (head)
//    {
//        head = head->next;
//        count++;
//    }
//
//    while (k < count / 2)
//    {
//        k++;
//        cur = cur->next;
//    }
//    return cur;
//
//}

//中间结点2（快慢指针）
//struct ListNode* middleNode(struct ListNode* head) {
//    struct ListNode* fast, * slow;
//    slow = fast = head;
//    while (fast && fast->next)
//    {
//        slow = slow->next;
//        fast = fast->next->next;
//    }
//    return slow;
//}

//链表中倒数第k个结点
//struct ListNode* FindKthToTail(struct ListNode* pListHead, int k) {
//    // write code here
//    struct ListNode* tail = pListHead;
//    int count = 0;
//    while (tail)
//    {
//        tail = tail->next;
//        count++;
//    }
//    for (int i = 0; i < count - k; i++)
//    {
//        pListHead = pListHead->next;
//    }
//    if (k > count)
//    {
//        return NULL;
//    }
//    return pListHead;
//}

//链表中倒数第k个结点（快慢指针）
//struct ListNode* FindKthToTail(struct ListNode* pListHead, int k) {
//    // write code here
//    struct ListNode* slow, * fast;
//    slow = fast = pListHead;
//    //先走k步
//	  while (k--)
//    {
//		//链表可能没有k步长
//        if (fast == NULL)
//        {
//            return NULL;
//        }
//        fast = fast->next;
//    }
//    while (fast)
//    {
//        slow = slow->next;
//        fast = fast->next;
//    }
//
//    return slow;
//}

//链表分（哨兵位）
//ListNode* partition(ListNode* pHead, int x) {
//    // write code here
//    struct ListNode* lesshead, * lesstail, * greaterhead, * greatertail;
//    lesshead = lesstail = (struct ListNode*)malloc(sizeof(struct ListNode));
//    greaterhead = greatertail = (struct ListNode*)malloc(sizeof(struct ListNode));
//    lesstail->next = greatertail->next = NULL;
//    struct ListNode* cur = pHead;
//    while (cur)
//    {
//        if (cur->val < x)
//        {
//            lesstail->next = cur;
//            lesstail = lesstail->next;
//        }
//        else
//        {
//            greatertail->next = cur;
//            greatertail = greatertail->next;
//        }
//        //此时，并不改变被移数据的next地址
//        cur = cur->next;
//    }
//    lesstail->next = greaterhead->next;
//    greatertail->next = NULL;
//    pHead = lesshead->next;
//    free(lesshead);
//    free(greaterhead);
//    return pHead;
//}

//链表分割（无头链表）
//ListNode* partition(ListNode* pHead, int x) {
//    // write code here
//    struct ListNode* lessHead, * lessTail, * greaterHead, * greaterTail;
//    lessHead = lessTail = greaterHead = greaterTail = NULL;
//    struct ListNode* cur = pHead;
//    if (pHead == NULL || pHead->next == NULL)
//    {
//        return pHead;
//    }
//    while (cur)
//    {
//        if (cur->val < x)
//        {
//            if (lessHead == NULL && lessTail == NULL)
//            {
//                lessHead = lessTail = cur;
//            }
//            else
//            {
//                lessTail->next = cur;
//                lessTail = cur;
//            }
//        }
//        else
//        {
//            if (greaterHead == NULL && greaterTail == NULL)
//            {
//                greaterHead = greaterTail = cur;
//            }
//            else
//            {
//                greaterTail->next = cur;
//                greaterTail = cur;
//            }
//        }
//        cur = cur->next;
//    }
//
//    if (greaterTail)
//    {
//        greaterTail->next = NULL;
//    }
//    if (lessTail)
//    {
//        lessTail->next = greaterHead;
//    }
//    else
//    {
//        lessHead = greaterHead;
//    }
//    pHead = lessHead;
//    return pHead;
//}

//链表的回文结构（找到中间结点，反转后半部分链表，对照数据）
//class PalindromeList {
//public:
//
//    struct ListNode* middleNode(struct ListNode* head) {
//        struct ListNode* fast, * slow;
//        slow = fast = head;
//        while (fast && fast->next)
//        {
//            slow = slow->next;
//            fast = fast->next->next;
//        }
//        return slow;
//    }
//
//    struct ListNode* reverseList(struct ListNode* head) {
//        struct ListNode* cur = head;
//        struct ListNode* rhead = NULL;
//        while (cur)
//        {
//            struct ListNode* next = cur->next;
//            cur->next = rhead;//断开链表
//            rhead = cur;
//            cur = next;
//        }
//        return rhead;
//    }
//    bool chkPalindrome(ListNode* A) {
//        // write code here
//        struct ListNode* mid = middleNode(A);
//        struct ListNode* rhead = reverseList(mid);
//
//        while (A && rhead)
//        {
//            if (A->val != rhead->val)
//                return false;
//            A = A->next;
//            rhead = rhead->next;
//        }
//        return true;
//    }
//};

//相交链表
//int Count(struct ListNode* cur)
//{
//    int x = 0;
//    while (cur)
//    {
//        cur = cur->next;
//        x++;
//    }
//    return x;
//}
//
//struct ListNode* ComNewNode(struct ListNode* head, int a)
//{
//    while (a--)
//    {
//        head = head->next;
//    }
//    return head;
//}
//
//struct ListNode* getIntersectionNode(struct ListNode* headA, struct ListNode* headB) {
//    int a1 = Count(headA);
//    int a2 = Count(headB);
//    if (a1 < a2)
//    {
//        headB = ComNewNode(headB, a2 - a1);
//    }
//    else if (a1 > a2)
//    {
//        headA = ComNewNode(headA, a1 - a2);
//    }
//    while (headA != headB)
//    {
//        headA = headA->next;
//        headB = headB->next;
//    }
//    return headA;
//}

//struct ListNode* getIntersectionNode(struct ListNode* headA, struct ListNode* headB) {
//    struct ListNode* h1 = headA;
//    struct ListNode* h2 = headB;
//    while (h1)
//    {
//        while (h2)
//        {
//            if (h1 != h2)
//            {
//                h2 = h2->next;
//            }
//            else
//            {
//                return h1;
//            }
//        }
//        h1 = h1->next;
//        h2 = headB;
//    }
//    return NULL;
//}

//struct ListNode* getIntersectionNode(struct ListNode* headA, struct ListNode* headB) {
//    struct ListNode* curA = headA, * curB = headB;
//    int lenA = 0;
//    int lenB = 0;
//    while (curA->next)
//    {
//        ++lenA;
//        curA = curA->next;
//    }
//
//    while (curB->next)
//    {
//        ++lenB;
//        curB = curB->next;
//    }
//
//    if (curA != curB)
//    {
//        return NULL;
//    }
//    int gap = abs(lenA - lenB);
//    struct ListNode* longList = headA, * shortList = headB;
//    if (lenB > lenA)
//    {
//        longList = headB;
//        shortList = headA;
//    }
//
//    while (gap--)
//    {
//        longList = longList->next;
//    }
//
//    while (longList != shortList)
//    {
//        longList = longList->next;
//        shortList = shortList->next;
//    }
//    return longList;
//}