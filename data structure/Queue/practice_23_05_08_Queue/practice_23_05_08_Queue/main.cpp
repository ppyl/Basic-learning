#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<assert.h>
#include<stdlib.h>

typedef int STDataType;
typedef struct Stack
{
	STDataType* a;
	int capacity;
	int top;
}ST;

void StackInit(ST* ps);
void StackDestroy(ST* ps);
void StackPush(ST* ps, STDataType x);
void StackPop(ST* ps);
STDataType StackTop(ST* ps);

bool StackEmpty(ST* ps);
int StackSize(ST* ps);

void StackInit(ST* ps)
{
	assert(ps);

	ps->a = (STDataType*)malloc(sizeof(STDataType) * 4);
	if (ps->a == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	ps->capacity = 4;
	ps->top = 0;
}

void StackDestroy(ST* ps)
{
	assert(ps);

	free(ps->a);
	ps->a = NULL;
	ps->top = ps->capacity = 0;
}

void StackPush(ST* ps, STDataType x)
{
	assert(ps);

	//����
	if (ps->top == ps->capacity)
	{
		STDataType* tmp = (STDataType*)realloc(ps->a, ps->capacity * 2 * sizeof(STDataType));
		if (tmp == NULL)
		{
			perror("realloc fail");
			exit(-1);
		}
		ps->a = tmp;
		ps->capacity *= 2;
	}
	ps->a[ps->top] = x;
	ps->top++;
}

void StackPop(ST* ps)
{
	assert(ps);
	assert(!StackEmpty(ps));

	ps->top--;
}

STDataType StackTop(ST* ps)
{
	assert(ps);
	assert(!StackEmpty(ps));

	return ps->a[ps->top - 1];
}

bool StackEmpty(ST* ps)
{
	assert(ps);

	return ps->top == 0;
}

int StackSize(ST* ps)
{
	assert(ps);

	return ps->top;
}

typedef struct {
	ST pushst;
	ST popst;
}MyQueue;

bool myQueueEmpty(MyQueue* obj);

MyQueue* myQueueCreate() {
	MyQueue* pq = (MyQueue*)malloc(sizeof(MyQueue));
	StackInit(&(pq->pushst));
	StackInit(&(pq->popst));

	return pq;
}

void myQueuePush(MyQueue* obj, int x) {
	assert(obj);

	StackPush(&(obj->pushst), x);
}

int myQueuePop(MyQueue* obj) {
	assert(obj);
	assert(!myQueueEmpty(obj));

	int peek = myQueuePeek(obj);
	StackPop(&(obj->popst));

	return peek;
}

int myQueuePeek(MyQueue* obj) {
	assert(obj);
	assert(!myQueueEmpty(obj));

	//popΪ�գ�������
	if (StackEmpty(&(obj->popst)))
	{
		while (!StackEmpty(&(obj->pushst)));
		{
			StackPush(&(obj->popst), StackTop(&(obj->pushst)));
			StackPop(&(obj->pushst));
		}
	}

	return StackTop(&(obj->popst));
}

bool myQueueEmpty(MyQueue* obj) {
	assert(obj);

	return StackEmpty(&(obj->pushst)) && StackEmpty(&(obj->popst));
}

void myQueueFree(MyQueue* obj) {
	assert(obj);

	StackDestroy(&(obj->pushst));
	StackDestroy(&(obj->popst));

	free(obj);
}

int main()
{
	return 0;
}