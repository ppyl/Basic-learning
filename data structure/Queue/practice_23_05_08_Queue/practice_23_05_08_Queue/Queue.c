#define _CRT_SECURE_NO_WARNINGS 1

//用队列实现栈
//typedef int QDataType;
//
//typedef struct QueueNode
//{
//	QDataType data;
//	struct QDataNode* next;
//}QNode;
//
//typedef struct Queue
//{
//	QNode* head;
//	QNode* tail;
//	int size;
//}Queue;
//
//void QueueInit(Queue* pq);
//void QueueDestroy(Queue* pq);
//void QueuePush(Queue* pq, QDataType x);
//void QueuePop(Queue* pq);
//QDataType QueueFront(Queue* pq);
//QDataType QueueBack(Queue* pq);
//bool QueueEmpty(Queue* pq);
//int QueueSize(Queue* pq);
//
//void QueueInit(Queue* pq)
//{
//	assert(pq);
//
//	pq->head = NULL;
//	pq->tail = NULL;
//	pq->size = 0;
//}
//
//void QueueDestroy(Queue* pq)
//{
//	assert(pq);
//
//	QNode* cur = pq->head;
//	while (cur != NULL)
//	{
//		QNode* del = cur;
//		cur = cur->next;
//
//		free(del);
//	}
//	pq->head = NULL;
//	pq->tail = NULL;
//	pq->size = 0;
//}
//
//void QueuePush(Queue* pq, QDataType x)
//{
//	assert(pq);
//
//	QNode* newnode = (QNode*)malloc(sizeof(QNode));
//	if (newnode == NULL)
//	{
//		perror("malloc fail");
//		exit(-1);
//	}
//	newnode->data = x;
//	newnode->next = NULL;
//
//	if (pq->tail == NULL)
//	{
//		pq->head = pq->tail = newnode;
//	}
//	else
//	{
//		pq->tail->next = newnode;
//		pq->tail = newnode;
//	}
//	pq->size++;
//}
//
//void QueuePop(Queue* pq)
//{
//	assert(pq);
//	assert(!QueueEmpty(pq));
//
//	if (pq->head->next == NULL)
//	{
//		free(pq->head);
//		pq->head = pq->tail = NULL;
//	}
//	else
//	{
//		QNode* del = pq->head;
//		pq->head = pq->head->next;
//		free(del);
//	}
//
//	pq->size--;
//}
//
//QDataType QueueFront(Queue* pq)
//{
//	assert(pq);
//	assert(!QueueEmpty(pq));
//
//	return pq->head->data;
//}
//
//QDataType QueueBack(Queue* pq)
//{
//	assert(pq);
//	assert(!QueueEmpty(pq));
//
//	return pq->tail->data;
//}
//
//bool QueueEmpty(Queue* pq)
//{
//	assert(pq);
//
//	return pq->head == NULL && pq->tail == NULL;
//}
//
//int QueueSize(Queue* pq)
//{
//	assert(pq);
//
//	return pq->size;
//}
//
//typedef struct {
//	Queue q1;
//	Queue q2;
//} MyStack;
//
//
//MyStack* myStackCreate() {
//	MyStack* obj = (MyStack*)malloc(sizeof(MyStack));
//	QueueInit(&(obj->q1));
//	QueueInit(&(obj->q2));
//
//	return obj;
//}
//
//void myStackPush(MyStack* obj, int x) {
//	if (!QueueEmpty(&(obj->q1)))
//	{
//		QueuePush(&(obj->q1), x);
//	}
//	else
//	{
//		QueuePush(&(obj->q2), x);
//
//	}
//}
//
//int myStackPop(MyStack* obj) {
//	Queue* emptyQ = &(obj->q1);
//	Queue* nonemptyQ = &(obj->q2);
//	if (!QueueEmpty(&(obj->q1)))
//	{
//		emptyQ = &(obj->q2);
//		nonemptyQ = &(obj->q1);
//	}
//
//	//非空队列的前n-1个数据倒入空队列
//	while (QueueSize(nonemptyQ) > 1)
//	{
//		QueuePush(emptyQ, QueueFront(nonemptyQ));
//		QueuePop(nonemptyQ);
//	}
//	int top = QueueFront(nonemptyQ);
//	QueuePop(nonemptyQ);
//
//	return top;
//}
//
//int myStackTop(MyStack* obj) {
//	if (!QueueEmpty(&(obj->q1)))
//	{
//		return QueueBack(&(obj->q1));
//	}
//	else
//	{
//		return QueueBack(&(obj->q2));
//	}
//}
//
//bool myStackEmpty(MyStack* obj) {
//	return QueueEmpty(&(obj->q1)) && QueueEmpty(&(obj->q2));
//}
//
//void myStackFree(MyStack* obj) {
//	QueueDestroy(&(obj->q1));
//	QueueDestroy(&(obj->q2));
//
//	free(obj);
//}
//
///**
// * Your MyStack struct will be instantiated and called as such:
// * MyStack* obj = myStackCreate();
// * myStackPush(obj, x);
//
// * int param_2 = myStackPop(obj);
//
// * int param_3 = myStackTop(obj);
//
// * bool param_4 = myStackEmpty(obj);
//
// * myStackFree(obj);
//*/

//用栈实现队列
//typedef int STDataType;
//typedef struct Stack
//{
//	STDataType* a;
//	int capacity;
//	int top;
//}ST;
//
//void StackInit(ST* ps);
//void StackDestroy(ST* ps);
//void StackPush(ST* ps, STDataType x);
//void StackPop(ST* ps);
//STDataType StackTop(ST* ps);
//
//bool StackEmpty(ST* ps);
//int StackSize(ST* ps);
//
//void StackInit(ST* ps)
//{
//	assert(ps);
//
//	ps->a = (STDataType*)malloc(sizeof(STDataType) * 4);
//	if (ps->a == NULL)
//	{
//		perror("malloc fail");
//		exit(-1);
//	}
//	ps->capacity = 4;
//	ps->top = 0;
//}
//
//void StackDestroy(ST* ps)
//{
//	assert(ps);
//
//	free(ps->a);
//	ps->a = NULL;
//	ps->top = ps->capacity = 0;
//}
//
//void StackPush(ST* ps, STDataType x)
//{
//	assert(ps);
//
//	//扩容
//	if (ps->top == ps->capacity)
//	{
//		STDataType* tmp = (STDataType*)realloc(ps->a, ps->capacity * 2 * sizeof(STDataType));
//		if (tmp == NULL)
//		{
//			perror("realloc fail");
//			exit(-1);
//		}
//		ps->a = tmp;
//		ps->capacity *= 2;
//	}
//	ps->a[ps->top] = x;
//	ps->top++;
//}
//
//void StackPop(ST* ps)
//{
//	assert(ps);
//	assert(!StackEmpty(ps));
//
//	ps->top--;
//}
//
//STDataType StackTop(ST* ps)
//{
//	assert(ps);
//	assert(!StackEmpty(ps));
//
//	return ps->a[ps->top - 1];
//}
//
//bool StackEmpty(ST* ps)
//{
//	assert(ps);
//
//	return ps->top == 0;
//}
//
//int StackSize(ST* ps)
//{
//	assert(ps);
//
//	return ps->top;
//}
//
//typedef struct {
//	ST pushst;
//	ST popst;
//}MyQueue;
//
//bool myQueueEmpty(MyQueue* obj);
//
//MyQueue* myQueueCreate() {
//	MyQueue* pq = (MyQueue*)malloc(sizeof(MyQueue));
//	StackInit(&(pq->pushst));
//	StackInit(&(pq->popst));
//
//	return pq;
//}
//
//void myQueuePush(MyQueue* obj, int x) {
//	assert(obj);
//
//	StackPush(&(obj->pushst), x);
//}
//
//int myQueuePop(MyQueue* obj) {
//	assert(obj);
//	assert(!myQueueEmpty(obj));
//
//	int peek = myQueuePeek(obj);
//	StackPop(&(obj->popst));
//
//	return peek;
//}
//
//int myQueuePeek(MyQueue* obj) {
//	assert(obj);
//	assert(!myQueueEmpty(obj));
//
//	//pop为空，倒数据
//	if (StackEmpty(&(obj->popst)))
//	{
//		while (!StackEmpty(&(obj->pushst)))
//		{
//			StackPush(&(obj->popst), StackTop(&(obj->pushst)));
//			StackPop(&(obj->pushst));
//		}
//	}
//
//	return StackTop(&(obj->popst));
//}
//
//bool myQueueEmpty(MyQueue* obj) {
//	assert(obj);
//
//	return StackEmpty(&(obj->pushst)) && StackEmpty(&(obj->popst));
//}
//
//void myQueueFree(MyQueue* obj) {
//	assert(obj);
//
//	StackDestroy(&(obj->pushst));
//	StackDestroy(&(obj->popst));
//
//	free(obj);
//}

/**
 * Your MyQueue struct will be instantiated and called as such:
 * MyQueue* obj = myQueueCreate();
 * myQueuePush(obj, x);

 * int param_2 = myQueuePop(obj);

 * int param_3 = myQueuePeek(obj);

 * bool param_4 = myQueueEmpty(obj);

 * myQueueFree(obj);
*/

/**
 * Your MyQueue struct will be instantiated and called as such:
 * MyQueue* obj = myQueueCreate();
 * myQueuePush(obj, x);

 * int param_2 = myQueuePop(obj);

 * int param_3 = myQueuePeek(obj);

 * bool param_4 = myQueueEmpty(obj);

 * myQueueFree(obj);
*/

//循环队列
//typedef struct {
//    int* a;
//    int front;
//    int rear;
//    int k;
//} MyCircularQueue;
//
//MyCircularQueue* myCircularQueueCreate(int k) {
//    MyCircularQueue* obj = (MyCircularQueue*)malloc(sizeof(MyCircularQueue));
//    //多开辟一个空间，解决判满问题
//    obj->a = (int*)malloc(sizeof(int) * (k + 1));
//    obj->front = obj->rear = 0;
//    //存储的最大数据个数
//    obj->k = k;
//    return obj;
//}
//
//bool myCircularQueueIsEmpty(MyCircularQueue* obj) {
//    assert(obj);
//    return obj->front == obj->rear;
//}
//
//bool myCircularQueueIsFull(MyCircularQueue* obj) {
//    assert(obj);
//    return ((obj->rear + 1) % (obj->k + 1)) == obj->front;
//}
//
//bool myCircularQueueEnQueue(MyCircularQueue* obj, int value) {
//    assert(obj);
//    if (myCircularQueueIsFull(obj))
//        return false;
//
//    obj->a[obj->rear++] = value;
//    obj->rear %= (obj->k + 1);
//    return true;
//}
//
//bool myCircularQueueDeQueue(MyCircularQueue* obj) {
//    assert(obj);
//    if (myCircularQueueIsEmpty(obj))
//        return false;
//
//    obj->front++;
//    obj->front %= (obj->k + 1);
//    return true;
//}
//
//int myCircularQueueFront(MyCircularQueue* obj) {
//    assert(obj);
//    if (myCircularQueueIsEmpty(obj))
//        return -1;
//    else
//        return obj->a[obj->front];
//}
//
//int myCircularQueueRear(MyCircularQueue* obj) {
//    assert(obj);
//    if (myCircularQueueIsEmpty(obj))
//        return -1;
//    else
//        return obj->a[(obj->rear + obj->k) % (obj->k + 1)];
//}
//
//void myCircularQueueFree(MyCircularQueue* obj) {
//    free(obj->a);
//    free(obj);
//}
//
///**
// * Your MyCircularQueue struct will be instantiated and called as such:
// * MyCircularQueue* obj = myCircularQueueCreate(k);
// * bool param_1 = myCircularQueueEnQueue(obj, value);
//
// * bool param_2 = myCircularQueueDeQueue(obj);
//
// * int param_3 = myCircularQueueFront(obj);
//
// * int param_4 = myCircularQueueRear(obj);
//
// * bool param_5 = myCircularQueueIsEmpty(obj);
//
// * bool param_6 = myCircularQueueIsFull(obj);
//
// * myCircularQueueFree(obj);
//*/