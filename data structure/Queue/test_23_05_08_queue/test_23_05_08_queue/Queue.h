#pragma once

#include<stdio.h>
#include<assert.h>
#include<stdbool.h>
#include<stdlib.h>

typedef int QDataType;
//链式结构：表示队列
typedef struct QueueNode
{
	QDataType data;
	struct QDataNode* next;
}QNode;

//队列的结构
typedef struct Queue
{
	QNode* head;
	QNode* tail;
	int size;
}Queue;

void QueueInit(Queue* pq);
void QueueDestroy(Queue* pq);
//入队（尾插）
void QueuePush(Queue* pq, QDataType x);
//出队（头删）
void QueuePop(Queue* pq);

//取队头队尾数据
QDataType QueueFront(Queue* pq);
QDataType QueueBack(Queue* pq);

bool QueueEmpty(Queue* pq);
int QueueSize(Queue* pq);