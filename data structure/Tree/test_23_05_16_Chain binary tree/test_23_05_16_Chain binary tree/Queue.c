#define _CRT_SECURE_NO_WARNINGS 1

#include"Queue.h"

void QueueInit(Queue* pq)
{
	assert(pq);

	pq->head = NULL;
	pq->tail = NULL;
	pq->size = 0;
}

void QueueDestroy(Queue* pq)
{
	assert(pq);

	QNode* cur = pq->head;
	while (cur != NULL) 
	{
		QNode* del = cur;
		cur = cur->next;

		free(del);
		////del = NULL;//局部变量无需置空
	}
	pq->head = NULL;
	pq->tail = NULL;
	pq->size = 0;
}

//放入结构体中，指针属于结构体（改变的本身就是结构体指针），所以可以改变指针而不需要二级指针或者返回值
void QueuePush(Queue* pq, QDataType x)
{
	assert(pq);

	//申请新结点
	QNode* newnode = (QNode*)malloc(sizeof(QNode));
	if (newnode == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	newnode->data = x;
	newnode->next = NULL;

	if (pq->tail == NULL)
	{
		pq->head = pq->tail = newnode;
	}
	else
	{
		pq->tail->next = newnode;
		pq->tail = newnode;
	}
	pq->size++;
}

void QueuePop(Queue* pq)
{
	assert(pq);
	assert(!QueueEmpty(pq));
	
	if (pq->head->next == NULL)
	{
		free(pq->head);
		pq->head = pq->tail = NULL;
	}
	else
	{
		QNode* del = pq->head;
		pq->head = pq->head->next;
		free(del);
	}

	pq->size--;
	//QNode* del = pq->head;
	//pq->head = pq->head->next;
	//free(del);
	//if (pq->head == NULL)
	//{
	//	pq->tail == NULL;
	//}
}

QDataType QueueFront(Queue* pq)
{
	assert(pq);	
	assert(!QueueEmpty(pq));

	return pq->head->data;
}

QDataType QueueBack(Queue* pq)
{
	assert(pq);
	assert(!QueueEmpty(pq));

	return pq->tail->data;
}

bool QueueEmpty(Queue* pq)
{
	assert(pq);

	return pq->head == NULL && pq->tail == NULL;
}

int QueueSize(Queue* pq)
{
	assert(pq);

	//int size = 0;
	//QNode* cur = pq->head;
	//while (cur)
	//{
	//	cur = cur->next;
	//	++size;
	//}
	//return size;
	return pq->size;
}