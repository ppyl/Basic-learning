#define _CRT_SECURE_NO_WARNINGS 1
#include"Chain binary tree.h"
#include"Queue.h"

//int size = 0;//全局变量可以随时置0
//
//void TreeSize(CBTNode* root)
//{
//	if (root == NULL)
//		return;
//	size++;
//	TreeSize(root->left);
//	TreeSize(root->right);
//}

//节点数
int TreeSize2(CBTNode* root)
{
	return root == NULL ? 0 : 
		TreeSize2(root->left) + TreeSize2(root->right) + 1;
}

//叶子节点数
int TreeLeafSize(CBTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}
	if (root->left == NULL && root->right == NULL)
	{
		return 1;
	}
	return TreeLeafSize(root->left) + TreeLeafSize(root->right);
}

//前序遍历
void PrevOrder(CBTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		//回到调用本次函数的地方
		return;
	}

	printf("%d ", root->data);
	PrevOrder(root->left);
	PrevOrder(root->right);
}

//中序遍历
void InOrder(CBTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	InOrder(root->left);
	printf("%d ", root->data);
	InOrder(root->right);
}

void PostOrder(CBTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	PostOrder(root->left);
	PostOrder(root->right);
	printf("%d ", root->data);
}

//树的高度/深度
// err 极大的浪费了空间（未保存之前统计的数值）
//int TreeHeight(CBTNode* root)
//{
//	if (root == NULL)
//		return 0;
//	return TreeHeight(root->left) > TreeHeight(root->right)
//		? TreeHeight(root->left) + 1 : TreeHeight(root->right) + 1;
//}

int TreeHeight(CBTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}
	int leftHeight = TreeHeight(root->left);
	int rightHeight = TreeHeight(root->right);
	return leftHeight > rightHeight ? leftHeight + 1 : rightHeight + 1;
}

int TreeKLevelSize(CBTNode* root, int k)
{
	if (root == NULL)
	{
		return 0;
	}
	if (k == 1)
	{
		return 1;
	}
	return TreeKLevelSize(root->left, k - 1)
		+ TreeKLevelSize(root->right, k - 1);
}

//二叉树查找值为x的结点
CBTNode* TreeFind(CBTNode* root, CBTDataType x)
{
	if (root == NULL)
	{
		return NULL;
	}
	if (root->data == x)
	{
		return root;
	}
	CBTNode* left = TreeFind(root->left, x);
	if (left)
		return left;

	CBTNode* right = TreeFind(root->right, x);
	if (right)
		return right;
	return NULL;
}

//层序遍历
void LevelOrder(CBTNode* root)
{
	Queue q;
	QueueInit(&q);
	int levelSize = 0;
	if (root)
	{
		QueuePush(&q, root);
		levelSize = 1;
	}
	while (!QueueEmpty(&q))
	{
		while (levelSize--)
		{
			CBTNode* front = QueueFront(&q);
			printf("%d ", front->data);
			QueuePop(&q);

			if (front->left)
			{
				QueuePush(&q, front->left);
			}
			if (front->right)
			{
				QueuePush(&q, front->right);
			}
		}
		printf("\n");
		levelSize = QueueSize(&q);
	}
	printf("\n");
	QueueDestroy(&q);
}

//判断完全二叉树
bool TreeComplete(CBTNode* root)
{
	Queue q;
	QueueInit(&q);
	if (root)
	{
		QueuePush(&q, root);
	}
	while (!QueueEmpty(&q))
	{
		CBTNode* front = QueueFront(&q);
		QueuePop(&q);
		if (front == NULL)
		{
			break;
		}
		else
		{
			QueuePush(&q, front->left);
			QueuePush(&q, front->right);
		}
	}
	while (!QueueEmpty(&q))
	{
		CBTNode* front = QueueFront(&q);
		QueuePop(&q);
		if (front != NULL)
		{
			QueueDestroy(&q);
			return false;
		}
	}
	QueueDestroy(&q);
	return true;
}

//后序遍历销毁
void TreeDestroy(CBTNode* root)
{
	if (root == NULL)
	{
		return;
	}
	TreeDestroy(root->left);
	TreeDestroy(root->right);
	free(root);
}

CBTNode* BuyCBTNode(CBTDataType x)
{
	CBTNode* node = (CBTNode*)malloc(sizeof(CBTNode));
	if (malloc == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	node->data = x;
	node->left = node->right = NULL;
	return node;
}