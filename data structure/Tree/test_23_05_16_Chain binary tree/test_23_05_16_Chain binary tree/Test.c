#define _CRT_SECURE_NO_WARNINGS 1
#include"Chain binary tree.h"
#include"Queue.h"

extern int size;

int main()
{
	CBTNode* n1 = BuyCBTNode(1);
	CBTNode* n2 = BuyCBTNode(2);
	CBTNode* n3 = BuyCBTNode(3);
	CBTNode* n4 = BuyCBTNode(4);
	CBTNode* n5 = BuyCBTNode(5);
	CBTNode* n6 = BuyCBTNode(6);
	CBTNode* n7 = BuyCBTNode(7);
	n2->right = n7;
	n1->left = n2;
	n1->right = n4;
	n2->left = n3;
	n4->left = n5;
	n4->right = n6;

	PrevOrder(n1);
	printf("\n");

	InOrder(n1);
	printf("\n");

	PostOrder(n1);
	printf("\n");

	/*size = 0;
	TreeSize(n1);
	printf("TreeSize=%d\n", size);

	size = 0;
	TreeSize(n1);
	printf("TreeSize=%d\n", size);

	size = 0;
	TreeSize(n1);
	printf("TreeSize=%d\n", size);*/

	printf("TreeSize2=%d\n", TreeSize2(n1));
	printf("TreeSize2=%d\n", TreeSize2(n1));
	printf("TreeSize2=%d\n", TreeSize2(n1));
	
	printf("TreeLeafSize=%d\n", TreeLeafSize(n1));
	printf("TreeHeight=%d\n", TreeHeight(n1));
	printf("TreeKLevelSize=%d\n", TreeKLevelSize(n1, 3));
	printf("TreeKLevelSize=%d\n", TreeKLevelSize(n1, 4));
	printf("TreeFind=%p\n", TreeFind(n1, 7));

	LevelOrder(n1);
	
	printf("TreeComplete=%d\n", TreeComplete(n1));

	TreeDestroy(n1);
	//函数内形参无法改变实参，需要手动置空（也可以使用二级指针在函数内置空）
	n1 = NULL; 

	return 0;
}