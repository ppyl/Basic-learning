#pragma once

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdbool.h>

typedef int CBTDataType;
typedef struct ChainBinaryTreeNode
{
	CBTDataType data;
	struct ChainBinaryTreeNode* left;
	struct ChainBinaryTreeNode* right;
}CBTNode;

void TreeSize(CBTNode* root);
int TreeSize2(CBTNode* root);
int TreeLeafSize(CBTNode* root);
int TreeHeight(CBTNode* root);
int TreeKLevelSize(CBTNode* root, int k);

void PrevOrder(CBTNode* root);
void InOrder(CBTNode* root);
void PostOrder(CBTNode* root); 
void LevelOrder(CBTNode* root);

bool TreeComplete(CBTNode* root);

void TreeDestroy(CBTNode* root);
CBTNode* BuyCBTNode(CBTDataType x);

CBTNode* TreeFind(CBTNode* root, CBTDataType x);