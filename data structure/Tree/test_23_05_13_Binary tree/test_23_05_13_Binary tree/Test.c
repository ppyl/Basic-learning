#define _CRT_SECURE_NO_WARNINGS 1
#include<time.h>
#include"Heap.h"

void TestHeap1()
{
	int a[] = { 2,3,4,7,8,9,4,3,6,8 };
	HP hp;
	HeapInit(&hp);
	for (int i = 0; i < sizeof(a) / sizeof(int); i++)
	{
		HeapPush(&hp, a[i]);
	}
	HeapPrint(&hp);

	int k = 5;
	while (k--)
	{
		printf("%d ",HeapTop(&hp));
		//取一个，堆顶更新一次
		HeapPop(&hp);
	}
	//HeapPop(&hp);
	//HeapPrint(&hp);

	HeapDestroy(&hp);
}

void TestHeap2()
{
	int a[] = { 2,3,4,7,8,9,4,3,6,8 };
	HP hp;
	HeapInit(&hp);
	for (int i = 0; i < sizeof(a) / sizeof(int); i++)
	{
		HeapPush(&hp, a[i]);
	}
	HeapPrint(&hp);

	int k = 5;
	//排序
	//时间复杂度O(N)
	//前提需要一个堆的数据结构
	while (!HeapEmpty(&hp))
	{
		printf("%d ", HeapTop(&hp));
		//取一个，堆顶更新一次
		HeapPop(&hp);
	}
	//HeapPop(&hp);
	//HeapPrint(&hp);

	HeapDestroy(&hp);
}

void TestHeap3()
{
	int a[] = { 2,3,4,7,8,9,4,3,6,8 };
	HP hp;
	HeapCreate(&hp, a, sizeof(a) / sizeof(int));
	HeapPrint(&hp);

	HeapDestroy(&hp);
}

//只实现堆排序（升序）
//小堆实现，需要开辟新空间存放排好的堆，不开辟的话会打乱堆，需要重新建堆更加麻烦
//大堆实现，排好大堆后，将根节点和最后一个节点互换
//此时，不考虑最后一个节点，向下调整，如此反复因为只换一个数据所以是logN次
//升序建议大堆，降序建议小堆

//O(N*logN)
void HeapSort(int* a, int n)
{
	//向上调整建堆 O(N*logN)
	//for (int i = 1; i < n; ++i)
	//{
	//	AdjustUp(a, i);
	//}
	//向下调整建堆 O(N	)
	//找最后一个父亲
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(a, n, i);
	}
	//O(N*logN)
	int end = n - 1;
	while (end > 0)
	{
		Swap(&a[0], &a[end]);
		AdjustDown(a, end, 0);
		--end;
	}
}

void TestHeap4()
{
	int a[] = { 2,3,4,7,8,9,4,3,6,8 };
	HeapSort(a, sizeof(a) / sizeof(int));
	for (int i = 0; i < sizeof(a) / sizeof(int); i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

//topK问题
void TestHeap5()
{
	int minHeap[5];
	FILE* font = fopen("data.txt", "r");
	if (font == NULL)
	{
		perror("fopen fail");
		return;
	}
	int k = 5;
	for (int i = 0; i < k; ++i)
	{
		fscanf(font, "%d", &minHeap[i]);
	}
	//建小堆
	for (int i = (k - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(minHeap, k, i);
	}
	int val = 0;
	while (fscanf(font, "%d", &val) != EOF)
	{
		if (val > minHeap[0])
		{
			minHeap[0] = val;
			AdjustDown(minHeap, k, 0);
		}
	}
	for (int i = 0; i < k; i++)
	{
		printf("%d ", minHeap[i]);
	}
	printf("\n");
	fclose(font);
}

void TestHeap6()
{
	//int n = 10000;
	//int k = 5;
	int n, k;
	printf("请输入n和k:>");
	//不需要加空格，scanf可以自动能够识别
	//（前提控制台手动输入时需要加入空格）
	scanf("%d%d", &n, &k);
	srand(time(0));
	FILE* fin = fopen("data.txt", "w");
	if (fin == NULL)
	{
		perror("fopen fail");
		return;
	}

	int randK = k;
	for (size_t i = 0; i < n; i++)
	{
		int val = rand() % n;
		fprintf(fin, "%d ", val);
	}

	fclose(fin);

	//int minHeap[5];
	FILE* font = fopen("data.txt", "r");
	if (font == NULL)
	{
		perror("fopen fail");
		return;
	}
	int* minHeap = malloc(sizeof(int) * k);
	if (minHeap == NULL)
	{
		perror("malloc fail");
		return;
	}

	for (int i = 0; i < k; i++)
	{
		fscanf(font, "%d", &minHeap[i]);
	}
	//建小堆
	for (int i = (k - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(minHeap, k, i);
	}
	int val = 0;
	while (fscanf(font, "%d", &val) != EOF)
	{
		if (val > minHeap[0])
		{
			minHeap[0] = val;
			AdjustDown(minHeap, k, 0);
		}
	}
	for (int i = 0; i < k; ++i)
	{
		printf("%d ", minHeap[i]);
	}
	printf("\n");
	fclose(font);
}

int main()
{
	//TestHeap1();
	//TestHeap2();
	//TestHeap3();
	//TestHeap4();
	//TestHeap5();
	TestHeap6();
	return 0;
}