#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>
#include<string.h>

//堆 - 优先级队列
typedef int HPDataType;
typedef struct Heap
{
	HPDataType* a;
	int size;
	int capacity;
}HP;

void HeapCreate(HP* php, HPDataType* a, int n);

void HeapPrint(HP* php);
void HeapInit(HP* php);
void HeapDestroy(HP* php);
// 保持结构仍为堆（O(logN)）
void HeapPush(HP* php,HPDataType x);
// 删除堆顶的数据，结构仍为堆（O(logN)）
void HeapPop(HP* php);

HPDataType HeapTop(HP* php);

void Swap(HPDataType* p1, HPDataType* p2);
void AdjustUp(HPDataType* a, int child);
void AdjustDown(HPDataType* a, int n, int parent);
int HeapSize(HP* php);
bool HeapEmpty(HP* php);