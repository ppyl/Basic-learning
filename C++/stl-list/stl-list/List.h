#pragma once
#include<assert.h>

namespace pyl
{
	template <class T>
	struct list_node
	{
		list_node<T>* _next;
		list_node<T>* _prev;
		T _data;

		// 构造函数（匿名对象方便匹配类型）
		list_node(const T& x = T())
			:_next(nullptr)
			,_prev(nullptr)
			,_data(x)
		{

		}
	};
	
	// 迭代器可以是原生指针
	// 也可以是自定义类型对原生指针的的封装，模拟指针的行为
	// 模板复用(const)
	template <class T, class Ref, class Ptr>
	struct __list_iterator
	{
		typedef list_node<T> node;
		typedef __list_iterator<T , Ref, Ptr> self;
		node* _node;

		// 用结点指针构造迭代器
		__list_iterator(node* n)
			:_node(n)
		{}

		Ref operator*()
		{
			return _node->_data;
		}

		// 方便访问自定义类型（流输出...）
		Ptr operator->()
		{
			return &_node->_data;
		}

		self& operator++()
		{
			_node = _node->_next;

			return *this;
		}

		// 后置
		self operator++(int)
		{
			self tmp(*this);
			_node = _node->_next;

			return tmp;
		}

		self& operator--()
		{
			_node = _node->_prev;

			return *this;
		}

		// 后置
		self operator--(int)
		{
			self tmp(*this);
			_node = _node->_prev;

			return tmp;
		}

		bool operator!=(const self& s)
		{
			return _node != s._node;
		}

		bool operator==(const self& s)
		{
			return _node == s._node;
		}
	};

	//// 内容不可变
	//template <class T>
	//struct __list_const_iterator
	//{
	//	typedef list_node<T> node;
	//	typedef __list_const_iterator<T> self;
	//	node* _node;

	//	// 用结点指针构造迭代器
	//	__list_const_iterator(node* n)
	//		:_node(n)
	//	{}

	//	const T& operator*()
	//	{
	//		return _node->_data;
	//	}
	//	
	//	self& operator++()
	//	{
	//		_node = _node->_next;

	//		return *this;
	//	}

	//	// 后置
	//	self operator++(int)
	//	{
	//		self tmp(*this);
	//		_node = _node->_next;

	//		return tmp;
	//	}

	//	self& operator--()
	//	{
	//		_node = _node->_prev;

	//		return *this;
	//	}

	//	// 后置
	//	self operator--(int)
	//	{
	//		self tmp(*this);
	//		_node = _node->_prev;

	//		return tmp;
	//	}

	//	bool operator!=(const self& s)
	//	{
	//		return _node != s._node;
	//	}

	//	bool operator==(const self& s)
	//	{
	//		return _node == s._node;
	//	}
	//};

	template <class T>
	class list
	{
		typedef list_node<T> node;
	public:
		// 内嵌类型（自定义类型）
		typedef __list_iterator<T, T&, T*> iterator;
		typedef __list_iterator<T, const T&, const T*> const_iterator;

		iterator begin()
		{
			//iterator it = _head->next;
			//return it;
			return iterator(_head->_next);
		}

		// 指针不可变，内容拷贝可改
		const_iterator begin() const
		{
			return const_iterator(_head->_next);
		}

		iterator end()
		{
			return iterator(_head);
		}

		const_iterator end() const
		{
			return const_iterator(_head);
		}

		void empty_init()
		{
			_head = new node;
			_head->_next = _head;
			_head->_prev = _head;
		}

		list()
		{
			empty_init();
		}

		template <class InputIterator>
		list(InputIterator first, InputIterator last)
		{
			empty_init();

			while (first != last)
			{
				push_back(*first);
				++first;
			}
		}

		// 拷贝构造
		list(const list<T>& lt)
		{
			empty_init();
			for (auto e : lt)
			{
				push_back(e);
			}
		}

		void swap(list<T>& tmp)
		{
			std::swap(_head, tmp._head); 
		}

		list(const list<T>& lt)
		{
			list<T> tmp(lt.begin(), lt.end());
			swap(tmp);
		}

		~list()
		{
			clear();
			delete _head;
			_head = nullptr;
		}

		void clear()
		{
			iterator it = begin();
			while (it != end())
			{
				it = erase(it);
				// 后置++使用临时拷贝，迭代器不会失效
				//erase(it++);
			}
		}

		void push_back(const T& x)
		{
			//node* tail = _head->_prev;
			//// 调用构造函数构造结点
			//node* new_node = new node(x);

			//tail->_next = new_node;
			//new_node->_prev = tail;
			//new_node->_next = _head;
			//_head->_prev = new_node;

			insert(end(), x);
		}

		void push_front(const T& x)
		{
			insert(begin(), x);
		}

		void pop_back()
		{
			erase(--end());
		}

		void pop_front()
		{
			erase(begin());
		}

		void insert(iterator pos,const T& x)
		{
			node* cur = pos._node;
			node* prev = cur->_prev;

			node* new_node = new node(x);

			prev->_next = new_node;
			new_node->_prev = prev;
			new_node->_next = cur;
			cur->_prev = new_node;
		}

		iterator erase(iterator pos)
		{
			assert(pos != end());

			node* prev = pos._node->_prev;
			node* next = pos._node->_next;

			prev->_next = next;
			next->_prev = prev;
			delete pos._node;

			return iterator(next);
		}
	private:
		node* _head;
	};

	void print_list(const list<int>& lt)
	{
		list<int>::const_iterator it = lt.begin();
		// 比较头结点
		while (it != lt.end())
		{
			//(*it) *= 2;
			cout << *it << " ";
			++it;
		}
		cout << endl;
	}

	void test_list1()
	{
		list<int> lt;
		lt.push_back(1);
		lt.push_back(2);
		lt.push_back(3);
		lt.push_back(4);
		
		// 可读可写
		// int*
		list<int>::iterator it = lt.begin();
		// 比较头结点
		while (it != lt.end())
		{
			(*it) *= 2;
			cout << *it << " ";
			++it;
		}
		cout << endl;

		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;

		print_list(lt);
 	}

	// 公有（可以不重载实现流插入...）- 私有必须重载
	struct A
	{
		int _a1;
		int _a2;

		A(int a1 = 0, int a2 = 0)
			:_a1(a1)
			,_a2(a2)
		{

		}
	};

	void test_list2()
	{
		// 自定义类型作为list模板参数时，使用->模拟解引用
		list<A> lt;
		lt.push_back(A(1,1)); 
		lt.push_back(A(1,2));
		lt.push_back(A(1,3));

		list<A>::iterator it = lt.begin();
		while (it != lt.end())
		{
			// 先解引用再调对象
			cout << (*it)._a1 << "、" << (*it)._a2 << endl;
			// 重载->模拟解引用
			cout << it->_a1 << "、" << it->_a2 << endl;
			// 未优化前
			cout << it.operator->()->_a1 << "、" << it.operator->()->_a2 << endl;
			++it;
		}
		cout << endl;
	}

	void test_list3()
	{
		list<int> lt;
		lt.push_back(1);
		lt.push_back(2);
		lt.push_back(3);
		lt.push_back(4);

		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;

		//list<int>::iterator pos = lt.begin();
		auto pos = lt.begin();
		++pos;
		lt.insert(pos, 20);

		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;

		lt.push_back(10);
		lt.push_front(10);

		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;

		lt.pop_back();
		lt.pop_front();

		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	void test_list4()
	{
		list<int> lt;
		lt.push_back(1);
		lt.push_back(2);
		lt.push_back(3);
		lt.push_back(4);
		
		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;
		
		lt.clear();

		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;

		lt.push_back(5);
		lt.push_back(7);
		lt.push_back(9);
		lt.push_back(8);
	
		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	void test_list5()
	{
		// const属性在定义时并未实现
		//const list<int> lt;
		list<int> lt;
		lt.push_back(1);
		lt.push_back(2);
		lt.push_back(3);
		lt.push_back(4);
		lt.push_back(5);

		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;
	
		list<int> lt2(lt);

		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;
	}
}