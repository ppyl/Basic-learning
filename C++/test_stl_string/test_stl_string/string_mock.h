#pragma once
#include<assert.h>

namespace pyl
{
	class string
	{
	public:
		typedef char* iterator;
		typedef const char* const_iterator;
		
		iterator begin()
		{
			return _str;
		}

		iterator end()
		{
			return _str + _size;
		}

		const_iterator begin() const
		{
			return _str;
		}

		const_iterator end() const
		{
			return _str + _size;
		}

		//无参构造
		//string()
		//	:_str(new char[1])
		//	, _size(0)
		//	, _capacity(0)
		//{
		//	_str[0] = '\0';
		//}

		//缺省构造
		//string(const char* str = '\0') - err char类型不能转换为char*
		string(const char* str = "") // 字符串可以
			:_size(strlen(str))
		{
			_capacity = _size == 0 ? 3 : _size;
			_str = new char[_capacity + 1];
			strcpy(_str, str);
		}

		//拷贝构造
		string(const string& s)
			:_size(s._size)
			,_capacity(s._capacity)
		{
			_str = new char[s._capacity + 1];
			//strcpy(_str, s._str);
		}

		string& operator=(const string& s)
		{
			if (this != &s)
			{
				//深拷贝
				char* tmp = new char[s._capacity + 1];
				strcpy(tmp, s._str);
				delete[] _str;
				_str = tmp;

				_size = s._size;
				_capacity = s._capacity;
			}
			return *this;
		}

		//析构
		~string()
		{
			delete[] _str;
			_str = nullptr;
			_size = _capacity = 0;
		}

		//C输出
		const char* c_str()
		{
			return _str;
		}

		//[]
		const char& operator[](size_t pos) const
		{
			assert(pos < _size);
			return _str[pos];
		}

		char& operator[](size_t pos)
		{
			assert(pos < _size);
			return _str[pos];
		}

		size_t size() const
		{
			return _size;
		}

		//const
		bool operator>(const string& s)const
		{
			return strcmp(_str, s._str) == 0;
		}

		//扩容+初始化（可扩容，可添加数据）
		void resize(size_t n, char ch = '\0')
		{
			//三种情况
			//1. n < size
			//2. size < n < capacity
			//3. n > capacity
			if (n <= _size)
			{
				//删除数据，保留前n个
				_size = n;
				_str[_size] = '\0';
			}
			else
			{
				if (n > _capacity)
				{
					reserve(n);
				}

				size_t i = _size;
				while (i < n)
				{
					_str[i] = ch;
					++i;
				}
				_size = n;
				_str[_size] = '\0';
			}
		}

		//扩容
		void reserve(size_t n)
		{
			//避免缩容
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;

				_capacity = n;
			}
		}

		//指定位置插入数据（字符）
		//下标一般使用无符号类型
		void insert(size_t pos, char ch)
		{
			assert(pos <= _size);
			if (_size + 1 > _capacity)
			{
				reserve(2 * _capacity);
			}
			// err
			// 无符号类型条件中end会成为-1，导致出错
			//size_t end = _size;
			//while (end >= pos)
			//{
			//	_str[end + 1] = _str[end];
			//	--end;
			//}

			size_t end = _size + 1;
			while (end > pos)
			{
				_str[end] = _str[end - 1];
				--end;
			}

			_str[pos] = ch;
			++_size;
		}

		// 指定位置插入字符串
		string& insert(size_t pos, const char* str)
		{
			assert(pos <= _size);
			size_t len = strlen(str);
			if (_size + len > _capacity)
			{
				reserve(_size + len);
			}
			
			//挪动数据
			size_t end = _size + len;
			while (end > pos + len - 1)
			{
				_str[end] = _str[end - len];
				--end;
			}

			//size_t end = _size;
			//for (size_t i = 0; i < _size + 1; i++)
			//{
			//	_str[end + len] = _str[end];
			//	--end;
			//}
			//拷贝插入
			strncpy(_str + pos, str, len);
			_size += len; 

			return *this;
		}

		//pos位置删n个字符
		string& erase(size_t pos, size_t len = npos)
		{
			if (len == npos || pos + len >= _size)
			{
				_str[pos] = '\0';
				_size = pos;
			}
			else
			{
				strcpy(_str + pos, _str + pos + len);
				_size -= len;
			}
			//接收删除的值
			return *this;
		}

 		//从指定位置查找
		size_t find(char ch, size_t pos = 0)
		{
			for (size_t i = pos; i < _size; ++i)
			{
				if (_str[i] == ch)
				{
					return i;
				}
			}
			return npos;
		}

		void push_back(char ch)
		{
			//if (_size + 1 > _capacity)
			//{
			//	reserve(_capacity * 2);
			//}

			//_str[_size] = ch;
			//++_size;

			//_str[_size] = '\0';
			insert(_size, ch);
		}

		void append(const char* str)
		{
			//size_t len = strlen(str);
			//if (_size + len > _capacity)
			//{
			//	reserve(_size + len);
			//}

			//strcpy(_str + _size, str);
			////strcat(_str, str);
			//_size += len;
			insert(_size, str);
		}

		size_t capacity() const
		{
			return _capacity;
		}

		size_t find(const char* str, size_t pos = 0)
		{
			assert(pos < _size);

			char* p = strstr(_str + pos, str);
			if (p == nullptr)
			{
				return npos;
			}
			else
			{
				return p - _str;
			}
		}

		//设计内存拷贝，效率低
		//swap(s1, s2);
		//s1.swap(s2);
		
		void swap(string& s)
		{
			std::swap(_str, s._str);
			std::swap(_capacity, s._capacity);
			std::swap(_size, s._size);
		}

	private:
		char* _str;
		size_t _size;
		size_t _capacity;
		//静态成员变量在静态区属于整个类（所有对象）
		//此处不能给缺省值
		//声明
		static const size_t npos;
		//static size_t npos;
		//特殊情况(const + 整型)
		/*static const size_t npos = -1;*/
	};
	const size_t string::npos = -1;
	//size_t string::npos = -1;

	ostream& operator<<(ostream& out, const string& s)
	{
		for (auto ch : s)
		{
			out << ch;
		}
		return out;
	}

	istream& operator>>(istream& in, const string& s)
	{
		char ch;
		in >> ch;
		//while (ch != '\n') //getline
		while (ch != ' ' && ch != '\n')
		{
			s += ch;
			in >> ch; 
		}
		return in;
	}

	void test_string1()
	{
		string s1;
		string s2("***");

		cout << s1.c_str() << endl;
		cout << s2.c_str() << endl;

		//s2[0]++;

		//cout << s1.c_str() << endl;
		//cout << s2.c_str() << endl;

	}

	//std自带reserve不会缩容
	void test_string6()
	{
		//std::string s1("**********************");
		string s1("**********************");
		cout << s1.capacity() << endl;
		s1.reserve(10);
		cout << s1.capacity() << endl;
	}

	void test_string7()
	{
		string s1;
		s1.resize(20, '*');
		cout << s1.c_str() << endl;
		//在后面添加十个x
		s1.resize(30, 'x');
		cout << s1.c_str() << endl;
		//只保留前十个，容量不变
		//不会缩容（避免多次拷贝，申请新空间）
		s1.resize(10);
		cout << s1.c_str() << endl;
	}

	void test_string8()
	{
		string s1("***");
		s1.insert(0, 'x');
		cout << s1.c_str() << endl;

		s1.insert(3, 'x');
		cout << s1.c_str() << endl;

		s1.insert(3, "yyy");
		cout << s1.c_str() << endl;

		s1.insert(0, "yyy");
		cout << s1.c_str() << endl;
	}

	void test_string9()
	{
		string s1("0123456789");
		cout << s1.c_str() << endl;

		s1.erase(4, 3);
		cout << s1.c_str() << endl;

		s1.erase(4, 30);
		cout << s1.c_str() << endl;

		s1.erase(2);
		cout << s1.c_str() << endl;
	}

	//c_str识别为const char*，遇到\0终止
	//流插入重载（不仅友元一种形式）
	void test_string10()
	{
		string s1("******");
		s1 += '\0';
		s1 += "xxx";

		cout << s1 << endl;
		cout << s1.c_str() << endl;
	}
}