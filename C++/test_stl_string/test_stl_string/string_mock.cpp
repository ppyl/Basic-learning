#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
#include<string.h>
using namespace std;
#include"string_mock.h"

int main()
{
	try 
	{ 
		//pyl::test_string1();
		//pyl::test_string2();
		//pyl::test_string3();
		//pyl::test_string4();
		//pyl::test_string5();
		//pyl::test_string6();
		//pyl::test_string7();
		pyl::test_string8();


	}
	catch (const exception& e)
	{
		cout << e.what() << endl;
	}

	return 0;
}

int main()
{
	pyl::string s1;
	std::string s2;
	cout << sizeof(s1) << endl; //12
	cout << sizeof(s2) << endl; //28

	// std,对象上存的是指针，字符串都在堆上面（以空间换时间）
	pyl::string s3("******");
	std::string s4("********");
	cout << sizeof(s3) << endl; //12
	cout << sizeof(s4) << endl; //28
}