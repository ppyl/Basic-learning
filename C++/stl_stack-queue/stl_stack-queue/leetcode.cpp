//#include<iostream>
//#include<vector>
//#include<algorithm>
//
//using namespace std;
//
//class Solution {
//public:
//        static int eliminateMaximum(vector<int>& dist, vector<int>& speed) {
//        int n = dist.size();
//        vector<int> arrival_time(n);
//
//        // 统计到达所需时间
//        for (int i = 0; i < n; ++i)
//        {
//            arrival_time[i] = (dist[i] - 1) / speed[i];
//        }
//
//        // 升序所需时间
//        sort(arrival_time.begin(), arrival_time.end());
//
//        // 利用所需时间判断是否按时消灭怪物
//        for (int i = 0; i < n; ++i)
//        {
//            if (arrival_time[i] < i)
//            {
//                return i;
//            }
//        }
//        return n;
//    }
//};
//
//int main()
//{
//    int arr1[] = { 1,3,4 };
//    int arr2[] = { 1,1,1 };
//
//    vector<int> a1(arr1,arr1+3);
//    vector<int> a2(arr2,arr2+3);
//    Solution::eliminateMaximum(a1, a2);
//
//    return 0;
//}