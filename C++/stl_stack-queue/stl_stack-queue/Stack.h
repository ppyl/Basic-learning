#pragma once

namespace pyl
{
	// 适配器模式（配接器模式）
	// 模板也可以使用缺省，通常为类型
	//template<class T, class Container = vector<T>>
	template<class T, class Container>
	class stack
	{
	public:
		void push(const T& x)
		{
			_con.push_back(x);
		}

		void pop()
		{
			_con.pop_back();
		}

		const T& top()
		{
			return _con.back();
		}

		size_t size()
		{
			return _con.size();
		}

		bool empty()
		{
			return _con.empty();
		}
	private:
		Container _con;
	};

	void test_stack()
	{
		//stack<int,vector<int>> st;
		stack<int,list<int>> st;

		//stack<int> st;
		st.push(1);
		st.push(2);
		st.push(3);
		st.push(4);
		
		while (!st.empty())
		{
			cout << st.top() << " ";
			st.pop();
		}
		cout << endl;
	}
}
