#include<iostream>
#include<functional>
#include<stack>
#include<vector>
#include<list>

using namespace std;

#include"Stack.h"
#include"Queue.h"
#include"PriorityQueue.h"



void stack1()
{
	stack<int> st;
	st.push(1);
	st.push(2);
	st.push(3);
	st.push(4);

	while (!st.empty())
	{
		cout << st.top() << " ";
		st.pop();
	}
	cout << endl;
}

// 使用模板时，注意链接比较麻烦可以直接写道头文件中
//int main()
//{
//	//stack1();
//	pyl::test_stack();
//	pyl::test_queue();
//	return 0;
//}

int main()
{
	pyl::test_priority_queue();

	return 0;
}