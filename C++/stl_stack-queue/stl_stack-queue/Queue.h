#pragma once

namespace pyl
{
	// 适配器模式（配接器模式）
	// 模板也可以使用缺省，通常为类型
	//template<class T, class Container>
	template<class T, class Container = list<T>>
	class queue
	{
	public:
		void push(const T& x)
		{
			_con.push_back(x);
		}

		void pop()
		{
			_con.pop_front();
		}

		const T& front()
		{
			return _con.front();
		}

		const T& back()
		{
			return _con.back();
		}

		size_t size()
		{
			return _con.size();
		}

		bool empty()
		{
			return _con.empty();
		}
	private:
		Container _con;
	};

	void test_queue()
	{
		// 一般不使用vector实现队列（vector没有头插，使用insert效率太低）
		//queue<int, vector<int>> q;
		//queue<int,list<int>> q;

		queue<int> q;
		q.push(1);
		q.push(2);
		q.push(3);
		q.push(4);

		while (!q.empty())
		{
			cout << q.front() << " ";
			q.pop();
		}
		cout << endl;
	}
}
