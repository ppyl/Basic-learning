//#include<iostream>
//#include<set>
//#include<map>
//using namespace std;
//
//// set -- key (双向迭代器)
//// map -- key/value (双向迭代器)
//
//void test_set1()
//{
//	set<int> s1;
//	s1.insert(1);
//	s1.insert(2);
//	s1.insert(7);
//	s1.insert(7);
//	s1.insert(5);
//	s1.insert(9);
//
//	for (auto e : s1)
//	{
//		// 重复值，再次插入失败
//		cout << e << " ";
//	}
//	cout << endl; 
//
//	set<int>::iterator it1 = s1.begin();
//	while (it1 != s1.end())
//	{
//		// 搜索树不允许修改key，可能会破坏搜索的规则
//		//*it1 += 1;
//		cout << *it1 << " ";
//		it1++;
//	}
//	cout << endl;
//}
//
//void test_set2()
//{
//	// 排序 + 去重
//	set<int> s1;
//	s1.insert(1);
//	s1.insert(2);
//	s1.insert(7);
//	s1.insert(7);
//	s1.insert(5);
//	s1.insert(9);
//
//	int x;
//	while (cin >> x)
//	{
//		auto ret = s1.find(x);
//		if (ret != s1.end())
//		{
//			cout << "在" << endl;
//		}
//		else
//		{
//			cout << "不在" << endl;
//		}
//	}
//
//	//while (cin >> x)
//	//{
//	//	if (s1.count(x))
//	//	{
//	//		cout << "在" << endl;
//	//	}
//	//	else
//	//	{
//	//		cout << "不在" << endl;
//	//	}
//	//}
//
//}
//
//void test_set3()
//{
//	// 排序
//	multiset<int> s2;
//	s2.insert(1);
//	s2.insert(2);
//	s2.insert(7);
//	s2.insert(7);
//	s2.insert(5);
//	s2.insert(9);
//
//	for (auto e : s2)
//	{
//		// 允许键值冗余
//		cout << e << " ";
//	}
//	cout << endl;
//
//	set<int>::iterator it1 = s2.begin();
//	while (it1 != s2.end())
//	{
//		// 搜索树不允许修改key，可能会破坏搜索的规则
//		//*it1 += 1;
//		cout << *it1 << " ";
//		it1++;
//	}
//	cout << endl;
//
//	auto ret = s2.find(7);
//	while (ret != s2.end() && *ret == 7)
//	{
//		// 搜索树不允许修改key，可能会破坏搜索的规则
//		//*it1 += 1;
//		cout << *ret << " ";
//		++ret;
//	}
//	cout << endl;
//
//	cout << s2.count(1) << endl;
//	cout << s2.count(7) << endl;
//}
//
//void test_map1()
//{
//	map<string, string> dict;
//	// pair<string，string> 匿名函数
//	//dict.insert(pair<string, string>("sort", "排序"));
//	// make_pair 函数模板(还是去调匿名构造并返回)
//	// 自动推导类型
//	dict.insert(make_pair("sort", "排序"));
//	dict.insert(make_pair("string", "字符串"));
//	dict.insert(make_pair("count", "计数"));
//
//	map<string, string>::iterator dit = dict.begin();
//	while (dit != dict.end())
//	{
//		cout << (*dit).first << "：" << (*dit).second << endl;
//		++dit;
//	}
//	cout << endl;
//}
//
//void test_map2()
//{
//	map<string, string> dict;
//	// pair<string，string> 匿名函数
//	//dict.insert(pair<string, string>("sort", "排序"));
//	// make_pair 函数模板(还是去调匿名构造并返回)
//	// 自动推导类型
//	dict.insert(make_pair("sort", "排序"));
//	dict.insert(make_pair("string", "字符串"));
//	dict.insert(make_pair("count", "计数"));
//	// 插入失败
//	dict.insert(make_pair("string", "字符串"));
//	dict.insert(make_pair("count", "计数"));
//
//	dict["left"];
//	dict["right"] = "右边";
//	dict["string"] = "(字符串)";
//	cout << dict["string"] << endl;
//	cout << dict["string"] << endl;
//
//	map<string, string>::iterator dit = dict.begin();
//	while (dit != dict.end())
//	{
//		//cout << (*dit).first << "：" << (*dit).second << endl;
//		cout << dit->first << "：" << dit->second << endl;
//		++dit;
//	}
//	cout << endl;
//}
//
//void test_map3()
//{
//	string arr[] = { "西瓜","苹果","西瓜","香蕉","苹果","西瓜","梨","苹果","香蕉","梨" };
//	map<string, int> countMap;
//
//	//for (auto& e : arr)
//	//{
//	//	auto ret = countMap.find(e);
//	//	if (ret == countMap.end())
//	//	{
//	//		countMap.insert(make_pair(e, 1));
//	//	}
//	//	else
//	//	{
//	//		ret->second++;
//	//	}
//	//}
//
//	for (auto& e : arr)
//	{
//		// 返回value(second)的引用，修改value(second)
//		countMap[e]++;
//	}
//
//	for (auto& kv : countMap)
//	{
//		cout << kv.first << "：" << kv.second << endl;
//	}
//}
//
//// map中pair的模板参数（调用匿名函数）
////template <class T1, class T2>
////struct pair
////{
////	typedef T1 first_type;
////	typedef T2 second_type;
////	T1 first;
////	T2 second;
////	pair() : first(T1()), second(T2())
////	{}
////	pair(const T1& a, const T2& b) : first(a), second(b)
////	{}
////};
//
//// 1、插入 value（此事是缺省值）
//// 2、修改
//// 3、插入 + 修改 （返回value的引用，可以修改）
//// 4、查找
////V& operator[](const K& key)
////{
////	pair<iterator, bool> ret = insert(make_pair(key, V()));
////	return ret.first->second;
////}
//
//int main()
//{
//	//test_set3();
//	test_map2();
//
//	return 0;
//}