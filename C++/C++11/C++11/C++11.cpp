#include<iostream>
#include<thread>
#include<vector>
#include<Windows.h>
using namespace std;

//void Func(int n)
//{
//	for (int i = 0; i < n; i++)
//	{
//		cout << i << " ";
//	}
//	cout << endl;
//}
//
//int main()
//{
//	thread t1(Func, 10);
//	thread t2(Func, 20);
//}

//int main()
//{
//	size_t m;
//	cin >> m;
//	vector<thread> vthds(m);
//	for (int i = 0; i < m; i++)
//	{
//		size_t n;
//		cin >> n;
//		// 此处支持移动赋值
//		vthds[i] = thread([i, n]() {
//			for (int j = 0; j < n; j++)
//			{
//				//由于get_id调用的是线程对象（成员函数）构造过程中无法调用
//				//所以打造一个类，封装一个全局方法（子命名空间）
//				cout << this_thread::get_id() << i << ":" << j << endl;
//				
//				this_thread::sleep_for(chrono::milliseconds(200));
//			}
//			cout << endl;
//			});
//		//cout << vthds[i].get_id() << endl;
//	}
//
//	for (auto& t : vthds)
//	{
//		t.join();
//	}
//	return 0;
//}

//int main()
//{
//	int x = 0, y = 1;
//	int m = 0, n = 1;
//
//	auto swap1 = [](int& rx, int& ry)
//	{
//		int tmp = rx;
//		rx = ry;
//		ry = tmp;
//	};
//
//	cout << sizeof(swap1) << endl;
//
//	return 0;
//}

//#include <mutex>
//
//int x = 0;
//mutex mtx;
//
//// 对于线程，该函数不在同一地址
//void Func(int n)
//{
//	// 并行 （1.加锁解锁频繁。2.切换上下文消耗）
//	// 当中间操作更复杂时，并行优势出现，上下文消耗次数大量减少
//	//for (int i = 0; i < n; i++)
//	//{
//	//	mtx.lock();
//	//	++x;
//	//	mtx.unlock();
//	//}
//
//	// 快
//	// 串行
//	mtx.lock();
//	for (int i = 0; i < n; i++)
//	{
//		++x;
//	}
//	mtx.unlock();
//}
//
//int main()
//{
//	thread t1(Func, 10000000);
//	thread t2(Func, 20000000);
//	size_t begin = clock();
//
//	t1.join();
//	t2.join();
//	size_t end = clock();
//
//	cout << x << endl;
//	cout << end - begin << endl;
//	return 0;
//}

//#include <mutex>
//
//int main()
//{
//	int n = 1000000;
//	int x = 0;
//	mutex mtx;
//	size_t begin = clock();
//	
//	thread t1([&, n]() {
//		mtx.lock();
//		for (int i = 0; i < n; i++)
//		{
//			++x;
//		}
//		mtx.unlock();
//		});
//
//	thread t2([&, n]() {
//		for (int i = 0; i < n; i++)
//		{
//			mtx.lock();
//			++x;
//			mtx.unlock();
//		}
//		});
//
//	t1.join();
//	t2.join();
//	size_t end = clock();
//
//	cout << x << endl;
//	cout << end - begin << endl;
//	return 0;
//}

//#include <mutex>
//
//int x = 0;
//recursive_mutex mtx;
//
//void Func(int n)
//{
//	if (n == 0)
//		return;
//	
//	mtx.lock();
//	++x;
//	Func(n - 1);
//	mtx.unlock();
//}
//
//int main()
//{
//	thread t1(Func, 10000);
//	thread t2(Func, 20000);
//	
//	t1.join();
//	t2.join();
//
//	return 0;
//}

#include <mutex>

int x = 0;
mutex mtx;

void Func(int n)
{
	for (int i = 0; i < n; i++)
	{
		try {
			mtx.lock();
			++x;
			// ...抛异常
			if (rand() % 3 == 0)
			{
				throw exception("抛异常");
			}
			mtx.unlock();
		}
		catch (const exception& e)
		{
			cout << e.what() << endl;
		}
	}
}

int main()
{
	thread t1(Func, 10);
	thread t2(Func, 10);

	t1.join();
	t2.join();

	return 0;
}