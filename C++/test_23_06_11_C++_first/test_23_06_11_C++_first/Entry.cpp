// C++兼容C语法

//#include<stdio.h>
//
//int main()
//{
//	printf("***\n");
//
//	return 0;
//}

//// 包含输入输出流相关操作函数库
//#include<iostream> 
//// 所有C++标准库类型和函数都在std命名空间或嵌套在std内的命名空间中进行声明
//using namespace std; 
//
//int main()
//{
//	// 使用命名空间中的输出流操作
//	std::cout << "***" << std::endl;
//
//	return 0;
//}

//#include<stdio.h>
//#include<stdlib.h>
//
//// rand作为库函数，无法定义使用
//// 典型的C语言命名冲突问题，C++提出namespace命名空间解决
//int rand = 10;
//
//int main()
//{
//	printf("%d\n", rand);
//	
//	return 0;
//}

//#include<stdio.h>
//#include<stdlib.h>
//
//// 局部域/全局域
//// 1.使用
//// 2.生命周期
//int a = 2;
//
//void f1() 
//{
//	int a = 1;
//
//	printf("%d\n", a); // 1
//	// ::域作用限定符
//	printf("%d\n", ::a); // 2
//}
//
//void f2()
//{
//	int a = 3;
//}
//
//int main()
//{
//	printf("%d\n", a); // 2
//	f1();
//
//	return 0;
//}

//#include"List.h"
//#include"Queue.h"
//
//int main()
//{
//	struct AQueue::Node node1;
//	struct BList::pyl::Node node1;
//
//	AQueue::min++;
//	BList::pyl::min++;
//
//	return 0;
//}

//#include<iostream>
//#include"Queue.h"
//using namespace std;
//using namespace AQueue;
//// 全局展开，一般不建议
//// 指定命名空间访问
//// 部分
//
//int main()
//{
//	struct Queue q;
//	QueueInit(&q);
//	QueuePush(&q, 1);
//	QueuePush(&q, 2);
//
//	std::cout << "***";
//
//	return 0;
//}

//#include<iostream>
//using namespace std;
//
//// 规范使用
//// 1.指定命名空间访问
//// 2.常用部分展开
//
//using std::cout;
//using std::endl;
//
//int main()
//{
//	cout << "***" << endl;
//	cout << "***" << endl;
//	cout << "***" << endl;
//	cout << "***" << endl;
//	cout << "***" << endl;
//
//	int i = 0;
//	std::cin >> i;
//
//	return 0;
//}