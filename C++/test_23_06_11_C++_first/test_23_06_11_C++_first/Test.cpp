//#include<iostream>
//using namespace std;
//
////int a = 2;
////
////void f1() {
////	int a = 0;
////	printf("%d\n", a);
////	printf("%d\n", ::a);
////}
////int main()
////{
////	f1();
////	return 0;
////}
//
////传引用调用
////int& Count()
////{
////	int n = 0;
////	n++;
////	//cout << &n << endl;
////
////	return n;
////}
////
////int main()
////{
////	//int& ret;//未初始化编译不通过
////	int& ret = Count();
////	//cout << &ret << endl;
////	cout << ret << endl;
////	//栈帧结束后，内存空间会返还，内容不会得到保存
////	int a = 3;
////	cout << ret << endl;
////
////	return 0;
////}
//
////int &Add(int a, int b)
////{
////	int c = a + b;
////	return c;
////}
////
//////err
////int main()
////{
////	//int ret = Add(1, 2);//将c的别名拷贝过来（取决于第一次函数栈帧销毁时的值）-不可控
////	int &ret = Add(1, 2);//为函数返回值取别名（用别名记住已被销毁空间位置）-不可控
////	//Add(3, 4);
////	//int c = 2;
////	cout << "Add(1,2) is :" << ret << endl; // 7（3）
////	//Add(3, 4);
////	cout << "Add(1,2) is :" << ret << endl; // 随机值
////
////	return 0;
////}
////
////int Count()
////{
////	int n = 1;
////	n++;
////
////	return n;//返回的是临时变量（具有常性）
////}
////
////int mian()
////{
////	int a = 1;
////	int& b = a;
////
////	//指针和引用，赋值/初始化 权限可以缩小，但是不能放大
////	//两者底层汇编模式近似，但是语法不一样（引用并不能替代指针）
////
////	//权限放大
////	//const int c = 2;
////	//int& d = c;
////
////	//const int* p1 = NULL;
////	//int* p2 = p1;
////
////	//权限保持
////	const int c = 2;
////	const int& d = c;
////
////	const int* p1 = NULL;
////	const int* p2 = p1;
////
////	//权限缩小
////	int x = 1;
////	const int& y = x;
////
////	int* p3 = NULL;
////	const int* p4 = p3;
////
////	const int& ret = Count();//需要用const int&接收
////
////	int i = 10;
////	cout << (double)i << endl;//显式类型转换
////	double dd = i;//隐式类型转换（i给一个double类型的临时变量然后赋值给dd）
////	const double& rd = i;//类型转换中涉及临时变量
////	return 0;
////}
////
////int main()
////{
////	int a = 10;
////	int& ra = a;
////	ra = 20;
////
////	int* pa = &a;
////	*pa = 20;
////
////	return 0;
////}
//
//// const和enum替代宏常量
//// inline去替代宏函数（内联函数-以空间换时间）编译出的可执行程序变大（底层实现时会反复重现-内联函数有触发条件）
//
//// 要求实现ADD宏函数
//
////#define ADD(x,y) ((x)+(y))
//inline int Add(int x, int y)
//{
//	int z = x + y;
//	return z;
//}
//
//int main()
//{
//	int a = 1;
//	int b = 1;
//
//	int ret = Add(3, 5);
//	int c = 1;
//
//	cout << ret << endl;
//	return 0;
//}
//
//int main()
//{
//	int a = 2;
//	auto b = a; //自动检测变量类型
//	auto c = &a;
//
//	cout << typeid(b).name() << endl;
//	cout << typeid(c).name() << endl;
//}
//
//int main()
//{
//	int array[] = { 1,2,3,4,5 };
//	//范围for --- 语法糖
//	//自动依次取数组中数据赋值给x对象，自动判断结束
//	//for (int x : array)
//	//for (auto& x : array) //加别名可以直接改变数组本身
//	for (auto x : array) //仅数组可用（指针不可-数组传参）
//	{
//		cout << x << endl;
//	}
//	cout << endl;
//	return 0;
//}