// C++兼容C语法

//#include<stdio.h>
//
//int main()
//{
//	printf("***\n");
//
//	return 0;
//}

//// 包含输入输出流相关操作函数库
//#include<iostream> 
//// 所有C++标准库类型和函数都在std命名空间或嵌套在std内的命名空间中进行声明
//using namespace std; 
//
//int main()
//{
//	// 使用命名空间中的输出流操作
//	std::cout << "***" << std::endl;
//
//	return 0;
//}

//#include<stdio.h>
//#include<stdlib.h>
//
//// rand作为库函数，无法定义使用
//// 典型的C语言命名冲突问题，C++提出namespace命名空间解决
//int rand = 10;
//
//int main()
//{
//	printf("%d\n", rand);
//	
//	return 0;
//}

//#include<stdio.h>
//#include<stdlib.h>
//
//// 局部域/全局域
//// 1.使用
//// 2.生命周期
//int a = 2;
//
//void f1() 
//{
//	int a = 1;
//
//	printf("%d\n", a); // 1
//	// ::域作用限定符
//	printf("%d\n", ::a); // 2
//}
//
//void f2()
//{
//	int a = 3;
//}
//
//int main()
//{
//	printf("%d\n", a); // 2
//	f1();
//
//	return 0;
//}

//#include"List.h"
//#include"Queue.h"
//
//int main()
//{
//	struct AQueue::Node node1;
//	struct BList::pyl::Node node1;
//
//	AQueue::min++;
//	BList::pyl::min++;
//
//	return 0;
//}

//#include<iostream>
//#include"Queue.h"
//using namespace std;
//using namespace AQueue;
//// 全局展开，一般不建议
//// 指定命名空间访问
//// 部分
//
//int main()
//{
//	struct Queue q;
//	QueueInit(&q);
//	QueuePush(&q, 1);
//	QueuePush(&q, 2);
//
//	std::cout << "***";
//
//	return 0;
//}

//#include<iostream>
//using namespace std;
//
//// 规范使用
//// 1.指定命名空间访问
//// 2.常用部分展开
//
//using std::cout;
//using std::endl;
//
//int main()
//{
//	cout << "***" << endl;
//	cout << "***" << endl;
//	cout << "***" << endl;
//	cout << "***" << endl;
//	cout << "***" << endl;
//
//	int i = 0;
//	std::cin >> i;
//
//	return 0;
//}

//#include<iostream>
//using namespace std;
//
//int main()
//{
//	// << 流输入
//	// endl '\n'
//	cout << "***" << endl;
//	cout << "***" << '\n';
//
//	// 自动识别类型
//	int n = 0;
//	cin >> n;
//
//	double* a = (double*)malloc(sizeof(double)*n);
//	if (a == NULL)
//	{
//		perror("malloc fail");
//		exit(-1);
//	}
//
//	//数组输入输出
//	for (int i = 0; i < n; ++i)
//	{
//		cin >> a[i];
//	}
//
//	for (int i = 0; i < n; ++i)
//	{
//		cout << a[i] << " ";
//		//printf("%.2f", a[i]);
//	}
//	cout << endl;
//
//	//cout和printf使用时的区别
//	char name[10] = "张三";
//	int age = 10;
//
//	cout << "姓名：" << name << endl;
//	cout << "年龄：" << age << endl;
//
//	printf("姓名：%s\n年龄：%d\n", name, age);
//
//	return 0;
//}

//缺省参数
//void Func(int a = 0)
//{
//	cout << a << endl;
//}
//
//int main()
//{
//	// 正常传参
//	Func(1);
//	Func();
//
//	return 0;
//}

//#include<iostream>
//using namespace std;
//
////全缺省
//void Func(int a = 10, int b = 20, int c = 30)
//{
//	cout << "a = " << a << endl;
//	cout << "b = " << b << endl;
//	cout << "c = " << c << endl;
//	cout << endl;
//}
//
//int main()
//{
//	// 使用缺省值，必须从右往左连续使用
//	Func(1, 2, 3);
//	Func(1, 2);
//	Func(1);
//	Func();
//
//	return 0;
//}

//// 半缺省参数
//// 必须从右往左连续缺省
//#include<iostream>
//using namespace std;
//
//void Func(int a, int b = 10, int c = 20)
//{
//	cout << "a = " << a << endl;
//	cout << "a = " << a << endl;
//	cout << "a = " << a << endl;
//}
//
//struct Stack
//{
//	int* a;
//	int top;
//	int capacity;
//};
//
//void StackInit(struct Stack* ps, int defaultCapacity = 4)
//{
//	ps->a = (int*)malloc(sizeof(int) * defaultCapacity);
//	if (ps->a == NULL)
//	{
//		perror("malloc fail");
//		exit(-1);
//	}
//	ps->top = 0;
//	ps->capacity = defaultCapacity;
//}
//
//int main()
//{
//	// 使用缺省值，必须从右往左连续使用
//	Func(1, 2, 3);
//	Func(1, 2);
//	Func(1);
//
//	//最多存100个数
//	Stack st1;
//	StackInit(&st1, 100);
//
//	//无法确定数据
//	Stack st2;
//	StackInit(&st2);
//
//	return 0;
//}