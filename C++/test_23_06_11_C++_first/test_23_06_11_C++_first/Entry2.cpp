#include<iostream>
using namespace std;

//#include"Test2.h"
//
//int main()
//{
//	Func();
//
//	return 0;
//}

////函数重载
//// 1.参数类型不同
//int Add(int left, int right)
//{
//	cout << "int Add(int left, int right)" << endl;
//
//	return left + right;
//}
//
//double Add(double left, double right)
//{
//	cout << "double Add(double left, double right)" << endl;
//
//	return left + right;
//}
//
//// 2.参数个数不同
//void f()
//{
//	cout << "f()" << endl;
//}
//
//void f(int a)
//{
//	cout << "f(int a)" << endl;
//}
//
////// err
////void f(int a, char b)
////{
////	cout << "f(int a, char b)" << endl;
////}
////
////int f(int a, char b)
////{
////	cout << "f(int a, char b)" << endl;
////}
//
//// 3.参数类型顺序不同
//void f(int a, char b)
//{
//	cout << "f(int a, char b)" << endl;
//}
//
//void f(char a, int b)
//{
//	cout << "f(char a, int b)" << endl;
//}

////引用
//int main()
//{
//	int i = 0;
//	// 引用定义时需要初始化
//	int& k = i;
//
//	int j = i;
//
//	cout << &i << endl;
//	cout << &k << endl;
//	cout << &j << endl;
//
//	++k;
//	++j;
//	
//	//一个变量可以有多个引用
//	//一个引用只能引用一个实体
//	int& m = i;
//	int& n = k;
//	++n;
//	cout << n << endl;
//
//	return 0;
//}

////引用实践
//void Swap(int& x, int& y)
//{
//	int tmp = x;
//	x = y;
//	y = tmp;
//}
//
//typedef struct Node
//{
//	struct Node* next;
//	int val;
//}Node, * PNode;
//
////void PushBack(Node*& phead, int x)
//void PushBack(PNode& phead, int x)
//{
//	Node* newNode = (Node*)malloc(sizeof(Node));
//	if (phead == nullptr)
//	{
//		phead = newNode;
//	}
//}
//
//typedef struct SeqList
//{
//	int* a;
//	int size;
//	int capaicty;
//}SQ;
//
//void PushBack(SQ& s, int x)
//{
//
//}
//
//int main()
//{
//	int i = 0, j = 1;
//	Swap(i, j);
//
//	cout << i << endl;
//	cout << j << endl;
//
//	int* p = &i;
//	int*& rp = p;
//
//	Node* plist = NULL;
//	PushBack(plist, 1);
//	PushBack(plist, 2);
//	PushBack(plist, 3);
//
//	return 0;
//}