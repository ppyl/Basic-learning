#pragma once

// 自定义域
namespace BList
{
	//命名空间允许嵌套使用
	namespace pyl
	{
		struct Node
		{
			struct Node* next;
			struct Node* prev;

			int val;
		};

		int min = 1;
	}
}