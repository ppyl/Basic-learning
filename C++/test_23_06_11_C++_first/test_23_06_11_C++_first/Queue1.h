//#pragma once
//
//// 命名空间 -- 命名空间域，只影响使用，不影响生命周期
//
//namespace AQueue
//{
//	// 类型
//	struct Node
//	{
//		struct Node* next;
//		int val;
//	};
//
//	struct Queue
//	{
//		struct Node* head;
//		struct Node* tail;
//	};
//
//	//函数
//	void QueueInit(struct Queue* q)
//	{
//
//	}
//
//	void QueuePush(struct Queue* q, int x)
//	{
//
//	}
//
//	// 变量
//	int min = 0;
//}