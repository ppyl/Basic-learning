#pragma once
#include<iostream>
using namespace std;

class Date
{
public:
	Date(int year = 2100, int month = 1, int day = 1);
	void Print();

	bool operator==(const Date& d);
	bool operator!=(const Date& d);
	bool operator<(const Date& d);
	bool operator<=(const Date& d);
	bool operator>(const Date& d);
	bool operator>=(const Date& d);
private:
	int _year;
	int _month;
	int _day;
};

