#include<iostream>
#include<map>
#include<string>
using namespace std;

//#include"Func1.h"
//
//int main()
//{
//	f(1);
//
//	return 0;
//}

//int main()
//{
//	int a = 0;
//	//int b = a;
//	auto b = a;
//	auto c = &a;
//	cout << typeid(b).name() << endl;
//	cout << typeid(c).name() << endl;
//
//	//auto 简化代码，当类型很长时，可以考虑使用自动推导
//	map<string, string> dict;
//	//typedef map<string, string>::iterator DictIt;
//	//map<string, string>::iterator dit = dict.begin();
//	//DictIt dit = dict.begin();
//	auto dit = dict.begin();
//
//	return 0;
//}

void TestFor(int& array)
{
	for (auto& e : array)
		cout << e << endl;
}

int main()
{
	int a = 0;
	auto b = &a;
	auto* c = &a;
	auto& d = a;

	int array[] = { 1, 2, 3, 4, 5, 6, 4 };
	for (int i = 0; i < sizeof(array) / sizeof(int); ++i)
	{
		cout << array[i] << " ";
	}
	cout << endl;

	// 范围for -- 语法糖
	// 自动依次取数组中数据赋值给e对象，自动判断结束
	for (auto& e : array)
	{
		e *= 2;
		cout << e << " ";
	}
	cout << endl;

	//for (int x : array)
	for (auto x : array)
	{
		cout << x << " ";
	}
	cout << endl;

	TestFor(array);

	return 0;
}