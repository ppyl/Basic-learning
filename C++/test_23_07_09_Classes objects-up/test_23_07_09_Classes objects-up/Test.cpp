//#include<iostream>
//using namespace std;
//
//////类的定义
////class Date
////{
////public:
////	void Init(int year, int month, int day)
////	{
////		_year = year;
////		_month = month;
////		_day = day;
////	}
////	void Print()
////	{
////		cout << "Print" << endl;
////	}
////private:
////	int _year;
////	int _month;
////	int _day;
////};
////
////int main()
////{
////	Date d1, d2;
////	d1.Init(2023, 2, 2);
////	d2.Init(2022, 2, 2);
////	d1.Print();
////	d1.Print();
////}
////
////class Date1
////{
////public:
////	void Print()
////	{
////		cout << "Print" << endl;
////	}
////private:
////	int _year;
////};
////
////int main()
////{
////	Date1* p = nullptr;
////	p->Print();
////
////	return 0;
////}
////
////class Date2
////{
////public:
////	void Print()
////	{
////		cout << _year << endl;
////	}
////private:
////	int _year;
////};
////
////int main()
////{
////	Date2* p = nullptr;
////	p->Print();
////
////	return 0;
////}
////
////class Date3
////{
////public:
////	void Print()
////	{
////		cout << "Print" << endl;
////	}
////private:
////	int _year;
////};
////
////int main()
////{
////	Date3* p = nullptr;
////	(*p).Print();
////
////	return 0;
////}
//
//////构造函数
////class Stack
////{
////public:
////	//构造函数
////	Stack()
////	{
////		_a = nullptr;
////	    _size = _capacity = 0;
////	}
////
////	Stack(int n)
////	{
////		_a = (int*)malloc(sizeof(int) * n);
////		if (nullptr == _a)
////		{
////			perror("malloc fail");
////			return;
////		}
////
////		_capacity = 0;
////		_size = 0;
////	}
////	void Push(int x)
////	{
////		//...
////		_a[_size++] = x;
////	}
////	void Pop()
////	{
////		//...
////	}
////	void Destory()
////	{
////		//...
////	}
////private:
////	int* _a;
////	int _capacity;
////	int _size;
////};
////
////int main()
////{
////	Stack st(4);
////	Stack st1;
////
////	st.Push(1);
////	st.Push(2);
////	st.Push(3);
////	st.Push(4);
////	st.Destory();
////	return 0;
////}
////typedef struct {
////    Stack pushst;
////    Stack popst;
////}MyQueue;
//
////class Date {
////public :
////    //默认生成构造函数不会处理内置类型
////    //默认生成析构函数不会处理内置类型
////    //Date(int y,int m,int d)
////    //{}
////    void Init(int y, int m, int d)
////    {
////        //...
////    }
////private:
////    //内置类型
////    //声明位置给的缺省值
////    int _y = 1;
////    int _m = 1;
////    int _d = 1;
////};
////class MyQueue {
////public:
////    //默认生成构造函数，自定义类型会调用它的默认构造函数
////    //默认生成析构函数，自定义类型会调用它的默认析构函数
////    //MyQueue(int y,int m,int d)
////    //    :_pushst(y,m,d)
////    //    ,_popst(y,m,d)
////    //{
////    //    
////    //}
////    void push(int x) {
////
////    }
////    //...
////
////    Date _pushst;
////    Date _popst;
////};
////
////int main()
////{
////    MyQueue q;
////
////    return 0;
////}
//
////class Date
////{
////public:
////	Date(int y = 1, int m = 1, int d = 1)
////	{
////		_y = y;
////		_m = m;
////		_d = d;
////	}
////
////	//const权限缩小，防止修改别名导致原数据遭到修改
////	Date(const Date& d)
////	{
////		cout << "Date(Date& d)" << endl;
////		_y = d._y;
////		_m = d._m;
////		_d = d._d;
////	}
////
////	//普通的构造函数（有拷贝构造的功能，但是不建议使用）
////	Date(const Date* d)
////	{
////		cout << "Date(Date& d)" << endl;
////		_y = d->_y;
////		_m = d->_m;
////		_d = d->_d;
////	}
////private:
////	int _y;
////	int _m;
////	int _d;
////};
//////传值传参
////void Func1(Date d)
////{
////
////}
////
////// 传引用传参
////void Func2(Date& d)
////{
////
////}
////
////void Func3(Date* d)
////{
////
////}
////
////int main()
////{
////	Date d1(2023, 1, 1);
////	Date d2(d1);
////	Date d3 = d1;
////
////	Func1(d1);
////	Func2(d1);
////	Func3(&d1);
////
////	return 0;
////}
//
////运算符重载
//class Date
//{
//public:
//	Date(int year = 2100, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//  void Print()
//  {
//		cout << _year << "/" << _month << "/" << _day << endl;
//  }
// 
//	//有一个隐藏参数，成员函数默认都有一个隐藏的参数this指针
//	//有几个操作数写几个参数
//	//bool operator==(const Date& d1, const Date& d2)
//	//{
//	//	return d1._year == d2._year
//	//		&& d1._month == d2._month
//	//		&& d1._day == d2._day;
//	//}
//
//	// d1 == d2 -> d1.operator==(d2)
//	bool operator==(const Date& d)
//	{
//		//this 和 d2 
//		return _year == d._year
//			&& _month == d._month
//			&& _day == d._day;
//	}
//
//	bool operator<(const Date& d)
//	{
//		if (_year < d._year)
//		{
//			return true;
//		}
//		else if (_year == d._year && _month < d._month)
//		{
//			return true;
//		}
//		else if (_year == d._year && _month == d._month && _day < d._day)
//		{
//			return true;
//		}
//		else
//		{
//			return false;
//		}
//	}
//
//	bool operator<=(const Date& d)
//	{
//		return *this < d || *this == d;
//	}
//
//	bool operator>(const Date& d)
//	{
//		return !(*this <= d);
//	}
//
//	bool operator>=(const Date& d)
//	{
//		return !(*this < d);
//	}
//
//	bool operator!=(const Date& d)
//	{
//		return !(*this == d);
//	}
//
//	//返回值可以做到连续赋值，保持运算符的特性
//	Date& operator=(const Date& d)
//	{
//		//避免自我赋值
//		if (this != &d)
//		{
//			_year = d._year;
//			_month = d._month;
//			_day = d._day;
//		}
//
//		return *this;
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date d1(2024, 3, 3);
//	Date d2(2023, 5, 3);
//
//	//cout << operator==(d1, d2) << endl;
//	cout << d1.operator==(d2) << endl;
//	cout << d1.operator<(d2) << endl;
//	cout << (d1 == d2) << endl;
//	cout << (d1 < d2) << endl;
//
//	d1 = d2;
//
//	return 0;
//}

//#include"Date.h"
//
//int main()
//{
//
//	return 0;
//}