// 三步问题。有个小孩正在上楼梯，楼梯有n阶台阶，小孩一次可以上1阶、2阶或3阶。
// 实现一种方法，计算小孩有多少种上楼梯的方式。
// 结果可能很大，你需要对结果模1000000007。

class Solution {
public:
    int waysToStep(int n) {
        const int MOD = 1e9 + 7;
        if (n == 1 || n == 2)
            return n;
        if (n == 3)
            return 4;
        vector<int> dp(n + 1);
        dp[1] = 1, dp[2] = 2, dp[3] = 4;
        for (int i = 4; i <= n; i++)
        {
            dp[i] = ((dp[i - 1] + dp[i - 2]) % MOD + dp[i - 3]) % MOD;
        }
        return dp[n];
    }
};

// 不严谨代码
//class Solution {
//public:
//    int waysToStep(int n) {
//        if (n == 0 || n == 1)
//            return 1;
//        if (n == 2)
//            return 2;
//        vector<long long> dp(n + 1);
//        dp[0] = 1, dp[1] = 1, dp[2] = 2;
//        for (int i = 3; i <= n; i++)
//        {
//            dp[i] = (dp[i - 1] + dp[i - 2] + dp[i - 3]) % 1000000007;
//        }
//        return dp[n];
//    }
//};