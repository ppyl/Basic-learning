//#include<iostream>
//#include<vector>
//#include<time.h>
//using namespace std;
//
//// 基本结构
//void test_vector1()
//{
//	// 无参构造
//	// explicit vector (const allocator_type& alloc = allocator_type());
//	vector<int> v;
//	v.push_back(1);
//	v.push_back(2);
//	v.push_back(3);
//	v.push_back(4);
//	v.push_back(5);
//
//	// 遍历
//	for (size_t i = 0; i < v.size(); i++)
//	{
//		cout << v[i] << " ";
//	}
//	cout << endl;
//
//	vector<int>::iterator it = v.begin();
//	while (it != v.end())
//	{
//		cout << *it << " ";
//		++it;
//	}
//	cout << endl;
//
//	for (auto e : v)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//
//	// 拷贝
//	// vector (const vector& x);
//	vector<int> copy(v);
//	for (auto e : copy)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//}
//
//void test_vector2()
//{
//	// 初始化为10个1
//	// explicit vector (size_type n, const value_type& val = value_type(),
//	// const allocator_type& alloc = allocator_type());
//	vector<int> v1(10, 1);
//	for (auto e : v1)
//	{
//		cout << e << " ";
//	}
//	cout << endl;	
//	
//	// template <class InputIterator>
//	// vector(InputIterator first, InputIterator last,
//	// const allocator_type & alloc = allocator_type());
//	vector<int> v2(v1.begin(), v1.end());
//	for (auto e : v2)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//
//	// 模板可以传任意类型，容器的迭代器之间可以互相使用
//	string s1("hello xxx");
//	//vector<int> v3(++s1.begin(), --s1.end());
//	vector<int> v3(++s1.begin() + 2, --s1.end());
//	for (auto e : v3)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//}
//
//void test_vector3()
//{
//	vector<int> v;
//	v.push_back(1);
//	v.push_back(2);
//	v.push_back(3);
//	v.push_back(4);
//	v.push_back(5);
//
//	vector<int>::iterator it = v.begin();
//	while (it != v.end())
//	{
//		cout << *it << " ";
//		++it;
//	}
//	cout << endl;
//
//	//逆向迭代器
//	vector<int>::reverse_iterator rit = v.rbegin();
//	while (rit != v.rend())
//	{
//		cout << *rit << " ";
//		++rit;
//	}
//	cout << endl;
//
//	// 无意义接口
//	//cout << v.max_size() << endl;
//}
//
//// 测试vector的默认扩容机制
//void TestVectorExpand()
//{
//	size_t sz;
//	vector<int> v;
//	const size_t n = 10000000;
//	size_t begin = clock();
//	//v.reserve(n);
//	sz = v.capacity();
//	cout << "making v grow:\n";
//	for (int i = 0; i < n; ++i)
//	{
//		v.push_back(i);
//		if (sz != v.capacity())
//		{
//			sz = v.capacity();
//			cout << "capacity changed: " << sz << '\n';
//		}
//	}
//	size_t end = clock();
//	cout << "消耗时长：" << end - begin << endl;
//}
//
//void test_vector4()
//{
//	vector<int> v1;
//	// 申请空间并初始化，自动调用释放
//	v1.resize(10, 0);
//
//	vector<int> v2(10, 0);
//}
//	
//void test_vector5()
//{
//	vector<int> v;
//	v.push_back(1);
//	v.push_back(2);
//	v.push_back(3);
//	v.push_back(4);
//	v.push_back(5);
//
//	for (auto e : v)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//
//	// insert（与string采用的下标寻找不同，vector采用区间寻找）
//	vector<int>::iterator pos = find(v.begin(), v.end(), 2);
//	if (pos != v.end())
//	{
//		v.insert(pos, 99);
//	}
//
//	for (auto e : v)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//
//	// 迭代器失效
//	// 使用find借用算法以及迭代器左闭右开的特性
//	pos = find(v.begin(), v.end(), 2);
//	if (pos != v.end())
//	{
//		v.erase(pos);
//	}
//
//	for (auto e : v)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//
//	// 头删不受影响
//	v.erase(v.begin());
//
//	for (auto e : v)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//}
//
//// vector<char> 和 string 二者有本质区别
//
//int main()
//{
//	//test_vector1();
//	//test_vector2();
//	//test_vector3();
//	//test_vector4();
//	test_vector5();
//
//	//TestVectorExpand();
//
//	return 0;
//}