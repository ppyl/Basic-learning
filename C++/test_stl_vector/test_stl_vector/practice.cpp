////给你一个 非空 整数数组 nums ，除了某个元素只出现一次以外，其余每个元素均出现两次。找出那个只出现了一次的元素。
//class Solution {
//public:
//    int singleNumber(vector<int>& nums) {
//        int ret = 0;
//        for (auto e : nums)
//        {
//            ret ^= e;
//        }
//        return ret;
//    }
//};
//
////给定一个非负整数 numRows，生成「杨辉三角」的前 numRows 行。在「杨辉三角」中，每个数是它左上方和右上方的数的和
//class Solution {
//public:
//    vector<vector<int>> generate(int numRows) {
//        vector<vector<int>> vv;
//        vv.resize(numRows, vector<int>());
//        for (size_t i = 0; i < vv.size(); ++i)
//        {
//            vv[i].resize(i + 1, 0);
//            vv[i][0] = vv[i][vv[i].size() - 1] = 1;
//        }
//        for (size_t i = 0; i < vv.size(); ++i)
//        {
//            for (size_t j = 0; j < vv[i].size(); ++j)
//            {
//                if (vv[i][j] == 0)
//                {
//                    vv[i][j] = vv[i - 1][j] + vv[i - 1][j - 1];
//                }
//            }
//        }
//        return vv;
//    }
//};
//
////给定一个仅包含数字 2-9 的字符串，返回所有它能表示的字母组合。答案可以按 任意顺序 返回。
//class Solution {
//    string _numToStr[10] = { "","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz" };
//public:
//    void Combinations(const string& digits, size_t di, string combineStr, vector<string>& strV)
//    {
//        if (di == digits.size())
//        {
//            strV.push_back(combineStr);
//            return;
//        }
//
//        int num = digits[di] - '0';
//        string str = _numToStr[num];
//        for (auto ch : str)
//        {
//            Combinations(digits, di + 1, combineStr + ch, strV);
//        }
//    }
//    vector<string> letterCombinations(string digits) {
//        // 传引用：返回上一层需要pop（只有一个string）
//        // 传值：每一层有一个string
//        vector<string> strV;
//        if (digits.size() == 0)
//        {
//            return strV;
//        }
//        Combinations(digits, 0, "", strV);
//        return strV;
//    }
//};