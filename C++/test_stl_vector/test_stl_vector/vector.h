#pragma once
#include<assert.h>

namespace pyl 
{
	template <class T>
	class vector
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator; 

		//默认构造（借用成员变量缺省值进行初始化） 其他构造同理
		vector()
		{}

		//初始化默认构造
		//vector()
		//	:_start(nullptr)
		//	,_finish(nullptr)
		//	,_end_of_storage(nullptr)
		//{

		//}

		// vector<int> v(10,3) - 10到n有类型转换不是最优选（提供一份重载函数解决 或 传递对应类型参数）
		vector(int n, const T& val = T())
		//vector(size_t n, const T& val = T())
			:_start(nullptr)
			, _finish(nullptr)
			, _end_of_storage(nullptr)
		{
			reserve(n);
			for (int i = 0; i < n; ++i)
			//for (size_t i = 0; i < n; ++i)
			{
				push_back(val);
			}
		}

		// vector<int> v(10,3) - 该形式优先选择这种构造函数，但是first是int类型，无法解引用
		// [first, last)
		// 模板参数可以使各个容器互相使用
		template<class InputIterator>
		vector(InputIterator first, InputIterator last)
			:_start(nullptr)
			, _finish(nullptr)
			, _end_of_storage(nullptr)
		{
			// 考虑list容器地址大小不适合比较，采用!=方式判断
			while (first != last)
			{
				push_back(*first);
				first++;
			}
		}

		//传统深拷贝
		vector(const vector<T>& v)
		{
			_start = new T[v.capacity()];
			// 适合内置类型深拷贝
			//memcpy(_start, v._start, sizeof(T) * v.size());
			// 使用模板深拷贝，避免深拷贝层次问题
			for (size_t i = 0; i < v.size(); ++i)
			{
				_start[i] = v._start[i];
			}

			_finish = _start + v.size();
			_end_of_storage = _start + v.capacity();
		}

		// 范围for拷贝
		//vector(const vector<T>& v)
		//{
		//	reserve(v.capacity());
		//	for (auto e : v)
		//	{
		//		push_back(e);
		//	}
		//}

		~vector()
		{
			delete[] _start;
			_start = _finish = _end_of_storage = nullptr;
		}

		iterator begin()
		{
			return _start;
		}

		iterator end()
		{
			return _finish;
		}

		const_iterator begin() const
		{
			return _start;
		}

		const_iterator end() const
		{
			return _finish;
		}

		// 实现任意类型，构造默认对象
		// 更改大小
		void resize(size_t n, T val = T())
		{
			if (n < size())
			{
				//删除
				_finish = _start + n;
			}
			else
			{
				if (n > capacity())
				{
					reserve(n);
				}

				while (_finish != _start + n)
				{
					*_finish = val;
					++_finish;
				}
			}
		}

		//扩容
		void reserve(size_t n)
		{
			if (n > capacity())
			{
				size_t sz = size();
				T* tmp = new T[n];
				if (_start)
				{
					//同理，只适合处理内置类型
					//memcpy(tmp, _start, sizeof(T) * size());
					for (size_t i = 0; i < sz; ++i)
					{
						tmp[i] = _start[i];
					}
					delete[] _start;
				}
				_start = tmp;
				_finish = _start + sz;
				_end_of_storage = _start + n;
			}
		}

		void push_back(const T& x)
		{
			if (_finish == _end_of_storage)
			{
				reserve(capacity() == 0 ? 4 : capacity() * 2);
			}
			*_finish = x;
			++_finish;
		}

		void pop_back()
		{
			assert(!empty());

			--_finish;
		}

		iterator insert(iterator pos, const T& val)
		{
			assert(pos >= _start);
			assert(pos <= _finish);

			if (_finish == _end_of_storage)
			{
				size_t len = pos - _start;
				reserve(capacity() == 0 ? 4 : capacity() * 2);

				// 扩容后更新pos，解决pos失效
				pos = _start + len;
			}

			iterator end = _finish - 1;
			while (end >= pos)
			{
				*(end + 1) = *end;
				--end;
			}

			*pos = val;
			++_finish;

			return pos;
		}

		iterator erase(iterator pos)
		{
			assert(pos >= _start);
			assert(pos <= _finish);

			iterator start = pos + 1;
			while (start != _finish)
			{
				*(start - 1) = *start;
				++start;
			}

			--_finish;
			
			return pos;
		}

		bool empty()
		{
			return _start == _finish;
		}

		T& operator[](size_t pos)
		{
			assert(pos < size());

			return _start[pos];
		}

		const T& operator[](size_t pos) const
		{
			assert(pos < size());

			return _start[pos];
		}

		size_t capacity() const
		{
			return _end_of_storage - _start;
		}

		size_t size() const
		{
			return _finish - _start;
		}

	// C++11以后内置类型缺省值，构造函数初始化
	private:
		iterator _start = nullptr;
		iterator _finish = nullptr;
		iterator _end_of_storage = nullptr;
	};

	// const对象的访问
	void func(const vector<int>& v)
	{
		for (size_t i = 0; i < v.size(); ++i)
		{
			cout << v[i] << " ";
		}
		cout << endl;

		vector<int>::const_iterator it = v.begin();
		while (it != v.end())
		{
			cout << *it << " ";
			++it;
		}
		cout << endl << endl;
	}

	//输出测试
	void test_vector1()
	{
		vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);
		v1.push_back(5);

		func(v1);

		for (size_t i = 0; i < v1.size(); ++i)
		{
			cout << v1[i] << " ";
		}
		cout << endl;

		v1.pop_back();
		v1.pop_back();

		vector<int>::iterator it = v1.begin();
		while (it != v1.end())
		{
			cout << *it << " ";
			++it;
		}
		cout << endl;

		v1.pop_back();
		v1.pop_back();
		v1.pop_back();

		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	//template<class T>

	//void f()
	//{
	//	T x = T();
	//	cout << x << endl;
	//}

	//void test_vector2()
	//{
	//	//内置类型有无构造函数（一般情况没有，在模板下有）
	//	f<int>();
	//	f<int*>();
	//	f<double>();
	//}


	// resize更改大小
	void test_vector2()
	{
		vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);
		v1.push_back(5);

		cout << v1.size() << endl;
		cout << v1.capacity() << endl;

		v1.resize(10);

		cout << v1.size() << endl;
		cout << v1.capacity() << endl;

		func(v1);

		v1.resize(3);

		func(v1);
	}

	// 迭代器失效问题-insert
	void test_vector3()
	{
		std::vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);
		//v1.push_back(5);
		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;

		/*v1.insert(v1.begin(), 0);
		for (auto e : v1)
		{
		cout << e << " ";
		}
		cout << endl;*/

		auto pos = find(v1.begin(), v1.end(), 3);
		if (pos != v1.end())
		{
			//v1.insert(pos, 30);
			pos = v1.insert(pos, 30);
		}

		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;

		// insert以后我们认为pos失效了（pos位置不可控），不能再使用
		(*pos)++;

		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	// 迭代器失效问题-erase
	void test_vector4()
	{
		std::vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);
		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;

		//auto pos = find(v1.begin(), v1.end(), 2);
		auto pos = find(v1.begin(), v1.end(), 4);
		if (pos != v1.end())
		{
			v1.erase(pos);
		}

		// pos已经失效，进行操作后可能导致容器出错
		//(*pos)++;

		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	// 删除偶数问题-迭代器失效
	void test_vector5()
	{
		pyl::vector<int> v1;
		v1.push_back(10);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);
		v1.push_back(5);
		v1.push_back(50);

		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;

		pyl::vector<int>::iterator it = v1.begin();
		while (it != v1.end())
		{
			if (*it % 2 == 0)
			{
				// 奇数删除后，迭代器指向删除位置的下一个位置
				it = v1.erase(it);
			}
			else
			{
				// 偶数向后遍历
				++it;
			}
		}

		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;

	}

	// 深浅拷贝问题
	void test_vector6()
	{
		// 也可以配置对应类型
		//pyl::vector<int> v(10u, 3);
		pyl::vector<int> v1(10, 3);
		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;

		vector<int> v2(v1.begin() + 1, v1.end() - 1);
		for (auto e : v2)
		{
			cout << e << " ";
		}
		cout << endl;

		std::string s1("hello");
		vector<int> v3(s1.begin(), s1.end());
		for (auto e : v3)
		{
			cout << e << " ";
		}
		cout << endl;

		// 数组
		int a[] = { 100, 10, 2, 20, 30 };
		vector<int> v4(a, a + 3);
		for (auto e : v4)
		{
			cout << e << " ";
		}
		cout << endl;

		v1.insert(v1.begin(), 10);
		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;

		sort(v1.begin(), v1.end());

		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;


		for (auto e : a)
		{
			cout << e << " ";
		}
		cout << endl;

		//sort(a, a+sizeof(a)/sizeof(int));
		// 仿函数
	/*	greater<int> g;
		sort(a, a + sizeof(a) / sizeof(int), g);*/

		sort(a, a + sizeof(a) / sizeof(int), greater<int>());

		for (auto e : a)
		{
			cout << e << " ";
		}
		cout << endl;

	}

	// 杨辉三角问题
	class Solution {
	public:
		vector<vector<int>> generate(int numRows) {
			vector<vector<int>> vv;
			vv.resize(numRows, vector<int>());
			for (size_t i = 0; i < vv.size(); ++i)
			{
				vv[i].resize(i + 1, 0);
				vv[i][0] = vv[i][vv[i].size() - 1] = 1;
			}

			for (size_t i = 0; i < vv.size(); ++i)
			{
				for (size_t j = 0; j < vv[i].size(); ++j)
				{
					if (vv[i][j] == 0)
					{
						vv[i][j] = vv[i - 1][j] + vv[i - 1][j - 1];
					}
				}
			}

			return vv;
		}
	};

    // 深拷贝层次问题
	void test_vector7()
	{
		vector<int> v1(10, 5);
		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;

		vector<int> v2(v1);
		for (auto e : v2)
		{
			cout << e << " ";
		}
		cout << endl;

		vector<std::string> v3(3, "111111111111111111111");
		for (auto e : v3)
		{
			cout << e << " ";
		}
		cout << endl;

		vector<std::string> v4(v3);
		for (auto e : v4)
		{
			cout << e << " ";
		}
		cout << endl;

		v4.push_back("2222222222222222222");
		v4.push_back("2222222222222222222");
		v4.push_back("2222222222222222222");
		for (auto e : v4)
		{
			cout << e << " ";
		}
		cout << endl;

		// 由于拷贝未涉及内部数据，即二维数组中的列，可以考虑改写赋值重载使其完成拷贝
		vector<vector<int>> ret = Solution().generate(5);
		for (size_t i = 0; i < ret.size(); ++i)
		{
			for (size_t j = 0; j < ret[i].size(); ++j)
			{
				cout << ret[i][j] << " ";
			}
			cout << endl;
		}
		cout << endl;
	}
}