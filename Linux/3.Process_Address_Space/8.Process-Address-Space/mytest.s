
mytest:     file format elf64-x86-64


Disassembly of section .init:

0000000000400508 <_init>:
  400508:	48 83 ec 08          	sub    $0x8,%rsp
  40050c:	48 8b 05 e5 0a 20 00 	mov    0x200ae5(%rip),%rax        # 600ff8 <__gmon_start__>
  400513:	48 85 c0             	test   %rax,%rax
  400516:	74 05                	je     40051d <_init+0x15>
  400518:	e8 63 00 00 00       	callq  400580 <__gmon_start__@plt>
  40051d:	48 83 c4 08          	add    $0x8,%rsp
  400521:	c3                   	retq   

Disassembly of section .plt:

0000000000400530 <.plt>:
  400530:	ff 35 d2 0a 20 00    	pushq  0x200ad2(%rip)        # 601008 <_GLOBAL_OFFSET_TABLE_+0x8>
  400536:	ff 25 d4 0a 20 00    	jmpq   *0x200ad4(%rip)        # 601010 <_GLOBAL_OFFSET_TABLE_+0x10>
  40053c:	0f 1f 40 00          	nopl   0x0(%rax)

0000000000400540 <getpid@plt>:
  400540:	ff 25 d2 0a 20 00    	jmpq   *0x200ad2(%rip)        # 601018 <getpid@GLIBC_2.2.5>
  400546:	68 00 00 00 00       	pushq  $0x0
  40054b:	e9 e0 ff ff ff       	jmpq   400530 <.plt>

0000000000400550 <printf@plt>:
  400550:	ff 25 ca 0a 20 00    	jmpq   *0x200aca(%rip)        # 601020 <printf@GLIBC_2.2.5>
  400556:	68 01 00 00 00       	pushq  $0x1
  40055b:	e9 d0 ff ff ff       	jmpq   400530 <.plt>

0000000000400560 <__assert_fail@plt>:
  400560:	ff 25 c2 0a 20 00    	jmpq   *0x200ac2(%rip)        # 601028 <__assert_fail@GLIBC_2.2.5>
  400566:	68 02 00 00 00       	pushq  $0x2
  40056b:	e9 c0 ff ff ff       	jmpq   400530 <.plt>

0000000000400570 <__libc_start_main@plt>:
  400570:	ff 25 ba 0a 20 00    	jmpq   *0x200aba(%rip)        # 601030 <__libc_start_main@GLIBC_2.2.5>
  400576:	68 03 00 00 00       	pushq  $0x3
  40057b:	e9 b0 ff ff ff       	jmpq   400530 <.plt>

0000000000400580 <__gmon_start__@plt>:
  400580:	ff 25 b2 0a 20 00    	jmpq   *0x200ab2(%rip)        # 601038 <__gmon_start__>
  400586:	68 04 00 00 00       	pushq  $0x4
  40058b:	e9 a0 ff ff ff       	jmpq   400530 <.plt>

0000000000400590 <getppid@plt>:
  400590:	ff 25 aa 0a 20 00    	jmpq   *0x200aaa(%rip)        # 601040 <getppid@GLIBC_2.2.5>
  400596:	68 05 00 00 00       	pushq  $0x5
  40059b:	e9 90 ff ff ff       	jmpq   400530 <.plt>

00000000004005a0 <sleep@plt>:
  4005a0:	ff 25 a2 0a 20 00    	jmpq   *0x200aa2(%rip)        # 601048 <sleep@GLIBC_2.2.5>
  4005a6:	68 06 00 00 00       	pushq  $0x6
  4005ab:	e9 80 ff ff ff       	jmpq   400530 <.plt>

00000000004005b0 <fork@plt>:
  4005b0:	ff 25 9a 0a 20 00    	jmpq   *0x200a9a(%rip)        # 601050 <fork@GLIBC_2.2.5>
  4005b6:	68 07 00 00 00       	pushq  $0x7
  4005bb:	e9 70 ff ff ff       	jmpq   400530 <.plt>

Disassembly of section .text:

00000000004005c0 <_start>:
  4005c0:	31 ed                	xor    %ebp,%ebp
  4005c2:	49 89 d1             	mov    %rdx,%r9
  4005c5:	5e                   	pop    %rsi
  4005c6:	48 89 e2             	mov    %rsp,%rdx
  4005c9:	48 83 e4 f0          	and    $0xfffffffffffffff0,%rsp
  4005cd:	50                   	push   %rax
  4005ce:	54                   	push   %rsp
  4005cf:	49 c7 c0 e0 07 40 00 	mov    $0x4007e0,%r8
  4005d6:	48 c7 c1 70 07 40 00 	mov    $0x400770,%rcx
  4005dd:	48 c7 c7 ad 06 40 00 	mov    $0x4006ad,%rdi
  4005e4:	e8 87 ff ff ff       	callq  400570 <__libc_start_main@plt>
  4005e9:	f4                   	hlt    
  4005ea:	66 0f 1f 44 00 00    	nopw   0x0(%rax,%rax,1)

00000000004005f0 <deregister_tm_clones>:
  4005f0:	b8 67 10 60 00       	mov    $0x601067,%eax
  4005f5:	55                   	push   %rbp
  4005f6:	48 2d 60 10 60 00    	sub    $0x601060,%rax
  4005fc:	48 83 f8 0e          	cmp    $0xe,%rax
  400600:	48 89 e5             	mov    %rsp,%rbp
  400603:	77 02                	ja     400607 <deregister_tm_clones+0x17>
  400605:	5d                   	pop    %rbp
  400606:	c3                   	retq   
  400607:	b8 00 00 00 00       	mov    $0x0,%eax
  40060c:	48 85 c0             	test   %rax,%rax
  40060f:	74 f4                	je     400605 <deregister_tm_clones+0x15>
  400611:	5d                   	pop    %rbp
  400612:	bf 60 10 60 00       	mov    $0x601060,%edi
  400617:	ff e0                	jmpq   *%rax
  400619:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)

0000000000400620 <register_tm_clones>:
  400620:	b8 60 10 60 00       	mov    $0x601060,%eax
  400625:	55                   	push   %rbp
  400626:	48 2d 60 10 60 00    	sub    $0x601060,%rax
  40062c:	48 c1 f8 03          	sar    $0x3,%rax
  400630:	48 89 e5             	mov    %rsp,%rbp
  400633:	48 89 c2             	mov    %rax,%rdx
  400636:	48 c1 ea 3f          	shr    $0x3f,%rdx
  40063a:	48 01 d0             	add    %rdx,%rax
  40063d:	48 d1 f8             	sar    %rax
  400640:	75 02                	jne    400644 <register_tm_clones+0x24>
  400642:	5d                   	pop    %rbp
  400643:	c3                   	retq   
  400644:	ba 00 00 00 00       	mov    $0x0,%edx
  400649:	48 85 d2             	test   %rdx,%rdx
  40064c:	74 f4                	je     400642 <register_tm_clones+0x22>
  40064e:	5d                   	pop    %rbp
  40064f:	48 89 c6             	mov    %rax,%rsi
  400652:	bf 60 10 60 00       	mov    $0x601060,%edi
  400657:	ff e2                	jmpq   *%rdx
  400659:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)

0000000000400660 <__do_global_dtors_aux>:
  400660:	80 3d f9 09 20 00 00 	cmpb   $0x0,0x2009f9(%rip)        # 601060 <__TMC_END__>
  400667:	75 11                	jne    40067a <__do_global_dtors_aux+0x1a>
  400669:	55                   	push   %rbp
  40066a:	48 89 e5             	mov    %rsp,%rbp
  40066d:	e8 7e ff ff ff       	callq  4005f0 <deregister_tm_clones>
  400672:	5d                   	pop    %rbp
  400673:	c6 05 e6 09 20 00 01 	movb   $0x1,0x2009e6(%rip)        # 601060 <__TMC_END__>
  40067a:	f3 c3                	repz retq 
  40067c:	0f 1f 40 00          	nopl   0x0(%rax)

0000000000400680 <frame_dummy>:
  400680:	48 83 3d 98 07 20 00 	cmpq   $0x0,0x200798(%rip)        # 600e20 <__JCR_END__>
  400687:	00 
  400688:	74 1e                	je     4006a8 <frame_dummy+0x28>
  40068a:	b8 00 00 00 00       	mov    $0x0,%eax
  40068f:	48 85 c0             	test   %rax,%rax
  400692:	74 14                	je     4006a8 <frame_dummy+0x28>
  400694:	55                   	push   %rbp
  400695:	bf 20 0e 60 00       	mov    $0x600e20,%edi
  40069a:	48 89 e5             	mov    %rsp,%rbp
  40069d:	ff d0                	callq  *%rax
  40069f:	5d                   	pop    %rbp
  4006a0:	e9 7b ff ff ff       	jmpq   400620 <register_tm_clones>
  4006a5:	0f 1f 00             	nopl   (%rax)
  4006a8:	e9 73 ff ff ff       	jmpq   400620 <register_tm_clones>

00000000004006ad <main>:
  4006ad:	55                   	push   %rbp
  4006ae:	48 89 e5             	mov    %rsp,%rbp
  4006b1:	41 54                	push   %r12
  4006b3:	53                   	push   %rbx
  4006b4:	48 83 ec 10          	sub    $0x10,%rsp
  4006b8:	e8 f3 fe ff ff       	callq  4005b0 <fork@plt>
  4006bd:	89 45 ec             	mov    %eax,-0x14(%rbp)
  4006c0:	83 7d ec 00          	cmpl   $0x0,-0x14(%rbp)
  4006c4:	79 19                	jns    4006df <main+0x32>
  4006c6:	b9 c6 08 40 00       	mov    $0x4008c6,%ecx
  4006cb:	ba 0c 00 00 00       	mov    $0xc,%edx
  4006d0:	be 00 08 40 00       	mov    $0x400800,%esi
  4006d5:	bf 09 08 40 00       	mov    $0x400809,%edi
  4006da:	e8 81 fe ff ff       	callq  400560 <__assert_fail@plt>
  4006df:	83 7d ec 00          	cmpl   $0x0,-0x14(%rbp)
  4006e3:	75 45                	jne    40072a <main+0x7d>
  4006e5:	44 8b 25 70 09 20 00 	mov    0x200970(%rip),%r12d        # 60105c <g_value>
  4006ec:	e8 9f fe ff ff       	callq  400590 <getppid@plt>
  4006f1:	89 c3                	mov    %eax,%ebx
  4006f3:	e8 48 fe ff ff       	callq  400540 <getpid@plt>
  4006f8:	41 b8 5c 10 60 00    	mov    $0x60105c,%r8d
  4006fe:	44 89 e1             	mov    %r12d,%ecx
  400701:	89 da                	mov    %ebx,%edx
  400703:	89 c6                	mov    %eax,%esi
  400705:	bf 18 08 40 00       	mov    $0x400818,%edi
  40070a:	b8 00 00 00 00       	mov    $0x0,%eax
  40070f:	e8 3c fe ff ff       	callq  400550 <printf@plt>
  400714:	bf 01 00 00 00       	mov    $0x1,%edi
  400719:	e8 82 fe ff ff       	callq  4005a0 <sleep@plt>
  40071e:	c7 05 34 09 20 00 c8 	movl   $0xc8,0x200934(%rip)        # 60105c <g_value>
  400725:	00 00 00 
  400728:	eb bb                	jmp    4006e5 <main+0x38>
  40072a:	44 8b 25 2b 09 20 00 	mov    0x20092b(%rip),%r12d        # 60105c <g_value>
  400731:	e8 5a fe ff ff       	callq  400590 <getppid@plt>
  400736:	89 c3                	mov    %eax,%ebx
  400738:	e8 03 fe ff ff       	callq  400540 <getpid@plt>
  40073d:	41 b8 5c 10 60 00    	mov    $0x60105c,%r8d
  400743:	44 89 e1             	mov    %r12d,%ecx
  400746:	89 da                	mov    %ebx,%edx
  400748:	89 c6                	mov    %eax,%esi
  40074a:	bf 70 08 40 00       	mov    $0x400870,%edi
  40074f:	b8 00 00 00 00       	mov    $0x0,%eax
  400754:	e8 f7 fd ff ff       	callq  400550 <printf@plt>
  400759:	bf 01 00 00 00       	mov    $0x1,%edi
  40075e:	e8 3d fe ff ff       	callq  4005a0 <sleep@plt>
  400763:	eb c5                	jmp    40072a <main+0x7d>
  400765:	66 2e 0f 1f 84 00 00 	nopw   %cs:0x0(%rax,%rax,1)
  40076c:	00 00 00 
  40076f:	90                   	nop

0000000000400770 <__libc_csu_init>:
  400770:	41 57                	push   %r15
  400772:	41 89 ff             	mov    %edi,%r15d
  400775:	41 56                	push   %r14
  400777:	49 89 f6             	mov    %rsi,%r14
  40077a:	41 55                	push   %r13
  40077c:	49 89 d5             	mov    %rdx,%r13
  40077f:	41 54                	push   %r12
  400781:	4c 8d 25 88 06 20 00 	lea    0x200688(%rip),%r12        # 600e10 <__frame_dummy_init_array_entry>
  400788:	55                   	push   %rbp
  400789:	48 8d 2d 88 06 20 00 	lea    0x200688(%rip),%rbp        # 600e18 <__init_array_end>
  400790:	53                   	push   %rbx
  400791:	4c 29 e5             	sub    %r12,%rbp
  400794:	31 db                	xor    %ebx,%ebx
  400796:	48 c1 fd 03          	sar    $0x3,%rbp
  40079a:	48 83 ec 08          	sub    $0x8,%rsp
  40079e:	e8 65 fd ff ff       	callq  400508 <_init>
  4007a3:	48 85 ed             	test   %rbp,%rbp
  4007a6:	74 1e                	je     4007c6 <__libc_csu_init+0x56>
  4007a8:	0f 1f 84 00 00 00 00 	nopl   0x0(%rax,%rax,1)
  4007af:	00 
  4007b0:	4c 89 ea             	mov    %r13,%rdx
  4007b3:	4c 89 f6             	mov    %r14,%rsi
  4007b6:	44 89 ff             	mov    %r15d,%edi
  4007b9:	41 ff 14 dc          	callq  *(%r12,%rbx,8)
  4007bd:	48 83 c3 01          	add    $0x1,%rbx
  4007c1:	48 39 eb             	cmp    %rbp,%rbx
  4007c4:	75 ea                	jne    4007b0 <__libc_csu_init+0x40>
  4007c6:	48 83 c4 08          	add    $0x8,%rsp
  4007ca:	5b                   	pop    %rbx
  4007cb:	5d                   	pop    %rbp
  4007cc:	41 5c                	pop    %r12
  4007ce:	41 5d                	pop    %r13
  4007d0:	41 5e                	pop    %r14
  4007d2:	41 5f                	pop    %r15
  4007d4:	c3                   	retq   
  4007d5:	90                   	nop
  4007d6:	66 2e 0f 1f 84 00 00 	nopw   %cs:0x0(%rax,%rax,1)
  4007dd:	00 00 00 

00000000004007e0 <__libc_csu_fini>:
  4007e0:	f3 c3                	repz retq 

Disassembly of section .fini:

00000000004007e4 <_fini>:
  4007e4:	48 83 ec 08          	sub    $0x8,%rsp
  4007e8:	48 83 c4 08          	add    $0x8,%rsp
  4007ec:	c3                   	retq   
