#include "../logs/PYLlog.h"
#include <unistd.h>

void test_log(const std::string &name) {
    INFO("%s", "测试开始");
    PYLlog::Logger::ptr logger = PYLlog::LoggerManager::getInstance().getLogger("async_logger");
    logger->debug("%s", "测试日志");
    logger->info("%s", "测试日志");
    logger->warn("%s", "测试日志");
    logger->error("%s", "测试日志");
    logger->fatal("%s", "测试日志");
    INFO("%s", "测试完毕");
}

int main()
{
    std::unique_ptr<PYLlog::LoggerBuilder> builder(new PYLlog::GlobalLoggerBuilder());
    builder->buildLoggerName("async_logger");
    builder->buildLoggerLevel(PYLlog::LogLevel::value::DEBUG);
    builder->buildFormatter("[%c][%f:%l][%p]%m%n");
    builder->buildLoggerType(PYLlog::LoggerType::LOGGER_SYNC);
    builder->buildSink<PYLlog::FileSink>("./logfile/sync.log");
    builder->buildSink<PYLlog::StdoutSink>();
    builder->buildSink<PYLlog::RollBySizeSink>("./logfile/roll_sync_by_size", 1024 * 1024);
    builder->build();

    test_log("async_logger");

    return 0;
}
