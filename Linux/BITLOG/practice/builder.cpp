/*通过构造苹果笔记本电脑理解建造者模式*/
// 主要应对于复杂对象的建造过程

#include<iostream>
#include<string>
#include<memory>

// 抽象出笔记本电脑的类
class Computer {
    public:
        Computer() {}
        void setBoard(const std::string &board)
        {
            _board = board;
        }
        void setDisplay(const std::string &display)
        {
            _display = display;
        }
        void showParamaters()
        {
            std::string param = "Computer Paramaters:\n";
            param += "\tBoard: " + _board + "\n";
            param += "\tDisplay: " + _display + "\n";
            param += "\tOs: " + _os + "\n";
            std::cout << param << std::endl;
        }
        virtual void setOs() = 0; 
    protected:
        std::string _board;
        std::string _display;
        std::string _os;
};

// 派生出具体的电脑型号
class MacBook : public Computer {
    public:
        void setOs() override {
            _os = "Mac Os x15";
        } 
};

// 抽象出建造者（建造者需要建造出每一个零件，然后建造出电脑）
class Builder {
    public:
        virtual void buildBoard(const std::string &board) = 0;
        virtual void buildDisplay(const std::string &display) = 0;
        virtual void buildOs() = 0;
        virtual std::shared_ptr<Computer> build() = 0;
};

class MacBookBuilder : public Builder {
    public:
        MacBookBuilder():_computer(new MacBook()) {};
        void buildBoard(const std::string &board) {
            _computer->setBoard(board);
        }
        void buildDisplay(const std::string &display) {
            _computer->setDisplay(display);
        }
        void buildOs() {
            _computer->setOs();
        }
        std::shared_ptr<Computer> build() {
            return _computer;
        }
    private:
        std::shared_ptr<Computer> _computer;
};

// 指挥者类（定义了各个参数所需要的信息以及顺序） - 最后通过建造者建造出电脑
class Director {
    public:
        Director(Builder *builder): _builder(builder) {}
        void construct(const std::string &board, const std::string &display)
        {
            _builder->buildBoard(board);
            _builder->buildDisplay(display);
            _builder->buildOs();
        }
    private:
        std::shared_ptr<Builder> _builder;
};

int main()
{
    Builder *builder = new MacBookBuilder();
    std::unique_ptr<Director> director(new Director(builder));
    director->construct("X型主板","Y型显示器");
    std::shared_ptr<Computer> computer = builder->build();
    computer->showParamaters();
    
    return 0;
}