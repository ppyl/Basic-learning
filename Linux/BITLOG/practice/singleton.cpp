#include <iostream>

// 单例模式（饿汉方式-以空间换时间）
// class Singleton
// {
//     private:
//         // 声明
//         static Singleton _eton;
//         // 构造函数私有化
//         Singleton():_data(99) 
//         {
//             std::cout << "单例对象构造！\n";
//         }
//         Singleton(const Singleton&) = delete;
//         ~Singleton() {}
//     private:
//         int _data;
//     public:
//         // 全局访问接口
//         static Singleton &getInstance()
//         {
//             return _eton;
//         }
//         int getData() {return _data;}
// };
// // 定义
// Singleton Singleton::_eton;

// 单例模式（懒汉方式-懒加载：延迟加载的思想---一个对象在用的时候再进行实例化）
// 由于C++11提供了静态变量的线程安全保护，所以该单例模式实现时不需要考虑线程安全问题
class Singleton
{
    private:
        Singleton():_data(99)
        {
            std::cout << "单例对象构造！\n";
        }
        Singleton(const Singleton&) = delete;
        ~Singleton() {}
    private:
        int _data;
    public:
        static Singleton &getInstance()
        {
            static Singleton _eton;
            return _eton;
        }
        int getData() {return _data;}
};

int main()
{
    std::cout << Singleton::getInstance().getData() << std::endl;
    return 0;
}