#include <iostream>
#include <memory>

class Fruit
{
    public:
        virtual void name() = 0;
};

class Apple : public Fruit
{
    public:
        void name() override
        {
            std::cout << "苹果!\n";
        }
};

class Banana : public Fruit
{
    public:
        void name() override
        {
            std::cout << "香蕉!\n";
        }
};

class Animal
{
    public:
        virtual void name() = 0;
};

class Cat : public Animal
{
    public:
        void name() override
        {
            std::cout << "猫！\n";
        }
};

class Lamp : public Animal
{
    public:
        void name() override
        {
            std::cout << "山羊！\n";
        }
};

// 抽象工厂模式
// 抽象工厂
class Factory
{
    public:
        virtual std::shared_ptr<Fruit> getFruit(const std::string &name) = 0;
        virtual std::shared_ptr<Animal> getAnimal(const std::string &name) = 0;
};

// 具体子类工厂
class FruitFactory : public Factory
{
    public:
        std::shared_ptr<Fruit> getFruit(const std::string &name)
        {
            if(name == "苹果")
            {
                return std::make_shared<Apple>();
            }
            else
            {
                return std::make_shared<Banana>();
            }
        } 
        std::shared_ptr<Animal> getAnimal(const std::string &name)
        {
            return std::shared_ptr<Animal>();
        } 
};

class AnimalFactory : public Factory
{
    public:
        std::shared_ptr<Animal> getAnimal(const std::string &name)
        {
            if(name == "猫")
            {
                return std::make_shared<Cat>();
            }
            else
            {
                return std::make_shared<Lamp>();
            }
        } 
        std::shared_ptr<Fruit> getFruit(const std::string &name)
        {
            return std::shared_ptr<Fruit>();
        } 
};

// 工厂生产类
class FactoryProducer
{
    public:
        static std::shared_ptr<Factory> create(const std::string &name)
        {
            if(name == "水果")
            {
                return std::make_shared<FruitFactory>();
            }
            else
            {
                return std::make_shared<AnimalFactory>();
            }
        }

};

// 简单工厂模式（违背开闭原则）
// class FruitFactory
// {
//     public:
//         //生产水果
//         static std::shared_ptr<Fruit> create(const std::string &name)
//         {
//             if(name == "苹果")
//             {
//                 return std::make_shared<Apple>();
//             }
//             else
//             {
//                 return std::make_shared<Banana>();
//             }
//         }
// };

// 工厂方法模式
// class FruitFactory
// {
//     public:
//         virtual std::shared_ptr<Fruit> create() = 0;
// };

// class AppleFactory : public FruitFactory
// {
//     public:
//         std::shared_ptr<Fruit> create() override
//         {
//             return std::make_shared<Apple>();
//         }
// };

// class BananaFactory : public FruitFactory
// {
//     public:
//         std::shared_ptr<Fruit> create() override
//         {
//             return std::make_shared<Banana>();
//         }
// };

int main()
{
    std::shared_ptr<Factory> ff = FactoryProducer::create("水果");
    std::shared_ptr<Fruit> fruit = ff->getFruit("苹果");
    fruit->name();
    fruit = ff -> getFruit("香蕉");
    fruit->name();

    std::shared_ptr<Factory> fa = FactoryProducer::create("动物");
    std::shared_ptr<Animal> animal = fa->getAnimal("猫");
    animal->name();
    animal = fa -> getAnimal("山羊");
    animal->name();

    // std::shared_ptr<Fruit> fruit = FruitFactory::create("苹果");
    // fruit->name();
    // fruit = FruitFactory::create("香蕉");
    // fruit->name();
 
    // std::shared_ptr<FruitFactory> ff(new AppleFactory());
    // std::shared_ptr<Fruit> fruit = ff->create();
    // fruit->name();
    // ff.reset(new BananaFactory());
    // fruit = ff->create();
    // fruit->name();

    return 0;
}