/*房东通过中介租出房子理解代理模式*/

#include<iostream>
#include<string>

// 出租屋
class RentHouse
{
    public:
        virtual void rentHouse() = 0;
};

// 房东
class Landlord : public RentHouse
{
    public:
        void rentHouse()
        {
            std::cout << "租出房子\n";
        }
};

// 中介
class Intermediary : public RentHouse
{
    public:
        void rentHouse()
        {
            std::cout << "发布招租告示\n";
            std::cout << "带人看房\n";
            _landlord.rentHouse();
            std::cout << "负责租后维修\n";
        }
    private:
        Landlord _landlord;
};

int main()
{
    Intermediary intermediary;
    intermediary.rentHouse();
    return 0;
}