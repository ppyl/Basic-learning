#include <iostream>
void xprintf()
{
    std::cout << std::endl;
}

template<typename T, typename ...Args>
void xprintf(const T &v, Args &&...args)
{
    std::cout << v;
    if((sizeof ...(args)) > 0) //获取参数包参数个数
    {
        xprintf(std::forward<Args>(args)...); // 对参数包进行完美转发
    }
    else
    {
        xprintf();
    }
}

int main()
{
    xprintf("日志");
    xprintf("日志","消息");
    xprintf("日志","消息",1001);
    return 0;
}