/*学习不定参宏函数的使用*/

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#define LOG(fmt, ...) printf("[%s:%d]" fmt, __FILE__, __LINE__, ##__VA_ARGS__);

/*C语言中不定参函数的使用，不定参数据的访问*/
void printNum(int count, ...)
{
	va_list ap;
	va_start(ap, count); //获取指定参数的起始地址（count之后第一个不定参地址）
	int i = 0;
    for(i = 0; i < count; i++)
	{
		int num = va_arg(ap, int); //参数类型（ap自动向后偏移）
		printf("param[%d]:%d\n", i, num);
	}
	va_end(ap); //将ap指针置空
}

void my_printf(const char* fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    char *res;
    int ret = vasprintf(&res, fmt, ap);
	if(ret != -1)
    {
        printf(res);
        free(res);
    }
    va_end(ap);
}

int main()
{
    LOG("日志");
    printNum(2,666,777);
    printNum(5,4,2,1,3,5);
    my_printf("%s-%d", "日志", 222);
    my_printf("日志");
    return 0;
}