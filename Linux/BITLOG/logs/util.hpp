// 防止头文件重复包含
#ifndef __M_UTIL_H_
#define __M_UTIL_H_

/*实用工具类的实现：
    1.获取系统时间
    2.判断文件是否存在
    3.获取文件所在路径
    4.创建目录
*/

#include <iostream>
#include <ctime>
#include <sys/stat.h>

//#include<unistd.h>

namespace PYLlog {
    namespace util {
        class Date {
            public:
                // 静态（无需实例化对象）
                static size_t now() {
                    return (size_t)time(nullptr);
                }
        };

        class File {
            public:
                static bool exists(const std::string &pathname) {
                    struct stat st;
                    if(stat(pathname.c_str(), &st) < 0) {
                        return false;
                    }
                    return true;
                    // 仅限于Linux平台的接口
                    //return (access(pathname.c_str(), F_OK) == 0);
                }
                static std::string path(const std::string &pathname) {
                    size_t pos = pathname.find_last_of("/\\");
                    // 没找到
                    if(pos == std::string::npos) return ".";
                    // 找到
                    return pathname.substr(0, pos + 1);
                }
                static void createDirectory(const std::string &pathname) {
                    // ./abc/efg/a
                    size_t pos = 0, idx = 0;
                    while(idx < pathname.size()) {
                        pos = pathname.find_first_of("/\\", idx);
                        if(pos == std::string::npos) {
                            mkdir(pathname.c_str(),0777);
                        }
                        // 注意：截取的路径应包含所有父级路径
                        std::string parent_dir = pathname.substr(0, pos + 1);
                        //if(parent_dir == "." || parent_dir == "..");
                        if(exists(parent_dir) == true) {
                            idx = pos + 1;
                            continue;
                        }
                        mkdir(parent_dir.c_str(),0777);
                        idx = pos + 1;
                    }
                }
        };
    }
}

#endif