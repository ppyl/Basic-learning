#include <stdio.h>

#include "myadd.h"
#include "mysub.h"

int main()
{
    int d1 = 100;
    int d2 = 39;
    printf("d1 + d2 = %d\n", myadd(d1, d2));
    printf("d1 - d2 = %d\n", mysub(d1, d2));
}
