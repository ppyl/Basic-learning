#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

//const char *err_string[] = {
//    "success",
//    "error"
//};

//int add_to_top(int top)
//{
//    printf("enter add\n");
//    int sum = 0;
//    for(int i = 0; i <= top; i++)
//    {
//        sum += i;
//    }
//    //exit(123);
//    _exit(222);
//    printf("out\n");
//    return sum;
//}

int main()
{
    pid_t id = fork();
    if(id == 0)
    {
        //child
        int cnt = 5;
        while(cnt)
        {
            printf("我是子进程，仍然存在，%dS, pid: %d, ppid%d\n", cnt--, getpid(), getppid());
            sleep(1);
            //野指针问题（异常）
            //int *p = NULL;
            //*p = 100;
        }
        exit(0);
    }
  
    // 非阻塞轮询
    while(1)
    {
        int status = 0;
        pid_t f_id = waitpid(id, &status, WNOHANG);// 非阻塞 
        if(f_id < 0)
        {
            printf("waitpid error!\n");
            exit(1);
        }
        else if(f_id == 0)
        {
            printf("子进程没有退出，父进程做其他事...\n");
            sleep(1);
            continue;
        }
        else
        {
        printf("我是父进程，等待子进程成功, pid: %d, ppid: %d, f_id: %d, child exit code: %d, child exit signal: %d\n", getpid(), getppid(), f_id, (status>>8) & 0xFF, status & 0x7F);
        break;
        }
    }
    //printf("hello"); // 输出缓冲区
    //sleep(2);
    //_exit(100);

    //for(int i = 0; i <= 200; i++)
    //{
    //    printf("%d: %s\n",i,strerror(i));
    //}
    //int result = add_to_top(100);
    //if(result == 5050) return 0;
    //else return 1;

    // 阻塞调用
    //father
    //pid_t f_id = wait(NULL);
    //int status = 0;
    //pid_t f_id = waitpid(id, &status, 0);
    //printf("我是父进程，等待子进程成功, pid: %d, ppid: %d, f_id: %d, child exit code: %d, child exit signal: %d\n", getpid(), getppid(), f_id, (status>>8) & 0xFF, status & 0x7F);

    //printf("hello"); // 输出缓冲区
    //sleep(2);
    //_exit(100);

    //for(int i = 0; i <= 200; i++)
    //{
    //    printf("%d: %s\n",i,strerror(i));
    //}
    //int result = add_to_top(100);
    //if(result == 5050) return 0;
    //else return 1;
}
