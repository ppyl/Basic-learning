#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <ctime>
#include <string>

#define NUM 10

using namespace std;

void *threadRun(void* args)
{
    const char*name = static_cast<const char *>(args);

    int cnt = 5;
    while(cnt)
    {
        cout << name << " is running: " << cnt-- << " obtain self id: " << pthread_self() << endl;
        sleep(1);
    }

    pthread_exit((void*)11); 

    // PTHREAD_CANCELED; #define PTHREAD_CANCELED ((void *) -1)
}

int main()
{
    pthread_t tid;
    pthread_create(&tid, nullptr, threadRun, (void*)"thread 1");
    // sleep(3);

    // pthread_cancel(tid);

    void *ret = nullptr;
    pthread_join(tid, &ret);
    cout << " new thread exit : " << (int64_t)ret << "quit thread: " << tid << endl;
    return 0;
}

//
void *thread_run(void *args)
{
    char *name = (char*)args;
    while(true)
    {
        cout << "new thread running" << name << endl;
        sleep(1);
    }
    delete name;
    pthread_exit(td);
    return nullptr;
}

int main()
{
    pthread_t tids[NUM];
    for(int i = 0; i < NUM; i++)
    {
        char *tname = new char[64];
        snprintf(tname, 64, "thread-%d", i+1);
        pthread_create(tids+i, nullptr, thread_run, tname);
    } 

    for(int i = 0; i < NUM; i++)
    {
        pthread_join(tids[i], nullptr);
    }

    return 0;
    // while(true)
    // {
    //     cout << "main thread running, new thread id : " << endl;
    //     sleep(1); 
    // }
    // return 0;
}