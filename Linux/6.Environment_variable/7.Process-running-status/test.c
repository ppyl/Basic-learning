#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{
    char *pwd = getenv("PWD");
    if(pwd == NULL)
        perror("getenv fail!")
    else
        printf("%s\n", pwd);
    return 0;
}
