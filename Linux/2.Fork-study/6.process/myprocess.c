#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <sys/types.h>

int main()
{
    int a = 100;
    int ret = fork();
    if(ret == 0)
    {
        //子进程
        while(1)
        {
          printf("我是子进程，pid：%d, 我的父进程是：%d, %d, %p\n", getpid(), getppid(), a, &a);
          sleep(1);
        }
    }
    else if(ret > 0)
    {
        //父进程
        while(1)
        {
          printf("我是父进程，pid：%d, 我的父进程是：%d, %d, %p\n", getpid(), getppid(), a, &a);
          a = 12345;
          sleep(1);
        }
    }
    else 
    {}

    return 0;
}
