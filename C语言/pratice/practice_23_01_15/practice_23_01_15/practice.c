#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//使用函数打印乘法口诀表
//void PrintMulTable(int n)
//{
//	for (int i = 0; i <= n; i++)
//	{
//		for (int j = 0; j <= i; j++)
//		{
//			printf("%d*%d=%-2d ", i, j, i * j);
//		}
//		printf("\n");
//	}
//}
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	PrintMulTable(a);
//	return 0;
//} 

//交换两个整数
//void Swap(int * px, int * py)
//{
//	int n = 0;
//	n = *px;
//	*px = *py;
//	*py = n;
//}
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	printf("交换前：%d %d\n", a, b);
//	Swap(&a, &b);
//	printf("交换后：%d %d", a, b);
//	return 0;
//}

//函数判断闰年
//int LeapYear(int x) 
//{
//	if ((x % 4 == 0) && (x % 100 != 0) || (x % 400 == 0))
//	{
//		return 1;
//	}
//	else
//	{
//		return 0;
//	}
//}
//int main()
//{
//	int y = 0;
//	scanf("%d", &y);
//	if (LeapYear(y) == 1)
//	{
//		printf("是闰年\n");
//	}
//	else
//	{
//		printf("不是闰年\n");
//	}
//	return 0;
//}

//函数判断素数
//#include<math.h>
//int is_prime(int n) 
//{
//	int i = 0;
//	for (i = 2; i <= sqrt(n); i++)
//	{
//		if(0==n%i)
//		{
//			return 0;
//		}
//	}
//	return 1;
//}
//int main()
//{
//	int i = 0;
//	for (i = 100; i <= 200; i++)
//	{
//		if (is_prime(i) == 1)
//		{
//			printf("%d\n", i);
//		}
//	}
//	return 0;
//}