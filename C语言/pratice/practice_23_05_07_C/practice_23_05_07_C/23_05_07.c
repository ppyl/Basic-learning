#define _CRT_SECURE_NO_WARNINGS 1

//BC1 实践出真知
#include <stdio.h>

int main() {
    printf("Practice makes perfect!");
    return 0;
}

//BC2 我是大V
#include <stdio.h>

int main() {
    printf("v   v\n");
    printf(" v v \n");
    printf("  v  \n");
    return 0;
}

//BC3 有容乃大
#include <stdio.h>

int main() {
    printf("The size of short is %d bytes.\n", sizeof(short));
    printf("The size of int is %d bytes.\n", sizeof(int));
    printf("The size of long is %d bytes.\n", sizeof(long));
    printf("The size of long long is %d bytes.\n", sizeof(long long));
    return 0;
}

//BC6 小飞机
#include <stdio.h>

int main() {
    printf("     **     \n");
    printf("     **     \n");
    printf("************\n");
    printf("************\n");
    printf("    *  *    \n");
    printf("    *  *    \n");
    return 0;
}

//BC7 缩短二进制
#include <stdio.h>

int main() {
    int a = 1234;
    printf("%#o %#Xww", a, a);
    return 0;
}

//BC8 十六进制转十进制
#include <stdio.h>

int main() {
    int a = 0XABCDEF;
    printf("%15d", a);
    return 0;
}

//BC9 printf的返回值
#include <stdio.h>

int main() {
    int a = printf("Hello world!");
    int b = printf("\n%d", a);
    return 0;
}

//BC10 成绩输入输出
#include <stdio.h>

int main() {
    int a, b, c = 0;
    scanf("%d %d %d", &a, &b, &c);
    printf("score1=%d,score2=%d,score3=%d", a, b, c);
    return 0;
}

//BC11 学生基本信息输入输出
#include <stdio.h>

int main() {
    int num = 0;
    float a, b, c = 0.0;
    scanf("%d;%f,%f,%f", &num, &a, &b, &c);
    printf("The each subject score of No. %d is %.2f, %.2f, %.2f.", num, a, b, c);
    return 0;
}

//BC12 字符圣诞树
#include <stdio.h>

int main() {
    char a = 0;
    scanf("%c", &a);
    for (int i = 1; i < 6; i++)//五层循环
    {
        for (int j = 5 - i; j > 0; j--)//打印第j行字符前的空格
        {
            printf(" ");
        }
        for (int m = 0; m < i; m++)//打印字符 第m行打印m个字符
        {
            printf("%c ", a);//字符后加一个空格
        }
        printf("\n");
    }
    return 0;
}

//BC13 ASCII码
#include <stdio.h>

int main() {
    char ch[] = { 73, 32, 99, 97, 110, 32, 100, 111, 32, 105, 116 , 33 };
    int sz = sizeof(ch) / sizeof(ch[0]);
    for (int i = 0; i < sz; i++)
    {
        printf("%c", ch[i]);
    }
    return 0;
}

//BC14 出生日期输入输出
#include <stdio.h>

int main() {
    int a = 0;
    scanf("%d", &a);
    printf("year=%d\n", a / 10000);
    printf("month=%02d\n", a / 100 % 100);
    printf("date=%02d\n", a % 100);
    return 0;
}

//BC15 按照格式输入并交换输出
#include <stdio.h>

void Swap(int* pa, int* pb)
{
    int tmp = 0;
    tmp = *pa;
    *pa = *pb;
    *pb = tmp;
}

int main() {
    int a, b = 0;
    scanf("a=%d,b=%d", &a, &b);
    Swap(&a, &b);
    printf("a=%d,b=%d", a, b);
    return 0;
}

//BC16 字符转ASCII码
#include <stdio.h>

int main() {
    char a = 0;
    scanf("%c", &a);
    getchar();
    printf("%d", a);
    return 0;
}

//BC17 计算表达式的值
#include <stdio.h>

int main() {
    int a = 40;
    int c = 212;
    printf("%d", (-8 + 22) * a - 10 + c / 2);
    return 0;
}

//BC18 计算带余除法
#include <stdio.h>

int main() {
    int a, b = 0;
    scanf("%d %d", &a, &b);
    printf("%d %d", a / b, a % b);
    return 0;
}

//BC19 反向输出一个四位数
#include <stdio.h>

int main() {
    int a = 0;
    scanf("%d", &a);
    while (a)
    {
        int b = a % 10;
        printf("%d", b);
        a = a / 10;
    }
    return 0;
}

//BC20 kiki算数
#include <stdio.h>

int main() {
    int a, b = 0;
    scanf("%d %d", &a, &b);
    if (a > 99)
    {
        a = a % 100;
    }
    else if (b > 99)
    {
        b = b % 100;
    }
    else
    {
        a = a;
        b = b;
    }
    int c = a + b;
    if (c > 99)
    {
        c = c % 100;
    }
    printf("%d", c);
    return 0;
}

//BC21 浮点数的个位数字
#include <stdio.h>

int main() {
    double a = 0;
    scanf("%lf", &a);
    int b = (double)a;
    printf("%d", b % 10);
    return 0;
}

//BC22 你能活多少秒
#include <stdio.h>

int main() {
    long long age = 0;
    scanf("%lld", &age);
    long long t = 31560000;
    printf("%lld", age * t);
    return 0;
}

//BC23 时间转换
#include <stdio.h>

int main() {
    int seconds = 0;
    scanf("%d", &seconds);
    printf("%d %d %d", seconds / 3600, seconds % 3600 / 60, seconds % 60);
    return 0;
}

//BC24 总成绩和平均分计算
#include <stdio.h>

int main() {
    float a, b, c = 0.0;
    scanf("%f %f %f", &a, &b, &c);
    float sum = a + b + c;
    float ave = (sum) / 3.0;
    printf("%.2f %.2f", sum, ave);
    return 0;
}

//BC25 计算体重指数
#include <stdio.h>

int main() {
    int a, b = 0;
    scanf("%d %d", &a, &b);
    printf("%.2f", a / ((b * 0.01) * (b * 0.01)));
    return 0;
}

//BC26 计算三角形的周长和面积
#include <stdio.h>
#include <math.h>

int main() {
    int a, b, c = 0;
    scanf("%d %d %d", &a, &b, &c);
    float circumference = a + b + c;
    float d = circumference / 2.0;
    float area = sqrt(d * (d - a) * (d - b) * (d - c));
    printf("circumference=%.2f area=%.2f", circumference, area);
    return 0;
}

//BC27 计算球体的体积
#include <stdio.h>
#include <math.h>
#define PI 3.1415926

int main() {
    double r = 0.0;
    scanf("%lf", &r);
    printf("%.3lf", 4.0 / 3.0 * PI * pow(r, 3));
    return 0;
}

//BC28 大小写转换
#include <stdio.h>

int main() {
    char a = 0;
    while (scanf("%c", &a) != EOF)
    {
        getchar();
        //putchar(a + 32);
        printf("%c\n", a + 32);
        //printf("\n");
    }
    return 0;
}

//BC29 2的n次方计算
#include <stdio.h>

int main() {
    int n = 0;
    int p = 0;
    while (scanf("%d", &n) != EOF)
    {
        p = 1 << n;
        printf("%d\n", p);
    }
    return 0;
}

//BC30 KiKi和酸奶
#include <stdio.h>

int main() {
    int n, h, m = 0;
    while (scanf("%d %d %d", &n, &h, &m) != EOF)
    {
        int x = 0;
        x = m / h;
        if (m % h == 0)
        {
            printf("%d", n - x);
        }
        else
        {
            printf("%d", n - x - 1);
        }
    }
    return 0;
}

//BC31 发布信息
#include <stdio.h>

int main() {
    printf("I lost my cellphone!");
    return 0;
}

//BC32 输出学生信息
#include <stdio.h>

int main() {
    printf("Name    Age    Gender\n");
    printf("---------------------\n");
    printf("Jack    18     man");
    return 0;
}

//BC33 计算平均成绩
#include <stdio.h>

int main() {
    int a, b, c, d, e = 0;
    scanf("%d %d %d %d %d", &a, &b, &c, &d, &e);
    printf("%.1f", (a + b + c + d + e) / 5.0);
    return 0;
}

//BC34 进制A+B
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include<string.h>

int con(char a[], char b[])
{
    int x = 0, y = 0;
    for (int i = strlen(a) - 1; i > 1; i--)
    {
        if ('0' <= a[i] && '9' >= a[i])
        {
            x += (a[i] - '0') * pow(16, strlen(a) - i - 1);
        }
        if ('A' <= a[i] && 'F' >= a[i])
        {
            x += (a[i] - 'A' + 10) * pow(16, strlen(a) - i - 1);
        }
    }
    for (int i = strlen(b) - 1; i > 0; i--)
    {
        if ('0' <= b[i] && '8' >= b[i])
        {
            y += (b[i] - '0') * pow(8, strlen(b) - i - 1);
        }
    }

    return x + y;
}

int main() {
    char a[13];
    char b[13];
    scanf("%s %s", a, b);
    int ret = con(a, b);
    printf("%d", ret);
    return 0;
}

#include <stdio.h>

int main() {
    int a, b = 0;
    scanf("%x %o", &a, &b);
    printf("%d", a + b);
    return 0;
}

//BC35 判断字母
#include <stdio.h>

int main() {
    char a = '\0';
    scanf("%c", &a);
    if (a >= 'a' && a <= 'z' || a >= 'A' && a <= 'Z')
        printf("YES");
    else
        printf("NO");
    return 0;
}

//BC36 健康评估
#include <stdio.h>

int main() {
    double h, t = 0;
    scanf("%lf %lf", &h, &t);
    double BMI = h / (t * t);
    if (BMI >= 18.5 && BMI <= 23.9)
        printf("Normal");
    else
        printf("Abnormal");
    return 0;
}

//BC37 网购
#include <stdio.h>

int main() {
    float money = 0.0;
    int m, d, q = 0;
    float sum = 0;
    scanf("%f %d %d %d", &money, &m, &d, &q);
    if (m == 11 && d == 11)
    {
        if (q)
        {
            sum = money * 0.7 - 50;
            if (sum > 0)
                printf("%.2f", sum);
            else
                printf("0.00");
        }
        else
        {
            printf("%.2f", money * 0.7);
        }
    }
    else if (m == 12 && d == 12)
    {
        if (q)
        {
            sum = money * 0.8 - 50;
            if (sum > 0)
                printf("%.2f", sum);
            else
                printf("0.00");
        }
        else
        {
            printf("%.2f", money * 0.8);
        }
    }
    else
    {
        printf("%.2f", money);
    }
    return 0;
}

//BC38 变种水仙花
#include <stdio.h>

int main() {
    for (int i = 10000; i < 100000; i++)
    {
        int ret = 0;
        for (int j = 10; j <= 10000; j *= 10)
        {
            列出所求数值，累加
            ret += (i / j) * (i % j);
        }
        if (ret == i)
        {
            printf("%d ", i);
        }
    }
    return 0;
}

//BC39 争夺前五名
#include <stdio.h>

int main() {
    int n = 0;
    scanf("%d", &n);
    int a[55] = { 0 };
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &a[i]);
    }
    for (int i = 0; i < n; i++)
    {
        int tmp = 0;
        for (int j = i + 1; j < n; j++)
        {
            if (a[i] < a[j])
            {
                tmp = a[i];
                a[i] = a[j];
                a[j] = tmp;
            }
        }
    }
    for (int i = 0; i < 5; i++)
    {
        printf("%d ", a[i]);
    }
    return 0;
}

//BC40 竞选社长
#include<stdio.h>

int main(void)
{
    char c;
    int a = 0, b = 0;
    while ((c = getchar()) != '0')
    {
        if (c == 'A')
            a++;
        else
            b++;
    }
    if (a > b)
        printf("A");
    else if (a < b)
        printf("B");
    else
        printf("E");
    return 0;
}

//BC41 你是天才吗？
#include <stdio.h>

int main() {
    int a = 0;
    scanf("%d", &a);
    if (a >= 140)
        printf("Genius");
    else
        return -1;
    return 0;
}

//BC42 完美成绩
#include <stdio.h>

int main() {
    int a = 0;
    while (scanf("%d", &a) != EOF)
    {
        if (a >= 90 && a <= 100)
            printf("Perfect");
        else
            return -1;
    }
    return 0;
}

//BC43 及格分数
#include <stdio.h>

int main() {
    int a = 0;
    while (scanf("%d", &a) != EOF)
    {
        if (a >= 60)
            printf("Pass\n");
        else
            printf("Fail\n");
    }
    return 0;
}

//BC44 判断整数奇偶性
#include <stdio.h>

int main() {
    int a = 0;
    while (scanf("%d", &a) != EOF)
    {
        if (a % 2)
        {
            printf("Odd\n");
        }
        else {
            printf("Even\n");
        }
    }
    return 0;
}

//BC45 最高分数
#include <stdio.h>

int main() {
    int a = 0, b = 0, c = 0;
    scanf("%d %d %d", &a, &b, &c);
    if (a > b)
    {
        if (a > c)
        {
            printf("%d", a);
        }
        else {
            printf("%d", c);
        }
    }
    else
    {
        if (b > c)
        {
            printf("%d", b);
        }
        else {
            printf("%d", c);
        }
    }
    return 0;
}

//BC46 判断是元音还是辅音
#include <stdio.h>

int main() {
    char a = 0;
    while (scanf("%c", &a) != EOF)
    {
        getchar();
        if (a >= 'A' && a <= 'Z' || a >= 'a' && a <= 'z')
        {
            if (a == 'A' || a == 'a' || a == 'E' || a == 'e' || a == 'I' || a == 'i' || a == 'O' || a == 'o' || a == 'U' || a == 'u')
                printf("Vowel\n");
            else
                printf("Consonant\n");
        }
    }
    return 0;
}

//BC47 判断是不是字母
#include <stdio.h>

int main() {
    char a = 0;
    while (scanf("%c", &a) != EOF)
    {
        getchar();
        if (a >= 'A' && a <= 'a' || a >= 'a' && a <= 'z')
        {
            printf("%c is an alphabet.\n", a);
        }
        else {
            printf("%c is not an alphabet.\n", a);
        }
    }
    return 0;
}

//BC48 字母大小写转换
#include <stdio.h>

int main() {
    char a = 0;
    while (scanf("%c", &a) != EOF)
    {
        if (a >= 'A' && a <= 'Z' || a >= 'a' && a <= 'z')
        {
            if (a >= 'A' && a <= 'Z')
            {
                printf("%c\n", a + 32);
            }
            if (a >= 'a' && a <= 'z')
            {
                printf("%c\n", a - 32);
            }
        }
    }
    return 0;
}

//BC49 判断两个数的大小关系
#include <stdio.h>

int main() {
    int a = 0, b = 0;
    while (scanf("%d %d", &a, &b) != EOF)
    {
        if (a > b)
        {
            printf("%d>%d\n", a, b);
        }
        if (a == b)
        {
            printf("%d=%d\n", a, b);
        }
        if (a < b)
        {
            printf("%d<%d\n", a, b);
        }
    }
    return 0;
}

//BC50 计算单位阶跃函数
#include <stdio.h>

int main() {
    int a = 0;
    while (scanf("%d", &a) != EOF)
    {
        if (a > 0)
        {
            printf("%d\n", 1);
        }
        if (a == 0)
        {
            printf("%.1f\n", 0.5);
        }
        if (a < 0)
        {
            printf("%d\n", 0);
        }
    }
    return 0;
}

//BC51 三角形判断
#include <stdio.h>

int main() {
    int a = 0, b = 0, c = 0;
    while (scanf("%d %d %d", &a, &b, &c) != EOF)
    {
        if (a + b > c && a + c > b && b + c > a)
        {
            if (a == b && b == c)
            {
                printf("Equilateral triangle!\n");
            }
            else if (a == b || a == c || b == c)
            {
                printf("Isosceles triangle!\n");
            }
            else {
                printf("Ordinary triangle!\n");
            }
        }
        else {
            printf("Not a triangle!\n");
        }
    }
    return 0;
}

//BC52 衡量人体胖瘦程度
#include <stdio.h>

int main() {
    float a = 0.0, b = 0.0;

    while (scanf("%f %f", &a, &b) != EOF)
    {
        float d = b / 100.0;
        float c = 0.0;
        c = a / (d * d);
        if (c < 18.5)
        {
            printf("Underweight\n");
        }
        else if (c >= 18.5 && c <= 23.9)
        {
            printf("Normal\n");
        }
        else if (c > 23.9 && c <= 27.9)
        {
            printf("Overweight\n");
        }
        else if (c > 27.9)
        {
            printf("Obese\n");
        }
        else
            return -1;
    }
    return 0;
}

//BC53 计算一元二次方程
#include <stdio.h>
#include<math.h>

int main() {
    float a = 0.0, b = 0.0, c = 0.0;
    while (scanf("%f %f %f", &a, &b, &c) != EOF)
    {
        float s = b * b - 4 * a * c;
        if (a == 0)
        {
            printf("Not quadratic equation\n");
        }
        else {
            if (s == 0)
                printf("x1=x2=%.2f\n", (-b) / (2.0 * a) + 0);
            else if (s > 0)
            {
                printf("x1=%.2f;x2=%.2f\n", ((-b) - sqrt(s)) / (2 * a), ((-b) + sqrt(s)) / (2 * a));
            }
            else
            {
                printf("x1=%.2f-%.2fi;x2=%.2f+%.2fi\n", (-b) / (2 * a), (sqrt(-s) / (2 * a)),
                    (-b) / (2 * a), (sqrt(-s) / (2 * a)));
            }
        }
    }
    return 0;
}

//BC54 获得月份天数
#include <stdio.h>

int main() {
    int a = 0, b = 0;
    while (scanf("%d %d", &a, &b) != EOF)
    {
        if (b == 1 || b == 3 || b == 5 || b == 7 || b == 8 || b == 10 || b == 12)
            printf("31\n");
        else if (b != 2)
            printf("30\n");
        if (a % 400 == 0 || a % 4 == 0 && a % 100 != 0)
        {
            if (b == 2)
                printf("29\n");
        }
        else {
            if (b == 2)
                printf("28\n");
        }
    }
    return 0;
}

//BC55 简单计算器
#include <stdio.h>

int main() {
    double a = 0.0, b = 0.0;
    char c = 0;
    while (scanf("%lf%c%lf", &a, &c, &b) != EOF)
    {
        if (c == '+')
        {
            printf("%.4lf+%.4lf=%.4lf\n", a, b, a + b);
        }
        else if (c == '-')
        {
            printf("%.4lf-%.4lf=%.4lf\n", a, b, a - b);
        }
        else if (c == '*')
        {
            printf("%.4lf*%.4lf=%.4lf\n", a, b, a * b);
        }
        else if (c == '/')
        {
            if (b == 0.0)
                printf("Wrong!Division by zero!\n");
            else
                printf("%.4lf/%.4lf=%.4lf\n", a, b, a / b);
        }
        else {
            printf("Invalid operation!\n");
        }
    }
    return 0;
}

//BC56 线段图案
#include <stdio.h>

int main() {
    int a = 0;
    while (scanf("%d", &a) != EOF)
    {
        for (int i = 0; i < a; i++)
        {
            printf("*");
        }
        printf("\n");
    }
    return 0;
}

//BC57 正方形图案
#include <stdio.h>

int main() {
    int a = 0;
    while (scanf("%d", &a) != EOF)
    {
        for (int i = 1; i <= a; i++)
        {
            for (int j = 1; j <= a; j++)
            {
                if (j == a)
                    printf("*\n");
                else
                    printf("* ");
            }
        }
    }
    return 0;
}

//BC58 直角三角形图案
#include <stdio.h>

int main() {
    int a = 0;
    while (scanf("%d", &a) != EOF)
    {
        for (int i = 0; i < a; i++)
        {
            for (int j = a - i - 1; j < a; j++)
            {
                printf("* ");
            }
            printf("\n");
        }
    }
    return 0;
}

//BC59 翻转直角三角形图案
#include <stdio.h>

int main() {
    int a = 0;
    while (scanf("%d", &a) != EOF)
    {
        for (int i = 0; i < a; i++)
        {
            for (int j = i; j < a; j++)
            {
                printf("* ");
            }
            printf("\n");
        }
    }
    return 0;
}

//BC60 带空格直角三角形图案
#include <stdio.h>

int main() {
    int a = 0;
    while (scanf("%d", &a) != EOF)
    {
        for (int i = 0; i < a; i++)
        {
            for (int j = 0; j < a - i - 1; j++)
            {
                printf("  ");
            }
            for (int m = 0; m <= i; m++)
            {
                printf("* ");
            }
            printf("\n");
        }
    }
    return 0;
}

//BC61 金字塔图案
#include <stdio.h>

int main() {
    int n = 0;
    while (scanf("%d", &n) != EOF)
    {
        for (int j = 1; j <= n; j++)
        {
            for (int i = 1; i <= n - j; i++)
            {
                printf(" ");
            }
            for (int q = 1; q <= j; q++)
            {
                printf("* ");
            }
            printf("\n");
        }
    }
    return 0;
}

//BC62 翻转金字塔图案
#include <stdio.h>

int main() {
    int n = 0;
    while (scanf("%d", &n) != EOF)
    {
        for (int j = 1; j <= n; j++)
        {
            for (int q = 1; q <= j - 1; q++)
            {
                printf(" ");
            }
            for (int i = j; i <= n; i++)
            {
                printf("* ");
            }
            printf("\n");
        }
    }
    return 0;
}

//BC63 菱形图案
#include <stdio.h>

int main() {
    int n = 0;
    while (scanf("%d", &n) != EOF)
    {
        for (int i = 0; i < n + 1; i++)
        {
            for (int j = 0; j < n - i; j++)
            {
                printf(" ");
            }
            for (int j = 0; j <= i; j++)
            {
                printf("* ");
            }
            printf("\n");

        }

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j <= i; j++)
            {
                printf(" ");
            }
            for (int j = 0; j < n - i; j++)
            {
                printf("* ");
            }
            printf("\n");
        }
    }
    return 0;
}

//BC64 K形图案
#include <stdio.h>

int main() {
    int n = 0;
    while (scanf("%d", &n) != EOF)
    {
        for (int i = n + 1; i > 0; i--)
        {
            for (int j = i; j > 0; j--)
            {
                printf("* ");
            }
            printf("\n");
        }
        for (int i = 1; i <= n; i++)
        {
            for (int j = 0; j <= i; j++)
            {
                printf("* ");
            }
            printf("\n");
        }
    }
    return 0;
}

//BC65 箭形图案
#include <stdio.h>

int main() {
    int n = 0;
    while (scanf("%d", &n) != EOF)
    {
        for (int i = 0; i <= n; i++)
        {
            for (int j = 0; j < 2 * (n - i); j++)
            {
                printf(" ");
            }
            for (int j = 0; j <= i; j++)
            {
                printf("*");
            }
            printf("\n");
        }
        for (int i = 1; i <= n; i++)
        {
            for (int j = 0; j < 2 * i; j++)
            {
                printf(" ");
            }
            for (int j = 0; j <= n - i; j++)
            {
                printf("*");
            }
            printf("\n");
        }
    }
    return 0;
}

//BC66 反斜线形图案
#include <stdio.h>

int main() {
    int n = 0;
    while (scanf("%d", &n) != EOF)
    {
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < i; j++)
            {
                printf(" ");
            }
            printf("*\n");
        }
    }
    return 0;
}

//BC67 正斜线形图案
#include <stdio.h>

int main() {
    int n = 0;
    while (scanf("%d", &n) != EOF)
    {
        for (int i = 0; i < n; i++)
        {
            for (int j = i + 1; j < n; j++)
            {
                printf(" ");
            }
            printf("*\n");
        }
    }
    return 0;
}

//BC68 X形图案
#include <stdio.h>

int main() {
    int n = 0;
    while (scanf("%d", &n) != EOF)
    {
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (i == j || i + j == n - 1)
                {
                    printf("*");
                }
                else
                {
                    printf(" ");
                }
            }
            printf("\n");
        }
    }
    return 0;
}

//BC69 空心正方形图案
#include <stdio.h>

int main() {
    int n = 0;
    while (scanf("%d", &n) != EOF)
    {
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (i == 0 || j == 0 || i == n - 1 || j == n - 1)
                {
                    printf("* ");
                }
                else
                {
                    printf("  ");
                }
            }
            printf("\n");
        }
    }
    return 0;
}

//BC70 空心三角形图案
#include <stdio.h>

int main() {
    int n = 0;
    while (scanf("%d", &n) != EOF)
    {
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < i + 1; j++)
            {
                if (i == j || i == 0 || j == n - 1 || j == 0 || i == n - 1)
                {
                    printf("* ");
                }
                else
                {
                    printf("  ");
                }
            }
            printf("\n");
        }
    }
    return 0;
}