#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

////n的阶乘
////非递归
//long long Fuc(int n)
//{
//	long long ret = 1;
//	for (int i = 2; i <= n; i++)
//	{
//		ret *= i;
//	}
//	return ret;
//}

////递归
//long long Fuc(int n)
//{
//	if (n <= 1)
//	{
//		return 1;
//	}
//	else
//	{
//		return Fuc(n - 1) * n;
//	}
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	printf("%d\n", Fuc(n));
//	return 0;
//}

//递归打印一个整数的每一位

//void print(int n)
//{
//	if (n > 9)
//	{
//		print(n / 10);
//	}
//	printf("%d ", n % 10);
//}
//
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	print(a);
//	return 0;
//}