#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//int main() {
//    int n = 0;
//    int count = 0;
//    while ((n = getchar()) != '0') {
//        if (n == 'A') {
//            count++;
//        }
//        else if (n == 'B') {
//            count--;
//        }
//    }
//    if (count > 0) {
//        printf("A\n");
//    }
//    else if (count < 0) {
//        printf("B\n");
//    }
//    else {
//        printf("E\n");
//    }
//    return 0;
//}

//int main() {
//    int n = 0;
//    int countA = 0;
//    int countB = 0;
//    while ((n = getchar()) != '0') {
//        if (n == 'A') {
//            countA++;
//        }
//        else if (n == 'B') {
//            countB++;
//        }
//    }
//    if (countA > countB) {
//        printf("A\n");
//    }
//    else if (countA < countB) {
//        printf("B\n");
//    }
//    else {
//        printf("E\n");
//    }
//    return 0;
//}

//int main() {
//    int a, b, c = 0;
//    while (scanf("%d %d %d", &a, &b, &c) == 3) {
//        if (a + b > c && a + c > b && b + c > a) {
//            if (a == b && b == c) {
//                printf("Equilateral triangle!\n");
//            }
//            else if ((a == b && b != c) || (a == c && a != b) || (b == c && b != a)) {
//                printf("Isosceles triangle!\n");
//            }
//            else {
//                printf("Ordinary triangle!\n");
//            }
//        }
//        else {
//            printf("Not a triangle!\n");
//        }
//    }
//    return 0;
//}