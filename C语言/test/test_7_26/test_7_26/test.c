#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int main()
{
    char str[] = "hello bit";
    printf("%d %d\n", sizeof(str), strlen(str));
    return 0;
}

void Swap(int A[], int B[], int sz) {
	
	for (int i = 0; i < sz; i++) {
		int temp = A[i];
		A[i] = B[i];
		B[i] = temp;
	}
}

int main() {
	int i = 0;
	int A[6];
	int B[6];
	int sz = sizeof(A) / sizeof(A[0]);
	for (i = 0; i < sz; i++)
	{
		scanf("%d ", &A[i]);
	}
	for (i = 0; i < sz; i++)
	{
		scanf("%d ", &B[i]);
	}
	Swap(A, B, sz);
	for (i = 0; i < sz; i++)
	{
		printf("%d ", A[i]);
	}
	printf("\n");
	for (i = 0; i < sz; i++)
	{
		printf("%d ", B[i]);
	}
	printf("\n");
	return 0;
}

void init(int arr1[], int sz) {
	int i;
	for (i = 0; i < sz; i++) {
		arr1[i] = 0;
	}
}

void print(int arr1[], int sz) {
	int i = 0;
	for (i = 0; i < sz; i++) {
		printf("%d", arr1[i]);
	}
	printf("\n");
}

void reverse(int arr1[], int sz) {
	int temp;
	int left = 0;
	int right = sz - 1;
	while (left < right) {
		temp = arr1[left];
		arr1[left] = arr1[right];
		arr1[right] = temp;
		left++;
		right--;
	}
}

int main() {
	int arr1[6] = { 1,2,3,4,5,6 };
	int sz = sizeof(arr1) / sizeof(arr1[0]);
	/*init(arr1, sz);*/
	print(arr1, sz);
	reverse(arr1, sz);
	print(arr1, sz);
	return 0;
}

void bubble_sort(int arr[], int sz)
{
	int i = 0;
	for (i = 0; i < sz-1; i++)
	{
		int j = 0;
		for (j = 0; j < sz-1-i; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				int tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
			}
		}
	}
}

int main()
{
	int arr[6] = {3,1,4,2,8,0};
	int sz = sizeof(arr) / sizeof(arr[0]);
	bubble_sort(arr, sz);
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}

#include<stdio.h>

int main() {
    int n, m = 0;
    scanf("%d %d", &n, &m);
    int arr[n][m];
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            scanf("%d", &arr[i][j]);
        }
    }
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            printf("%d ", arr[j][i]);
        }
        printf("\n");
    }
    return 0;
}

int DigitSum(int n)
{
	int sum = 0;
	int m = 0;
	if (n != 0)
	{
		m = n % 10;
		n = n / 10;
		sum = m + DigitSum(n);
	}
	return sum;
}
int main()
{
	int a;
	scanf("%d", &a);
	int b = DigitSum(a);
	printf("%d\n",b);
	return 0;
}
