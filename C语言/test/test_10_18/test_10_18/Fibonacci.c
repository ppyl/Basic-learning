#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int Fibonacci(int n) {
	int a = 0;
	int b = 1;
	int c = 0;
	while (1) {
		c = a + b;
		a = b;
		b = c;
		if (b >= n) {
			break;
		}
	}
	int x = 0;
	int y = 0;
	int max = b;
	int min = a;
	x = max - n;
	y = n - min;
	return x > y ? y : x;
}

int main() {
	int N = 0;
	scanf("%d", &N);
	int ret = Fibonacci(N);
	printf("%d\n", ret);
	return 0;
}