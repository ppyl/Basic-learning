#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<assert.h>

void* my_memmove(void* dest, void* src, size_t num)
{
	void* ret = dest;
	assert(dest);
	assert(src);

	//一个字节一个字节拷贝
	if (dest < src)
	{
		while (num--) {
			*(char*)dest = *(char*)src;
			dest = (char*)dest + 1;
			src = (char*)src + 1;
		}
	}
	else
	{
		while (num--) {
			*((char*)dest + num) = *((char*)src + num);
		}
	}
	return ret;
}

int main2()
{
	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
	my_memmove(arr1 + 2, arr1, 20);
	int i = 0;
	for (i = 0; i < 10; i++) {
		printf("%d", arr1[i]);
	}
	/*int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
	int arr2[5] = { 0 };
	my_memcpy(arr2, arr1, 20);
	int i = 0;
	for (i = 0; i < 10; i++) {
		printf("%d", arr2[i]);
	}
	float arr3[] = { 1.0f,2.0f,3.0f,4.0f,5.0f };
	float arr4[5] = { 0.0 };
	my_memcpy(arr4, arr3, 10);*/
	return 0;
}