#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

void replaceSpace(char* str, int len) {
	int space_count = 0;
	char* cur = str;
	while (*cur) {
		if (*cur == ' ') {
			space_count++;
		}
		cur++;
	}
	char* end1 = str + len;
	char* end2 = str + len + 2 + space_count;
	while (end1 != end2) {
		if (*end1 != ' ') {
			*end2-- = *end1--;
		}
		else {
			*end2-- = '0';
			*end2-- = '2';
			*end2-- = '%';
			*end1--;
		}
	}
}

int main() {
	char arr[40] = "We Are Happy";
	int len = strlen(arr);
	replaceSpace(arr, len);
	printf("%s\n", arr);
	return 0;
}