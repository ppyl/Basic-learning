#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>

int main() {
	char arr[] = "abc@def.com" ;
	char arr2[] = "123@456.com";
	char buf[200] = {0};
	strcpy(buf, arr);

	const char* p = "@.";
	const char* p2 = "@.";

	char* str = strtok(buf, p);
	printf("%s\n", str);
	char* str2 = strtok(arr2, p2);
	printf("%s\n", str2);

	str = strtok(NULL, p);
	printf("%s\n", str);

	str2 = strtok(NULL, p2);
	printf("%s\n", str2);

	str = strtok(NULL, p);
	printf("%s\n", str);

	str2 = strtok(NULL, p2);
	printf("%s\n", str2);
	
	return 0;
}