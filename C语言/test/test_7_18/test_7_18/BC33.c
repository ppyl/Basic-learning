#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int main()
{
	int a, b, c, d, e = 0;
	float p = 0;
	scanf("%d %d %d %d %d", &a, &b, &c, &d, &e);
	p = (a + b + c + d + e) / 5.0;
	printf("%.1f\n", p);
	return 0;
}
