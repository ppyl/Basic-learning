#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int main()
{
	int s, a, b;
	scanf("%d", &s);
	a = s / 3600;
	b = s % 3600 / 60;
	s = s % 60;
	printf("%d %d %d", a, b, s);
	return 0;
}