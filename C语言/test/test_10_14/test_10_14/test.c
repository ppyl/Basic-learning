#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>
#include<limits.h>

enum Status
{
	VALID,
	INVALID
};

enum Status status = INVALID;;

int my_atoi(const char* str) {
	if (str == NULL) {
		return 0;
	}
	if (*str == '\0') {
		return 0;
	}
	while (isspace(*str)) {
		str++;
	}
	int flag = 0;
	if (*str == '+') {
		flag = 1;
		str++;
	}
	else if (*str == '-') {
		flag = -1;
		str++;
	}
	long long ret = 0;
	while (isdigit(*str)) {
		ret = ret * 10 + flag*(*str - '0');
		if (ret<INT_MIN || ret>INT_MAX) {
			return 0;
		}
		str++;
	}
	if (*str == '\0') {
		status = VALID;
		return (int)ret;
	}
	else {
		return (int)ret;
	}
}

//int main() {
//	int ret = my_atoi("13");
//	if (status == VALID)
//		printf("合法的转换：%d", ret);
//	else
//		printf("非法的转换：%d", ret);
//	return 0;
//}

//int main() 
//{
//	//异或操作
//	int arr[] = {1,2,3,4,5,1,2,3,4,6};
//	int i = 0;
//	int ret = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (i = 0; i < sz; i++) {
//		ret ^= arr[i];
//	}
//	int ans = 0;;
//	for (i = 0; i < 32; i++) {
//		if (((ret >> i) & 1) == 1) {
//			ans = i;
//			break;
//		}
//	}
//	int n1 = 0;
//	int n2 = 0;
//	for (i = 0; i < sz; i++) {
//		if (((arr[i] >> ans) & 1) == 1) {
//			n1 ^= arr[i];
//		}
//		else {
//			n2 ^= arr[i];
//		}
//	}
//	printf("%d %d\n", n1, n2);
//	return 0;
//}

//int main() {
//	enum ENUM_A
//	{
//		X1,
//		Y1,
//		Z1 = 255,
//		A1,
//		B1,
//	};
//
//	enum ENUM_A enumA = Y1;
//	enum ENUM_A enumB = B1;
//	return 0;
//}

//union Un
//{
//	short s[7];
//	int n;
//};
//
//int main() {
//	printf("%d\n", sizeof(union Un));
//	return 0;
//}