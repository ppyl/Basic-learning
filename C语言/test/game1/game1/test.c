#define _CRT_SECURE_NO_WARNINGS 1
#include "game.h"

void menu()
{
	printf("***********************\n");
	printf("------  1. play  ------\n");
	printf("------  0. exit  ------\n");
	printf("***********************\n");
}

void game()
{
	char ret = 0;
	//数据的存储需要的二维数组
	char board[ROW][COL] = { 0 };
	//这里可以进行数组的初始化，防止'\n'等因素无法打印造成棋盘的错误
	init_board(board, ROW, COL);
	//打印棋盘
	display_board(board, ROW, COL);
	//玩游戏 
	while (1)//死循环，直到结果跳出
	{
		player_move(board, ROW, COL);
		display_board(board, ROW, COL);

		ret = is_win(board, ROW, COL);
		if (ret != 'R')
			break;

		computer_move(board, ROW, COL);
		display_board(board, ROW, COL);

		ret = is_win(board, ROW, COL);
		if (ret != 'R')
			break;

	}
	//定义特殊字符
	if (ret == '*')
	{
		printf("恭喜你获胜！\n");
	}
	else if (ret == '#')
	{
		printf("弱爆了，败给了人机！\n");
	}
	else if (ret == 'P')
	{
		printf("打成平手，继续训练吧！\n");
	}
	display_board(board, ROW, COL);
}

//玩家赢 - '*'
//电脑赢 - '#'
//平局了 - 'P'
//游戏继续 - 'R'

int main()
{
	int input = 0;
	//设置时间戳创建随机数
	srand((unsigned int)time(NULL));

	do
	{
		//菜单函数调用
		menu();
		printf("请选择:>");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("选择错误，重新选择!\n");
			break;
		}
	} while (input);

	return 0;
}