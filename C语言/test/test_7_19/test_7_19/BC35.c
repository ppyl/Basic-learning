#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int main() {
    char a = '\0';
    scanf("%c", &a);
    getchar();
    if ((a >= 'a' && a <= 'z') || (a >= 'A' && a <= 'Z'))
        printf("YES");
    else
        printf("NO");
    return 0;
}