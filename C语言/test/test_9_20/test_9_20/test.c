#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//int main() {
//    int n = 0;
//    while (scanf("%d", &n) == 1) {
//        switch (n) {
//        case 200:
//            printf("OK\n");
//            break;
//        case 202:
//            printf("Accepted\n");
//            break;
//        case 400:
//            printf("Bad Request\n");
//            break;
//        case 403:
//            printf("Forbidden\n");
//            break;
//        case 404:
//            printf("Not Found\n");
//            break;
//        case 500:
//            printf("Internal Server Error\n");
//            break;
//        case 502:
//            printf("Bad Gateway\n");
//            break;
//        }
//    }
//    return 0;
//}

//int main() {
//    int num = 0;
//    int max = 0;
//    int min = 100;
//    int sum = 0;
//    int count = 0;
//    while (scanf("%d", &num) == 1) {
//        if (num > max) {
//            max = num;
//        }
//        if (num < min) {
//            min = num;
//        }
//        sum += num;
//        count++;
//        if (count == 7) {
//            printf("%.2lf\n", (sum - max - min) / 5.0);
//            max = 0;
//            min = 100;
//            sum = 0;
//            count = 0;
//        }
//    }
//    return 0;
//}

//int main() {
//    int arr[7] = { 0 };
//    while (scanf("%d %d %d %d %d %d %d", &arr[0], &arr[1], &arr[2], &arr[3], &arr[4], &arr[5], &arr[6]) == 7) {
//        int max = 0;
//        int min = 100;
//        int sum = 0;
//        for (int i = 0; i < 7; i++) {
//            if (arr[i] > max) {
//                max = arr[i];
//            }
//            if (arr[i] < min) {
//                min = arr[i];
//            }
//            sum += arr[i];
//        }
//        printf("%.2lf\n", (sum - max - min) / 5.0);
//    }
//    return 0;
//}