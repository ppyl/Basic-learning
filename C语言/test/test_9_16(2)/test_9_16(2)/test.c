#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>



int main() {
    int arr[7] = { 0 };
    while (scanf("%d %d %d %d %d %d %d", &arr[0], &arr[1], &arr[2], &arr[3],
        &arr[4], &arr[5], &arr[6]) == 7) {
        int i = 0;
        int max = 0;
        int min = 0;
        int sum = 0;
        for (i = 0; i < 7; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
            if (arr[i] > min) {
                min = arr[i];
            }
            sum += arr[i];
            printf("%.2lf\n", (sum - max - min) / 5.0);

        }
    }
    return 0;
}