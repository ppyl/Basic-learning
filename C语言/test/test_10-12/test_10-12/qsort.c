#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
void print(int arr[], int sz);

struct Stu
{
	char name[20];
	int age;
};

////qsort 函数可以排序任意类型的数据
int cmp_stu_name(const void* e1, const void* e2) {
	return strcmp(((struct Stu*)e1)->name,((struct Stu*)e2)->name);
}

int cmp_stu_age(const void* e1, const void* e2) {
	return ((struct Stu*)e1)->age - ((struct Stu*)e2)->age;
}

////测试qsort函数排序结构体数据
int main() {
	struct Stu s[] = { {"zhangsan",20} ,{"lisi",28},{"wangwu",19} };
	int sz = sizeof(s) / sizeof(s[0]);
	//按照名字排序
	qsort(s, sz, sizeof(s[0]), cmp_stu_name);
	//按照年龄排序
	/*qsort(s, sz, sizeof(s[0]), cmp_stu_age);*/
	for (int i = 0; i < sz; i++) {
		printf("%s %d ", s[i].name, s[i].age);
	} 
}

//void Swap(char* buf1, char* buf2, int width) {
//	int i = 0;
//	for (i = 0; i < width; i++) {
//		char temp = *buf1;
//		*buf1 = *buf2;
//		*buf2 = temp;
//		buf1++;
//		buf2++;
//	}
//}
//
//void bubble_qsort(void* base,int sz,int width,int (*cmp)(const void* e1,const void* e2)) 
//{
//	int i = 0;
//	//趟数
//	for(i=0;i<sz-1;i++){
//		int j = 0;
//		for (j = 0;j<sz-i-1; j++) {
//			if (cmp((char*)base+j*width,(char*)base+(j+1)*width)>0){
//				Swap((char*)base + j * width, (char*)base + (j + 1) * width,width);
//			}
//		}
//	}
//}

//int cmp(const void* e1, const void* e2) {
//	return (*(int*)e1 - *(int*)e2);
//}
////冒泡排序借鉴使用qsort函数
//int main() {
//	int arr[] = { 3,1,5,6,2,7,9,8 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_qsort(arr, sz, sizeof(arr[0]),cmp);
//	print(arr, sz);
//	return 0;
//}

//int cmp(const void* e1, const void* e2) {
//	if (*(int*)e1 > *(int*)e2) {
//		return 1;
//	}
//	if (*(int*)e1 < *(int*)e2) {
//		return -1;
//	}
//	return 0;
//}

//测试qsort函数排序整型数据
//int main() {
//	int arr[] = { 3,1,5,6,2,7,9,8 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]),cmp);
//	print(arr, sz);
//  return 0;
//}

//输出函数
void print(int arr[], int sz) {
	int i = 0;
	for (i = 0; i < sz - 1; i++) {
		printf("%d", arr[i]);
	}
	printf("\n");
}

////bubble_sort 函数只能排序整型数据
//void bubble_sort(int arr[],int sz) 
//{
//	int i = 0;
//	//趟数
//	for(i=0;i<sz-1;i++){
//		int j = 0;
//		for (j = 0;j<sz-i-1; j++) {
//			if (arr[j] > arr[j + 1]) {
//				int temp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = temp;
//			}
//		}
//	}
//}
//

//冒泡排序
//int main() 
//{
//	//升序
//	int arr[] = { 3,1,5,6,2,7,9,8 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_sort(arr, sz);
//	print(arr, sz);
//	return 0;
//}