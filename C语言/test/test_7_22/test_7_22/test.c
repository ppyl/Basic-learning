#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

void a(int n) {
	int i, j = 0;
	for (i = 1; i < n+1; i++) {
		for (j = 1; j < i+1; j++) {
			printf("%d*%d=%-2d ", i, j, i * j);
		}
		printf("\n");
	}
	
}
int main()
{
	int n= 0;
	scanf("%d", &n);
	a(n);
	return 0;
}