#define _CRT_SECURE_NO_WARNINGS 1
//头文件的包含
//stdio std input output
//.h header 头文件
//标准输入输出
#include<stdio.h>

int main()
{
	//打印hello bit
	printf("hello bit\n");//""括起来的叫字符串
	return 0;
}

//main是主函数的意思
//main函数是程序的入口，一个工程有且只有一个
//F10逐过程
//int是返回类型（整形）
//printf是库函数，库函数的使用是需要包含头文件的
int main()
{
	return 0;
}
//void表示main函数不需要参数
int main(void) 
{
	return 0;
}

//%d 打印10进制的整数
int main() 
{
	printf("%d\n", sizeof(char));//1
	printf("%d\n", sizeof(short));//2
	printf("%d\n", sizeof(int));//4
	printf("%d\n", sizeof(long));//4
	printf("%d\n", sizeof(long long));//8
	printf("%d\n", sizeof(float));//4
	printf("%d\n", sizeof(double));//8

	return 0;
}

int b = 20;//全局变量
int a = 30;

int main()
{
	int a = 10;//局部变量
	//局部变量和全局变量的名字可以相同，当我们既可以使用局部，又可以使用全局变量的时候，局部变量优先
	printf("%d\n", a);
	return 0;
}

//写一个程序计算2个整数的和
//printf - 输出/打印
//scanf - 输入
//& 取地址
//strcpy strcat scanf 部分不安全函数
int main()
{
	int num1 = 1;
	int num2 = 2;
	int sum = 0;
	//1.输入2个整数
	scanf("%d %d", &num1, &num2);
	//2.求和
	sum = num1 + num2;
	//3.输出
	printf("%d\n", sum);

	return 0;
}

int b = 20;

int main()
{
	int a = 10;

	return 0;
}

int main()
{
	{
		int a = 10;
		printf("1:%d\n",a);
	}
	/*printf("2:%d\n", a);超过局部变量作用域*/

	return 0;
}

int a = 10;

void test()
{
	pritnf("test-->%d\n",a);
}
int main()
{
	printf("%d\n", a);
	test();
	return 0;
}

//extern是用来声明来自外部的符号
extern int g_val;
int main()
{
	printf("%d\n", g_val);
	return 0;
}