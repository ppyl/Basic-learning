#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int main() {
	int n = 0;
	int i,sum = 0;
	for (n = 1; n <= 3; n++) {
		int ret = 1;
		for (i = 1; i <= n; i++) {
			ret = ret * i;
		}
		sum = sum + ret;
	}
	printf("%d\n", sum);
	return 0;
}
int main() {
	int i = 0;
	for (i = 1000; i <= 2000; i++) {
		if (i % 100 == 0 || i % 4 == 0 && i % 100 != 0){
			printf("%d\n", i);
		}
	}
	return 0;
}
int main() {
	int a, b, c= 0;
	scanf("%d %d", &a, &b);
	while (c == a % b) {
		a = b;
		b = c;
	}
	printf("%d\n", b);
	return 0;
}
int main() {
	for (int i = 1; i < 10; i++) {
		for (int j = 1; j <= i; j++) {
			printf("%d = %d * %d   ", i*j, i, j);
			if (i == j) {
				printf("\n");
			}
		}
	}
	return 0;
}
int main() {
	int arr[10] = {0};
	int i = 0;
	int max = 0;
	for (i = 0; i < 10; i++) {
		scanf("%d", &arr[i]);
	}
	
	for (i = 1; i < 10; i++) {
		if (arr[i] > max) {
			max = arr[i];
		}
	}
	printf("%d", max);
	return 0;
}
int main() {
	double a = 0.0;
	double b = 0.0;
	double sum = 0.0;
	for (int i = 1; i < 100; i+=2) {
		a = a + (1.0) / i;
	}
	for (int j = 2; j < 101; j+=2) {
		b = b + (1.0) / j;
	}
	sum = a - b;
	printf("%lf\n", sum);
	return 0;
}
int main() {
	int sum = 0;
	for (int i = 1; i < 101; i++) {
		if (i % 10 == 9 || i / 10 == 9) {
			sum++;
		}
	}
	printf("%d\n", sum+1);
	return 0;
}
#include <stdlib.h>
#include <time.h>

void menu()
{
	printf("***********************\n");
	printf("******  1. play  ******\n");
	printf("******  0. exit  ******\n");
	printf("***********************\n");
}

void game()
{
	int guess = 0;
	//1. 生成随机数
	int ret = rand()%100+1;
	//2. 猜数字
	int i = 0;
	for (i = 0; i < 6; i++)
	{
		printf("请猜数字:>");
		scanf("%d", &guess);
		if (guess < ret)
		{
			printf("猜小了\n");
		}
		else if (guess > ret)
		{
			printf("猜大了\n");
		}
		else
		{
			printf("恭喜你，猜对了\n");
			break;
		}
	}
	if (i == 6) {
		printf("次数已用完，游戏结束\n");
	}
}

int main()
{
	int input = 0;
	//设置随机数的生成器(只调用一次)
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		printf("请选择:>");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("选择错误，重新选择!\n");
			break;
		}

	} while (input);

	return 0;
}
int main()
{
	int arr[] = { 1,2,3,4,15,26,37,43,54 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int left = 0;
	int right = sz-1;
	int k = 0;
	scanf("%d", &k);

	while (left<=right)
	{
		int mid = left + (right - left) / 2;
		if (arr[mid] < k)
		{
			left = mid + 1;
		}
		else if (arr[mid] > k)
		{
			right = mid - 1;
		}
		else
		{
			printf("找到了，下标是:%d\n", mid);
			break;
		}
	}
	if (left > right)
		printf("找不到\n");

	return 0;
}