#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#define N 4

void set(int(*arr)[N])
{
	for (int row = 0; row < N; row++)
	{
		for (int col = 0; col <= row; col++)
		{
			arr[row][0] = 1;
			if (row >= 1 && col >= 1)
			{
				arr[row][col] = arr[row - 1][col - 1] + arr[row - 1][col];
			}
		}
	}
}

void print(int(*arr)[N])
{
	for (int row = 0; row < N; row++)
	{
		for (int col = 0; col <= row; col++)
		{
			printf("%d ", arr[row][col]);
		}
		printf("\n");
	}
}
int main()
{
	int arr[N][N] = { 0 };
	set(arr);
	print(arr);

	return 0;
}