#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

int main() {
    int n;
    while (scanf("%d", &n) != EOF) {
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                printf(" ");
            }
            printf("*\n");
        }
    }
    return 0;
}s