#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#define SWAP(num) (num=((num & 0X55555555) << 1) | ((num & 0XAAAAAAAA) >> 1))

int main() {
	int a = 20;
	SWAP(a);
	printf("%d\n", a);
	return 0;
}

//#define OFFSETOF(S,len) (int)(&((struct S*)0)->len)
//#include<stdio.h>
//
//struct S
//{
//	int a;
//	char b;
//	long c;
//};
//
//int main() {
//	printf("%d\n", OFFSETOF(S, a));
//	printf("%d\n", OFFSETOF(S, b));
//	printf("%d\n", OFFSETOF(S, c));
//	return 0;
//}

//#define N 4
//#define Y(n)((N+2)*n)
//#include<stdio.h>
//
//int main() {
//	int z = 2 * (N + Y(5 + 1));
//	printf("%d\n", z);
//}