#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int missingNumber(int* nums,int numsize) {
	int N = numsize;
	int ret = N * (N + 1) / 2;
	for (int i = 0; i < numsize; ++i) {
		ret -= nums[i];
	}
	return ret;
}

int missingNumber(int* nums, int numsize) {
	int N = numsize;
	int x = 0;
	for (int i = 0; i < numsize; ++i) {
		x ^= nums[i];
	}
	for (size_t i = 0; i < N + 1; ++i) {
		x ^= i;
	}
	return x;
}